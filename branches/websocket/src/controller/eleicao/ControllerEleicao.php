<?php

include_once ("lib/MVC/Controller.php");
include_once ("lib/MVC/View.php");
include_once ("conf/functions.inc.php");
include_once ("controller/usuario/ControllerUsuario.php");
include_once ("model/candidato/ModelCandidato_" . BD_TYPE . ".php");
include_once ("model/eleicao/ModelEleicao_" . BD_TYPE . ".php");
include_once ("model/urna/ModelUrna_" . BD_TYPE . ".php");
include_once ("model/voto/ModelVoto_" . BD_TYPE . ".php");
include_once ("controller/urna/ControllerUrna.php");
include_once ("controller/pdf/ControllerPdf.php");


/**
 * ControllerEleicao
 * 
 */
class ControllerEleicao extends Controller {

	/**
	 * Método padrão que será chamado caso nenhum método seja especificado na classe.
	 * Chama o método eleicaoHome
	 * 
	 * @return void
	 */
	public function acaoPadrao() {
		$this->eleicaoHome();
	}

	/**
	 * Lista as urnas e seu status de apuração
	 * 
	 * @return void
	 */
	public function eleicaoHome() {
		ControllerUsuario::seAdministradorLogado();
		$view = new View();
		$view->tipo = 'CANDIDATO';
		$modelUrna = new ModelUrna();
		$data['arrEleicao'] = $modelUrna->listaUrnaParaApuracao();
		$modelEleicao = new ModelEleicao();
		$data['recarregar'] = $modelEleicao->urnasApuradasSemVotoSeparado();
		$data['urnasApuradas'] = $modelEleicao->urnasApuradas();
		$data['msg'] = $parametros['msg'];
		$view->data = $data;
		$view->carregar("eleicao/eleicaoHome.html");
		$view->mostrar();
	}

	/**
	 * Lista as urnas e seu status de apuração
	 * 
	 * @return void
	 */
	public function menuGeral() {
		ControllerUsuario::seAdministradorLogado();
		$view = new View();
		$view->tipo = 'CANDIDATO';
		$modelUrna = new ModelUrna();
		$data['urnasContabilizadas'] = $modelUrna->verificarUrnasSemContabilizacao();
		$view->data = $data;
		$view->carregar("eleicao/menuGeral.html");
		$view->mostrar();
	}


	/**
	 * ControllerEleicao::duplicidades()
	 * 
	 * @param mixed $parametros
	 * @return void
	 */
	public function duplicidades($parametros) {
		$data['zona'] = $parametros[0];
		$data['secao'] = $parametros[1];
	}

	/**
	 * Monitoramento de mirror´s
	 * 
	 * @return void
	 */
	public function mirror() {
		ControllerUsuario::seAdministradorLogado();
		$view = new View();
		$view->tipo = 'CANDIDATO';
		$modelUrna = new ModelUrna();
		$this->downloadArquivos(ORIGEM_STATUS, DESTINO_STATUS);
		sleep(2);
		$data['status'] = json_decode($this->carregaDadosMirror());
		sleep(2);
		$view->data = $data;
		$view->carregar("eleicao/monitorarMirror.html");
		$view->mostrar();
	}

	/**
	 * ControllerEleicao::verificarMirror()
	 * Verifica se existe a mídua de backup
	 * 
	 * @return void
	 */
	public function verificarMirror() {
		$status = json_decode($this->carregaDadosMirror());
		$dados['situacao'] = $status;
		print_r(json_encode($dados));
	}

	/**
	 * Download Mirror
	 * 
	 * @return void
	 */
	public function carregaDadosMirror() {
		ControllerUsuario::seAdministradorLogado();
		if ($this->downloadArquivos(ORIGEM_STATUS, DESTINO_STATUS)) {
			$pointer = opendir(ORIGEM_STATUS);
			while ($files = readdir($pointer)) {
				if (!is_dir($files)) {
					$data = explode('_', $files);
					$id = $data[0];
					if ($id == $_SESSION['eleicao']['id']) {
						$urna = $data[1];
						$zonaSecao = explode("-", str_replace(".txt", "", $data[2]));
						$zona = $zonaSecao[0];
						$secao = $zonaSecao[1];
						$f = fopen(ORIGEM_STATUS . $files, "r+");
						$obj = (object)array();
						while (!feof($f)) {
							$line = fgets($f, 4096);
						}
						fclose($f);
						$dados[] = array(
							$urna,
							$zona,
							$secao,
							$line);
					}
				}
			}
		}
		return json_encode($dados);
	}

	/**
	 * ControllerEleicao::zerarStatusUrna()
	 * 
	 * @return void
	 */
	public function zerarStatusUrna() {
		$controllerPdf = new ControllerPdf();
		$pointer = opendir(ORIGEM_STATUS);
		while ($files = readdir($pointer)) {
			if (!is_dir($files)) {
				$data = explode('_', $files);
				$id = $data[0];
				if ($id == $_SESSION['eleicao']['id']) {
					$f = fopen(ORIGEM_STATUS . $files, "w+");
					fwrite($f, '');
					fclose($f);
					chmod(ORIGEM_STATUS . $files, 0777);
					sleep(1);
					$controllerPdf->transmissaoDados(ORIGEM_STATUS, DESTINO_STATUS, $files);
				}
			}
		}
	}

	/**
	 * Mostra opções da apuração manual ou pela mídia de backup
	 * 
	 * @param array parâmetros passados pela url
	 * @return void
	 */
	public function apuracao($parametros = null) {
		ControllerUsuario::seAdministradorLogado();
		$view = new View();
		$view->tipo = 'padrao';
		$modelEleicao = new ModelEleicao();
    $modelUrna = new ModelUrna();
    $data['arrEleicao'] = $modelEleicao->eleicao();
    $data['msg'] = $parametros['msg'];
		$data['zonaAtiva'] = $parametros[0];
		$data['secaoAtiva'] = $parametros[1];
		$data['urnaAtiva'] = $parametros[2];
    $apurada = $modelUrna->consultarStatusUrnaApurada($data['zonaAtiva'],$data['secaoAtiva']);
    if($apurada == '-1' || ($apurada % 2 == 1 && $apurada != 0)) {
      $data['eletronica'] = true;
    }
    if($apurada > 0) {
      $data['manual'] = true;
    }
		$view->data = $data;
		$view->carregar("eleicao/apuracao.html");
		$view->mostrar();
	}

	/**
	 * Mostra formulário com lista de candidatos para informação dos votos manualmente.
	 * 
	 * @param array parâmetros passados pela url
	 * @return void
	 */
	public function apuracaoManual($parametros = null) {
		ControllerUsuario::seAdministradorLogado();
		$view = new View();
		$modelEleicao = new ModelEleicao();
		$modelCandidato = new ModelCandidato();
		$data['arrCandidato'] = $modelCandidato->listaCandidato($parametros[0]);
		$data['arrEleicao'] = $modelEleicao->eleicao();
		$data['arrCargos'] = $modelEleicao->cargosPorZona($parametros[0]);
		$data['msg'] = $parametros['msg'];
		$data['zona'] = $parametros[0];
		$data['secao'] = $parametros[1];
		$data['urna'] = $parametros[2];
		$view->tipo = 'Home';
		$view->data = $data;
		$view->carregar("eleicao/apuracaoManual.html");
		$view->mostrar();
	}

  /**
	 * Mostra formulário com lista de candidatos e votos para edição dos votos manualmente.
	 * 
	 * @param array parâmetros passados pela url
	 * @return void
	 */
	public function apuracaoManualEdicao($parametros = null) {
		ControllerUsuario::seAdministradorLogado();
		$view = new View();
		$modelEleicao = new ModelEleicao();
		$modelCandidato = new ModelCandidato();
		$data['arrCandidato'] = $modelCandidato->listaCandidatoVotosAputacaoManual($parametros[0],$parametros[1]);
    $data['arrBrancoNulo'] = $modelCandidato->listaBrancoNuloVotosAputacaoManual($parametros[0],$parametros[1]);
    $data['arrEleicao'] = $modelEleicao->eleicao();
		$data['arrCargos'] = $modelEleicao->cargosPorZona($parametros[0]);
		$data['msg'] = $parametros['msg'];
		$data['zona'] = $parametros[0];
		$data['secao'] = $parametros[1];
		$data['urna'] = $parametros[2];
		$view->tipo = 'Home';
		$view->data = $data;
		$view->carregar("eleicao/apuracaoManual.html");
		$view->mostrar();
	}

	/**
	 * Mostra lista de zonas para seleção da apuração manual.
	 * 
	 * @param array parâmetros passados pela url
	 * @return void
	 */
	public function listaZonas($parametros = null) {
		ControllerUsuario::seAdministradorLogado();
		$view = new View();
		$modelEleicao = new ModelEleicao();
		$data['arrZonas'] = $modelEleicao->zonas();
		$data['msg'] = $parametros['msg'];
		$view->tipo = 'Home';
		$view->data = $data;
		$view->carregar("eleicao/listaZonas.html");
		$view->mostrar();
	}

	/**
	 * Insere os votos da apuração manual na base de dados do servidor
	 * 
	 * @return void
	 */
	public function processaApuracaoManual() {
		ControllerUsuario::seAdministradorLogado();
		$view = new View();
		$modelVoto = new ModelVoto();
		$objVoto->zona = $_POST['zona'];
		$objVoto->secao = $_POST['secao'];
		$objVoto->urna = $_POST['urna'];
		$objVoto->votos = $_POST['candidato'];
		$objVoto->eleitores = $_POST['eleitores'];
		$modelVoto->inserirVotoManual($objVoto);
    $modelVoto->contabilizaVoto($objVoto);
		header('Location: /eleicao/eleicaoHome');
	}

	/**
	 * Mostra na tela o resultado geral da apuração e a porcentagem das urnas apuradas;
	 * 
	 * @return void
	 */
	public function publico() {
		ControllerUsuario::seAdministradorLogado();
		$model = new ModelEleicao();
		$view = new View();
		$view->tipo = 'CANDIDATO';
		include_once ("model/eleicao/ModelEleicao_" . BD_TYPE . ".php");
		$modelEleicao = new ModelEleicao();
		$data['arrResultadoEleicao'] = $modelEleicao->resultado();
		$data['arrCargos'] = $modelEleicao->cargos();
		$vtSeparado = $modelEleicao->existeVotoSeparado();
		$data['urnasApuradas'] = $modelEleicao->urnasApuradas();
		$view->data = $data;
		$view->carregar("eleicao/eleicaoPublico.html");
		$view->mostrar();
	}

	/**
	 * Mostra na tela o resultado geral da apuração e a porcentagem das urnas apuradas separado por zona;
	 * 
	 * @return void
	 */
	public function publicoGeral($parametros) {
		
		$model = new ModelEleicao();
		$view = new View();
		$view->tipo = 'CANDIDATO';
		include_once ("model/eleicao/ModelEleicao_" . BD_TYPE . ".php");
		$modelEleicao = new ModelEleicao();

		$conselho = $parametros[0];


		$data['arrResultadoEleicao'] = $modelEleicao->resultadoPublicoGeral($conselho);

		$view->data = $data;
		$view->carregar("eleicao/eleicaoPublicoGeral.html");
		$view->mostrar();
	}

	/**
	 * Exibe o status da situação da urna: mesário ou eleitor
	 * 
	 * @return var status da urna
	 */
	public function verificarStatus() {
		die($_SESSION['status']);
	}

	/**
	 * Cancela o voto não finalizado do usuário e libera a urna para um novo eleitor votar
	 * 
	 * @return status da urna
	 */
	public function cancelarVotacao($paramentros) {
		ControllerUsuario::seLogado();
		$func = new Functions();
		$modelUrna = new ModelUrna();
		$codigoInformado = $_POST['codigo'];
		$codigo = $func->removeNumeroLetra(sha1($_SESSION['eleicao']['zona'] . $_SESSION['eleicao']['secao']),
			1);
		if ($codigo == $codigoInformado) {
			if ($paramentros[0] == 's') {
				$modelUrna->atualizarCancelamento();
				$_SESSION['status'] = 'M';
				$_SESSION['acessibilidade'] = '';
				unset($_SESSION['acessibilidade']);
				//exec(CAMINHO_PORTAL . 'altf11.exe');
			}
		} else {
			die('ERRO');
		}
	}

	/**
	 * Verifica se a mídia externa inserida na máquina corresponde à urna a ser apurada
	 * informada pelo técnico de apuração. Caso seja a mídia correpsondente chama a 
	 * função de apuração
	 * 
	 * @param array parâmetros passados pela url
	 * @return boolean
	 */
	public function apuracaoAutomatica($parametros) {
		$dir = $_SESSION['CAMINHO_MIRROR']; 
		if (!is_dir($dir)) {
			error_reporting(0);
			echo 'Dispositivo não encontrado.';
			return false;
		}
		if (!file_exists($dir . "config.ini")) {
			error_reporting(0);
			echo 'Arquivo de configuração da mídia externa inválido.';
			return false;
		}
		$zona = $parametros[0];
		$secao = $parametros[1];
		$urna = $parametros[2];
		$dataBase = array(
			'ldf',
			'mdf',
			'ini');
		#Salva no array $files os nomes dos arquivos que terminam com as extensões
		#informadas no array $dataBase
		$dh = opendir($dir);
		while (false !== ($filename = readdir($dh))) {
			if ($filename != '.' && $filename != '..') {
				$base = explode('.', $filename);
				if (in_array($base[1], $dataBase)) {
					$files[] = $filename;
				}
			}
		}
		$f = fopen($dir . "config.ini", "r");
		while (!feof($f)) {
			$line = fgets($f, 4096);
		}
		$line = explode(";", $line);
    $modelUrna = new ModelUrna();
		$obj[] = $zona;
		$obj[] = $secao;
		$obj[] = $urna;
		#Verifica se o arquivo de configuração da mídia externa corresponde à mesma urna que o
		#usuário informou para ser apurada
		if (md5($zona) == $line[0] && md5($secao) == $line[1] && count($files) > 0) {
			$this->carregaDados($obj);
			return 'ok';
		} else {
			error_reporting(0);
			echo 'Urna apurada não equivale à mídia externa.';
			return false;
		}
	}

	/**
	 * Descriptografa o arquivo com os votos da urna e insere-os na base de dados
	 * 
	 * @param array parâmetros passados pela url
	 * @return void
	 */
	public function carregaDados($parametros) {
		$obj->zona = $parametros[0];
		$obj->secao = $parametros[1];
		$obj->urna = $parametros[2];
		$obj->dataBase = DB_EXTERNAL;
		$modelVoto = new ModelVoto();
		$objVotosBase = $modelVoto->consultarVotosLocal($obj);
		#Verifica se já existem votos da urna apurados da urna no servidor de apuração
		if (!$objVotosBase) {
			include ("lib/encDec/classEncDec.php");
			$encDec = new classEncDec(KEY_ENCRYPT);
			$f = fopen($_SESSION['CAMINHO_MIRROR'] . $obj->zona . "-" . $obj->secao . '.txt',
				"r+");
			$xReadTxt = 0;
			$obj = (object)array();
			#Verifica se o arquivo pertence à urna informada para ser apurada, caso pertença
			#percorre o arquivo com os votos, descriptografa o conteúdo e salva em um objeto
			
      while (!feof($f)) {
				$line = fgets($f, 4096);
				if ($xReadTxt == 0) {
					$line = explode(".", $line);
					$zona = $encDec->decrypt($line[0]);
					$line[1] = trim($line[1]);
					$secao = $encDec->decrypt($line[1]);
					$obj->zona = $zona;
					$obj->secao = $secao;
					$obj->urna = $parametros[2];
					$modelUrna = new ModelUrna();
					//$apurada = $modelUrna->consultarUrnaApurada($obj);
				} elseif ($xReadTxt == 1) {
					$line = explode("|", $line);
					$votantesTotal = $encDec->decrypt(trim($line[1]));
					$votantesComparecidos = $encDec->decrypt(trim($line[2]));
				} else {
					//if ($apurada) {
						$line = explode("|", $line);
						if (count($line) > 1) {
							$obj->votos[$encDec->decrypt($line[0])] = $encDec->decrypt(trim($line[1]));
						}
					//}
				}
				$xReadTxt++;
			} 
			$modelVoto = new ModelVoto();
			$nameFile = explode(".", $files);
			fclose($f);
      
			#Passa o objeto com os dados da votação para a função de inserção no banco
      //exit(var_dump($modelVoto->inserirVotoSincronizado($obj)));
			if ($modelVoto->inserirVotoSincronizado($obj)) {
				$modelUrna->inserirTotalEleitores($votantesTotal, $votantesComparecidos, $obj->
					zona, $obj->secao,$obj->urna);
				echo 'Urna'.  $obj->urna .' da Zona: ' . $obj->zona . ' Secão: ' . $obj->secao . ' apurada.';
			} else {
				echo 'Falha na sincronização dos dados da Zona: ' . $obj->zona . ' Secão: ' . $obj->
					secao . '.';
			}
		} else {
			echo 'Urna'.  $obj->urna .' da Zona: ' . $obj->zona . ' Secão: ' . $obj->secao . ' já apurada.';
		}
	}

	/**
	 * Exibe resultado da eleição
	 * 
	 * @return void
	 */
	public function exibirResultado() {
		ControllerUsuario::seLogado();
		$model = new ModelEleicao();
		$view = new View();
		$view->tipo = 'Home';
		include_once ("model/eleicao/ModelEleicao_" . BD_TYPE . ".php");
		$modelEleicao = new ModelEleicao();
		$hora = date("H:i:s");
		$data['arrEleicao'] = $modelEleicao->listaEleicao();
		$data['msg'] = $parametros['msg'];
		$view->data = $data;
		$view->carregar("eleicao/eleicaoHome.html");
		$view->mostrar();
	}

	/**
	 * Emite a zerésima
	 * 
	 * @return void
	 */
	public function emitirZeresima() { 
		$controllerUrna = new ControllerUrna(); 
		$dadosUrna->zona = $_SESSION['eleicao']['zona'];
		$dadosUrna->secao = $_SESSION['eleicao']['secao'];
		$modelUrna = new ModelUrna();
		$modelUrna->alterarStatus('2');
		$controllerEleicao = new ControllerEleicao();
		$controllerUrna->emitirPdfBoletimUrna($dadosUrna,'zeresima');
		
	}

	/**
	 * Verifica se existem votos computados na urna.
	 * 
	 * @return void statusZeresima
	 */
	public function verificarEmissaoZeresima() {
		$controllerUrna = new ControllerUrna();
		$existemVotosUrna = $controllerUrna->verificarEmissaoZeresima();
		if ($existemVotosUrna) {
			echo "UCV"; #UCV - Urna com votos
		} else {
			echo "NTV"; #NTV - Não tem votos
		}
	}

	/**
	 * Emite o boletim de urna
	 * 
	 * @return void
	 */
	public function emitirBU($param = null) {
		$controlerUrna = new ControllerUrna();
		$modelUrna = new ModelUrna();
		$dadosUrna->zona = $_SESSION['eleicao']['zona'];
		$dadosUrna->secao = $_SESSION['eleicao']['secao'];
		$dadosUrna->urna = $_SESSION['eleicao']['urn_numero'];
		if ($param[0]) {
			$dadosUrna->enviar = "true";
		}
		$modelUrna->alterarStatus('5');
		$controlerUrna->emitirPdfBoletimUrna($dadosUrna,'bu');
		echo '<script>window.close();</script>';
	}

		/**
	 * Emite o boletim de urna Apuração
	 * 
	 * @return void
	 */
	public function emitirBuApuracao($param = null) {

		$dadosUrna->zona  = $param[0];
		$dadosUrna->secao = $param[1];

		$controlerUrna = new ControllerUrna();
		$controlerUrna->emitirPdfBoletimUrnaApuracao($dadosUrna);

		var_dump($dadosUrna);die();
		
		echo '<script>window.close();</script>';
	}

	/**
	 * Emite o boletim final
	 * 
	 * @return void
	 */
	public function emitirApuracaoGeralPorConselho($param = null) {
		
		$documento = $param[0];
		$controlerUrna = new ControllerUrna();
		$modelUrna = new ModelUrna();	
		$controlerUrna->emitirApuracaoGeralPorConselho($documento);
		echo '<script>window.close();</script>';
	}

	/**
	 * Emite o boletim final
	 * 
	 * @return void
	 */
	public function impressaoApuracaoFinal() {
		$controlerUrna = new ControllerUrna();
		$controlerUrna->emitirBoletimFinal();
	}

	/**
	 * Pega o status atual da eleição
	 * 
	 * @return string statusEleicao
	 */
	public function obterStatus() {
		$modelUrna = new ModelUrna();
		$status = $modelUrna->obterStatus();
		return $status;
	}

	/**
	 * Altera o status da eleição
	 * 
	 * @param array parâmetros passados pela url
	 * @return void
	 */
	public function alterarStatus($parametros) {
		$controllerEleicao = new ControllerEleicao();
		$dataInicioEleicao = $_SESSION['eleicao']['data'];
		$dataFimEleicao = $_SESSION['eleicao']['dataFim'];
		$func = new Functions();
		$diferencaEntreDias = $func->diffDate($dataFimEleicao, $dataInicioEleicao, 'D');
		if ($diferencaEntreDias == 0) {
			$modelUrna = new ModelUrna();
			$status = $parametros[0];
			$modelUrna->alterarStatus($status);
		} else {
			foreach ($_SESSION as $k => $v) {
				$_SESSION[$k] = '';
				unset($_SESSION[$k]);
			}
			session_destroy();
			$modelEleicao = new ModelEleicao();
			$modelEleicao->atualizarInicioEleicao();
		}
		$controllerEleicao = new ControllerEleicao();
	}

	/**
	 * Faz a leitura dos arquivos enviador para o servidor com o mapa de votação e faz a 
	 * importação para o banco apuração
	 * 
	 * @return void
	 */
	public function apurarUrna() {
		$pointer = opendir(ORIGEM_APURACAO);
		include ("lib/encDec/classEncDec.php");
		$encDec = new classEncDec(KEY_ENCRYPT);
		$modelUrna = new ModelUrna();
		while ($files = readdir($pointer)) {
			if (!is_dir($files)) {
				if (!strstr($files, 'ok')) {
					$f = fopen(ORIGEM_APURACAO . '/' . $files, "r+");
					$xReadTxt = 0;
					$obj = (object)array();
					#Verifica se o arquivo pertence à urna informada para ser apurada, caso pertença
					#percorre o arquivo com os votos, descriptografa o conteúdo e salva em um objeto
					while (!feof($f)) {
						$line = fgets($f, 4096);
						if ($xReadTxt == 0) {
							$line = explode(".", $line);
							$zona = $encDec->decrypt($line[0]);
							$line[1] = trim($line[1]);
							$secao = $encDec->decrypt($line[1]);
							$obj->zona = $zona;
							$obj->secao = $secao;
							$modelUrna = new ModelUrna();
							$apurada = $modelUrna->consultarUrnaApurada($obj);
						} elseif ($xReadTxt == 1) {
							$line = explode("|", $line);
							$votantesTotal = $encDec->decrypt(trim($line[1]));
							$votantesComparecidos = $encDec->decrypt(trim($line[2]));
						} else {
							if (!$apurada) {
								$line = explode("|", $line);
								if (count($line) > 1) {
									$obj->votos[$encDec->decrypt($line[0])][trim($encDec->decrypt(trim($line[2])))] =
										$encDec->decrypt(trim($line[1]));
								}
							}
						}
						$xReadTxt++;
					}
					$modelVoto = new ModelVoto();
					$nameFile = explode(".", $files);
					fclose($f);
					#Passa o objeto com os dados da votação para a função de inserção no banco
					if ($modelVoto->inserirVotoSincronizado($obj) == 'NULL') {
						$modelUrna->inserirTotalEleitores($votantesTotal, $votantesComparecidos, $obj->
							zona, $obj->secao);
						rename(ORIGEM_APURACAO . "/" . $files, ORIGEM_APURACAO . "/" . $nameFile[0] .
							'_ok.' . $nameFile[1]);
					}
				}
			}
		}
	}

	/**
	 * Copia os arquivos de apuração do servidor WEB
	 *
	 * @return void
	 */
	public function downloadArquivos($origem, $destino) {
		$conn_id = ftp_connect(FTP_HOST, 21);
		$ftpResult = ftp_login($conn_id, FTP_USER, FTP_PASS);
		$arrayArquivosApuracaoLocal = array();
		if ($ftpResult) {
			$arquivos = ftp_nlist($conn_id, $destino);
			$pointer = opendir($origem);
			while ($files = readdir($pointer)) {
				if (!is_dir($files) && strpos($files, '_ok')) {
					$arrayArquivosApuracaoLocal[] = str_replace("_ok", "", $files);
				}
			}
			chmod($origem, 0777);
			if (count($arquivos) > 0) {
				foreach ($arquivos as $list) {
					$file = end(explode("/", $list));
					if (!in_array($file, $arrayArquivosApuracaoLocal)) {
						$fp = fopen($origem . $file, 'w+');
						$ret = ftp_fget($conn_id, $fp, $list, FTP_ASCII, 0);
						fclose($fp);
					}
				}
			}
			ftp_close($conn_id);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ControllerEleicao::copiarArquivoApuracao()
	 * Copia os arquivos de apuração do FTP
	 * 
	 * @return bool
	 */
	public function copiarArquivoApuracao() {
		$ftpServer = FTP_HOST;
		$conn_id = ftp_connect($ftpServer, 21);
		$ftpLogin = FTP_USER;
		$ftpSenha = FTP_PASS;
		$ftpResult = ftp_login($conn_id, $ftpLogin, $ftpSenha);
		if ($ftpResult) {
			$arquivos = ftp_nlist($conn_id, DESTINO_APURACAO);
			exit(var_dump($arquivos));
			$pointer = opendir(ORIGEM_APURACAO);
			while ($files = readdir($pointer)) {
				if (!is_dir($files)) {
					$arrayArquivosApuracaoLocal[] = str_replace("_ok", "", $files);
				}
			}
			chmod(ORIGEM_APURACAO, 0777);
			if (count($arquivos) > 0) {
				foreach ($arquivos as $list) {
					$file = end(explode("/", $list));
					if (!in_array($file, $arrayArquivosApuracaoLocal)) {
						$fp = fopen(ORIGEM_APURACAO . $file, 'w+');
						$ret = ftp_fget($conn_id, $fp, $list, FTP_ASCII, 0);
						fclose($fp);
					}
				}
			}
			ftp_close($conn_id);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ControllerEleicao::apagarDadosUrna()
	 * Apaga todos os votos da urna caso tente iniciar a eleição com algum voto
	 * 
	 * @return void
	 */
	public function apagarDadosUrna() {
		$modelVoto = new ModelVoto();
		$modelVoto->apagarVotosUrna();
		$modelEleitor = new ModelEleitor();
		$modelEleitor->zerarStatusEleitor();
		$modelUrna = new ModelUrna();
		$modelUrna->atualizarStatusUrna();
	}

	/**
	 * ControllerEleicao::gerarSeparado()
	 * 
	 * @return void
	 */
	public function gerarSeparado() {
		$view = new View();
		$view->tipo = 'padrao';
		$modelEleicao = new ModelEleicao();
		$data['dados'] = $modelEleicao->gerarSeparado();
		$view->data = $data;
		$view->carregar("eleicao/teste.html");
		$view->mostrar();
	}

	/**
	 * ControllerEleicao::copydataBases()
	 * Copia a base para a mídia de backup e restaura a base do backup em uma máquina sem banco
	 * @return
	 */
	public function copydataBases() {
		$func = new Functions();
		$msg = '';
		$caminho_portal = CAMINHO_PORTAL;
		$caminho_mirror = $_SESSION['CAMINHO_MIRROR'];
		if (!is_dir($_SESSION['CAMINHO_MIRROR'])) {
			$caminho_mirror = $caminho_portal . 'banco/';
		}
		$filePrincipal = $caminho_portal . 'banco/' . BD_BASE . '.db';
		$fileMirror = $caminho_mirror . BD_BASE_MIRROR . '.db';
		if (is_dir($caminho_mirror)) {
			if ($this->existDatabase($filePrincipal)) {
				sleep(1);
#echo $filePrincipal.'<br>';
#echo $fileMirror.'<br>';
				$copyMirror = @copy($filePrincipal, $fileMirror);
#var_dump($copyMirror);

#$errors= error_get_last();
#    echo "COPY ERROR: ".$errors['type'];
#    echo "<br />\n".$errors['message'];

#exit;
				if (SO == 'linux') {
					$func->copyMirror();
					$copyMirror = true;
				}
				if (!$copyMirror) {
					$msg = 'Espelho não criado, favor verificar.';
				}
			}
		} else {
			$msg = "Verificar dispositivo externo e recarregue a página.";
		}
		if (!$this->existDatabase($filePrincipal)) {
			if (!is_dir(CAMINHO_PORTAL . 'banco/')) {
				mkdir(CAMINHO_PORTAL . 'banco/');
				chmod(CAMINHO_PORTAL . 'banco/', 0777);
			}
			if (is_dir($caminho_mirror)) {
				if ($this->existDatabase($fileMirror)) {
					sleep(2);
					$_SESSION['copiaBackup'] = true;
					$msg = 'Deseja restaurar o backup que se encontra no dispositivo externo?';
				}
			}
		}
		//Backup Extra
		if ($this->existDatabase($fileMirror)) {
			copy($fileMirror, CAMINHO_PORTAL . 'banco/' . BD_BASE . '_backup.db');
		} elseif ($this->existDatabase($filePrincipal)) {
			copy($filePrincipal, CAMINHO_PORTAL . 'banco/' . BD_BASE . '_backup.db');
		}
		if (!$this->existDatabase($filePrincipal) && !$this->existDatabase($fileMirror)) {
			$msg = "Sistema sem base de dados.";
		}
		return $msg;
	}

	public function restaurarBackup() {

		$caminho_portal = CAMINHO_PORTAL;
		$caminho_mirror = $_SESSION['CAMINHO_MIRROR'];
		if (!is_dir($_SESSION['CAMINHO_MIRROR'])) {
			$caminho_mirror = $caminho_portal . 'banco/';
		}
		$filePrincipal = $caminho_portal . 'banco/' . BD_BASE . '.db';
		$fileMirror = $caminho_mirror . BD_BASE_MIRROR . '.db';

		if (SO == 'linux') {
			restoreMirror();
			$copyPrincipal = true;
		} else {
			$copyPrincipal = copy($fileMirror, $filePrincipal);
		}

		if ($copyPrincipal) {
			echo 'Backup restaurado com sucesso.';
		} else {
			echo 'Falha ao copiar base principal, favor verificar.';
		}
	}

	/**
	 * ControllerEleicao::existDatabase()
	 * Verifica se existe a base de dados
	 * 
	 * @param string $base
	 * @return bool
	 */
	public function existDatabase($base) {
		return file_exists($base);
	}

	public function eleicaoDeferirCandidato()
	{
		$view = new View();
		$view->tipo = 'padrao';
		$view->carregar("eleicao/eleicaoDeferirCandidato.html");
		$view->mostrar();
	}
  
  public function resultadoUrna($parametros = null) {
		ControllerUsuario::seAdministradorLogado();
		$view = new View();
    $urna = new StdClass;
    $urna->zona = $parametros[0];
    $urna->secao = $parametros[1];
    $urna->origem = $parametros[2];
    $modelCandidato = new ModelCandidato();
		$modelEleicao = new ModelEleicao();
		$data['arrCargos'] = $modelEleicao->cargosPorZona($parametros[0]); 
    $arr['arrCandidatoVoto'] = $modelCandidato->listaVotosCandidatoUrnaShow($urna);
    $arr['arrCandidatoZero'] = $modelCandidato->listaVotosCandidatoUrnaZeroShow($urna);
    $arr['arrBrancoNulo'] = $modelCandidato->listaVotosBrancoNuloUrnaShow($urna);
    foreach($arr['arrBrancoNulo'] as $bn) {
      if($bn['cdt_oab'] == 0) {
        $data['arrBrancoNulo']['B'] = $bn['votos'];
      } else {
        $data['arrBrancoNulo']['N'] = $bn['votos'];
      }
    }
    $data['arrCandidato'] = array_merge($arr['arrCandidatoVoto'],$arr['arrCandidatoZero']);
    $data['zona'] = $parametros[0];
		$data['secao'] = $parametros[1];
		$view->tipo = 'Home';
		$view->data = $data;
		$view->carregar("eleicao/resultadoUrna.html");
		$view->mostrar();
	}

	public function eleicaoMapaUrnas()
	{	
		$view = new View();
		$view->tipo = 'padrao';
		$modelEleicao = new ModelEleicao();
		$data['dados'] = $modelEleicao->eleicaoMapaUrnas();
		$data['apuracao'] = $modelEleicao->urnasApuradasGeral();
		$view->data = $data;
		$view->carregar("eleicao/eleicaoMapaUrnas.html");
		$view->mostrar();
	}

}
