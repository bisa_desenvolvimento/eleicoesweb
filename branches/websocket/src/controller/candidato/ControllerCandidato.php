
<?php

include_once ("lib/MVC/Controller.php");
include_once ("lib/MVC/View.php");
include_once ("model/candidato/ModelCandidato_" . BD_TYPE . ".php");
include_once ("controller/usuario/ControllerUsuario.php");
include_once ("conf/functions.inc.php");

/**
 * ControllerCandidato
 * 
 */
class ControllerCandidato extends Controller {

	/**
	 * Método padrão que será chamado caso nenhum método seja especificado na classe.
	 * Enviará o usuário para o menu da urna.
	 * 
	 * @return void
	 */
	public function acaoPadrao() {
		header('Location: /urna/menuUrna/');
	}

	/**
	 * Lista os votos do candidato na urna
	 * 
	 * @return json votos
	 */
	public function listaVotosCandidatoUrna() {
		ControllerUsuario::seLogado();
		$modelCandidato = new ModelCandidato();
		$arrUrna->zona = $_POST['zona'];
		$arrUrna->secao = $_POST['secao'];
		$arrUrna->urna = $_POST['urna'];
		$candidatos['candidatos'] = $modelCandidato->listaVotosCandidatoUrnaFull($arrUrna);
		die(json_encode($candidatos));
	}

	/**
	 * Retorna os dados do candidato passado como parâmetro
	 * 
	 * @return json votos
	 */
	public function getDadosCandidato() {
		ControllerUsuario::seLogado();
		$modelCandidato = new ModelCandidato();
		$numeroCandidato = $_REQUEST['numeroCandidato'];
		$cargoCandidato = $_REQUEST['cargoCandidato'];
		$candidatoObj = $modelCandidato->listaCandidatoPorOab($numeroCandidato,$cargoCandidato);
		$candidato['nome'] = $candidatoObj->nome;
		$candidato['numero'] = $candidatoObj->numCandidatura;
		$candidato['foto'] = $candidatoObj->foto;
		die(json_encode($candidato));
	}

	/**
	 * Retorna os dados do candidato passado como parâmetro
	 * 
	 * @return json dados
	 */
	public function buscaCandidatoDeferido()
	{
		$candidato = $_POST['candidato'];

		$modelCandidato = new ModelCandidato();
		$candidatoObj = $modelCandidato->buscaCandidatoDeferido($candidato);

		echo json_encode($candidatoObj[0]);

	}
	public function deferirCandidato()
	{
		$candidato = $_POST['candidato'];
		$status = $_POST['statusDeferimento'];
		$protocolo = $_POST['protocolo'];
		$imagem  = 'Não informado';

		if($protocolo == ''){
			$protocolo = 'Não informado';
		}

		if($status == 'D'){
			$status = 'I';
		}else{
			$status = 'D';
		}

		$modelCandidato = new ModelCandidato();
		$imagem = $modelCandidato->uploadImagemRequerimento($_FILE);
		$candidatoObj = $modelCandidato->deferirCandidato($candidato,$status,$protocolo,$imagem);
		$dados = array();

		$view = new View();
		$view->tipo = 'padrao';
		$data['mensagem'] = "Alterado com sucesso!";

		if(!$candidatoObj){
			
			$data['mensagem'] = "Erro durante a alteração!";
			
		}	
		
		$view->data = $data;
		$view->carregar("eleicao/eleicaoDeferirCandidato.html");
		$view->mostrar();
	}

	public function relatorioIndeferidos()
	{
		$view = new View();
		$modelCandidato = new ModelCandidato();
		$data['dados'] = $modelCandidato->relatorioIndeferidos();
		/*echo "<pre>";
		var_dump($data['dados']);die();*/
		$view->tipo = 'IMPRESSAO';
		$view->data = $data;
		$view->carregar("relatorios/relatorioIndeferidos.html");
		$view->mostrar();
	}
}
