<?php

include_once ("lib/MVC/View.php");
include_once ("lib/MVC/Controller.php");
include_once ("model/urna/ModelUrna_" . BD_TYPE . ".php");
include_once ("model/urna/Urna.php");
include_once ("model/voto/ModelVoto_" . BD_TYPE . ".php");
include_once ("model/eleitor/ModelEleitor_" . BD_TYPE . ".php");
include_once ("model/candidato/ModelCandidato_" . BD_TYPE . ".php");
include_once ("controller/pdf/ControllerPdf.php");
include_once ("controller/usuario/ControllerUsuario.php");
include_once ("controller/eleicao/ControllerEleicao.php");
include_once ("conf/functions.ini.php");
/**
 * ControllerUrna
 *
 */
class ControllerUrna extends Controller {

	/**
	 * M�todo padr�o que ser� chamado caso nenhum m�todo seja especificado na url.
	 * Chama o m�todo menuUrna
	 *
	 * @return void
	 */
	public function acaoPadrao() {
		$this->menuUrna();
	}

	/**
	 * Exibe listagem dos candidatos para vota��o.
	 *
	 * @return void
	 */
	public function exibirCandidatos() {
		ControllerUsuario::seMesarioLogado();
		$controllerEleicao = new ControllerEleicao();
		$modelCandidato = new ModelCandidato();

		//Verifica se o usu�rio est� realmente liberado para votar e se a elei��o ainda n�o foi encerrada
		//caso contr�rio envia para a tela de espera do eleitor
		if ($_SESSION['status'] == 'E' && $controllerEleicao->obterStatus() < 4) {
			$view = new View();
			$data['msg'] = $parametros['msg'];
			$data['arrCandidato'] = $modelCandidato->listaCandidato();
			$view->data = $data;
			$view->tipo = 'candidato';
			$view->carregar("terminalEleitor/digitarCandidato.html");
			$view->mostrar();
		} else {
			$this->standByEleitor();
		}
	}

	/**
	 * Recebe os votos do eleitor e envia parao model de inser��o na urna.
	 *
	 * @return void
	 */
	public function computarVoto() {
		ControllerUsuario::seLogado();
		$controllerEleicao = new ControllerEleicao();
		$modelVoto = new ModelVoto();
		$modelUrna = new ModelUrna();
		//$detectMirror = $modelUrna->gravarParaDetectarBanco(BD_BASE_MIRROR);
		//$func = new Functions;
		//$existeArquivoMemoryStick = $func->existeArquivo($_SESSION['CAMINHO_MIRROR'] .
		//	BD_BASE_MIRROR . '.mdf');
		//$existeArquivoLocal = $func->existeArquivo(DESTINO_RECOVERY . BD_BASE_MIRROR .
		//	'.mdf');
		//unset($_SESSION['error_mirror']);

        if (!array_key_exists('computados', $_SESSION)) {
            // Caso não hava nenhuma informação sobre votos computados, insere e cria o array
            $_SESSION['computados'] = array();
            $_SESSION['computados']['eleitor'] = true;
            $modelVoto->inserirVoto($_POST['votos'], $_POST['cargo']);
        } elseif (!array_key_exists($_SESSION['eleitor'], $_SESSION['computados'])) {
            // Caso já existam votos computados, esse eleitor já votou
            $_SESSION['computados']['eleitor'] = true;
            $modelVoto->inserirVoto($_POST['votos'], $_POST['cargo']);
        }

		$controllerEleicao->copydataBases();
		$_SESSION['acessibilidade'] = '';
		unset($_SESSION['acessibilidade']);
		header('Location: /urna/standByEleitor');
		$_SESSION['status'] = 'M';

	}

	/**
	 * Exibe a tela de final da vota��o par ao eleitor.
	 *
	 * @return void
	 */
	public function standByEleitor() {

		ControllerUsuario::seMesarioLogado();
		$controllerEleicao = new ControllerEleicao();
		$view = new View();
		$view->tipo = "cinza";
		if ($controllerEleicao->obterStatus() < 3) {
			$view->carregar("terminalEleitor/standbyinicio.html");
		} else {
			$view->carregar("terminalEleitor/standby.html");
		}
		$view->mostrar();
		
	}

	/**
	 * Exibe a tela informando que a elei��o ainda n�o foi iniciada no come�o da vota��o.
	 *
	 * @return void
	 */
	public function standByEleitorInicio() {
		$view = new View();
		$view->tipo = "cinza";
		$view->carregar("terminalEleitor/standbyinicio.html");
		$view->mostrar();
	}

	/**
	 * Exibe as op��es da elei��o (emitor zer�sima, iniciar elei��o, encerrar elei��o e emitir bu) para o mes�rio.
	 *
	 * @return void
	 */
	public function menuUrna() {
		$data['msg'] = $parametros[0];
		ControllerUsuario::seLogado();
		$modelUrna = new ModelUrna();
		$data['statusUrna'] = $modelUrna->obterStatus();
		$_SESSION['statusUrna'] = $modelUrna->obterStatus();
		$controllerEleicao = new ControllerEleicao();
		$data['msg'] = $controllerEleicao->copydataBases();
		$view = new View();
		$view->tipo = "branco";
		$view->data = $data;
		$view->carregar("terminalMesario/menuUrna.html");
		$view->mostrar();
	}

	/**
	 * Exibe a tela onde o mes�rio insere o documento do eleitor e libera o temrinal do eleitor para vota��o.
	 *
	 * @return void
	 */
	public function liberarEleitor() {

		ControllerUsuario::seLogado();
		$modelUrna = new ModelUrna();
		$data['statusUrna'] = $modelUrna->obterStatus();
		if ($modelUrna->obterStatus() == '2') {
			$modelUrna->alterarStatus('3');
		}
		$controlleEleicao = new ControllerEleicao();
		$filePrincipal = CAMINHO_PORTAL . 'banco/' . BD_BASE . '.db';
		if (!$controlleEleicao->existDatabase($filePrincipal)):
			$controlleEleicao->copydataBases();
		endif;
		$data['votantes'] = $this->votantesUrna();
		$view = new View();
		$view->data = $data;
		$view->tipo = "branco";
		$view->carregar("terminalMesario/liberarEleitor.html");
		$view->mostrar();
	}

	/**
	 * Exibe a tela informando que o elitor est� votando e a op��o do mes�rio cancelar o voto do eleitor.
	 *
	 * @return void
	 */
	public function standByMesario() {
		ControllerUsuario::seLogado();
		$controlleEleicao = new ControllerEleicao();
		$filePrincipal = CAMINHO_PORTAL . 'banco/' . BD_BASE . '.db';
		if (!$controlleEleicao->existDatabase($filePrincipal)) {
			$controlleEleicao->copydataBases();
		}
		$_SESSION['status'] = 'E';
		$view = new View();
		$view = new View();
		$view->tipo = "branco";
		$view->carregar("terminalMesario/standby.html");
		$view->mostrar();
	}

	/**
	 * Impressao do PDF da Zer�sima / Boletim de Urna
	 *
	 * @param mixed $parametros
	 * @return void
	 */
	public function gerarPdfZeresimaBu($parametros) {
		$urna = new Urna($_SESSION['eleicao']['zona'], $_SESSION['eleicao']['secao']);
		$urna->pdf = true;
		if ($parametros[0] == 1) {
			$this->emitirPdfZeresima($urna);
		} else {
			$this->emitirPdfBoletimUrna($urna);
		}
	}

	/**
	 * Define os dados de elei��o e passa para a classe que emite a zer�sima.
	 *
	 * @param mixed $urna
	 * @return void
	 */
	public function emitirPdfZeresima($urna) {
		$modelEleitor = new ModelEleitor();
		$qtdEleitores = $modelEleitor->getQuantidadeEleitores($urna);
		$qtdAptos = $qtdEleitores[0]["qtdAptos"];
		$modelUrna = new ModelUrna();
		$hora = date("Y-m-d H:i:s");
		$modelUrna->updateZeresima($hora);
		$modelCandidato = new ModelCandidato();
		$listaCandidatos = $modelCandidato->listaVotosCandidatoUrna($urna);
		$modelVoto = new ModelVoto();
		$existeVotosUrna = $modelVoto->consultarVotosLocal($urna);
		$zeresima['eleicao'] = $_SESSION['eleicao'];
		$zeresima['eleitores'] = $qtdEleitores;
		$zeresima['hora'] = $hora;
		$zeresima['listaCandidatos'] = $listaCandidatos;
		$zeresima['listaQtdVotos'] = $modelVoto->listaQtdVotosBU($urna);
		$zeresima['eleicao'] = $_SESSION['eleicao'];
		$zeresima['hora'] = $hora;
		$zeresima['qtdEleitoresAptos'] = $qtdAptos;
		$zeresima['listaCandidatos'] = $listaCandidatos;
		$pdf = new ControllerPdf();
		if ($urna->pdf == true) {
			$pdf->gerarPDFZeresima($zeresima);
		} else {
			$pdf->impressaoPdfZeresima($zeresima);
		}
	}

	/**
	 * Verifica se j� existem votos antes do inicio da vota��o na urna.
	 *
	 * @return string status da urna
	 */
	public function verificarEmissaoZeresima() {
		$modelVoto = new ModelVoto();
		$dadosUrna->zona = $_SESSION['eleicao']['zona'];
		$dadosUrna->secao = $_SESSION['eleicao']['secao'];
		$dadosUrna->dataBase = BD_BASE;
		$existeVotosUrna = $modelVoto->consultarVotosLocal($dadosUrna);
		if ($existeVotosUrna) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Define os dados de eleição e passa para a classe que emite o boletim de urna.
	 *
	 * @param mixed $urna
	 * @return
	 */
	public function emitirPdfBoletimUrna($urna,$tipo) {
		$modelEleitor = new ModelEleitor();
		$qtdEleitores = $modelEleitor->getQuantidadeEleitores($urna);
		$qtdEleitores = $qtdEleitores[0];
		$modelCandidato = new ModelCandidato();
		$listaCandidatos = $modelCandidato->listaVotosCandidatoUrna($urna);
		$listaBrancoNulo = $modelCandidato->listaVotosBrancoNuloCargo($urna);
		$modelVoto = new ModelVoto();
		$quantidadeVotos = $modelVoto->listaQtdVotosBU($urna);
		$quantidadeVotosCargo = $modelVoto->listaQtdVotosBUPorCargo();
		$modelUrna = new ModelUrna();
		$hora = date("Y-m-d H:i:s");
		$modelUrna->updateBu($hora);
		$bU['eleicao'] = $_SESSION['eleicao'];
		$bU['eleitores'] = $qtdEleitores;
		$bU['hora'] = $hora;
		$bU['listaCandidatos'] = $listaCandidatos;
		$bU['listaBrancoNulo'] = $listaBrancoNulo;
		$bU['listaQtdVotos'] = $quantidadeVotos;
		$bU['listaQtdVotosCargo'] = $quantidadeVotosCargo;
		$bU['enviar'] = $urna->enviar;
		$pdf = new ControllerPdf();
		if ($urna->pdf == true) {
			$pdf->gerarPDFBoletimUrna($bU);
		} else {
			$pdf->impressaoPdfBu($bU,$tipo);
		}
	}

  /**
     * Define os dados de elei��o e passa para a classe que emite o boletim de urna pela apuração.
     *
     * @param mixed $urna
     * @return
     */
    public function emitirPdfBoletimUrnaApuracao($urna)
    {

        $modelEleitor = new ModelEleitor();
        $qtdEleitores = $modelEleitor->getQuantidadeEleitoresApuracao($urna);
        $qtdEleitores = $qtdEleitores[0];

        $modelCandidato = new ModelCandidato();
        $listaCandidatos = $modelCandidato->listaVotosCandidatoUrnaApuracao($urna);
        $listaBrancoNulo = $modelCandidato->listaVotosBrancoNuloCargo($urna);

        $hora = date("Y-m-d H:i:s");

        $bU['eleicao'] = $_SESSION['eleicao'];
        $bU['urna'] = $urna;
        $bU['eleitores'] = $qtdEleitores;
        $bU['hora'] = $hora;
        $bU['listaCandidatos'] = $listaCandidatos;
        $bU['listaBrancoNulo'] = $listaBrancoNulo;
        $bU['listaQtdVotos'] = 0;
        $bU['enviar'] = true;

        $pdf = new ControllerPdf();
        echo $pdf->getHtmlBuApuracao($bU);


    }
	/**
	 * Define os dados de eleição e passa para a classe que emite o boletim de urna.
	 *
	 * @param mixed $urna
	 * @return
	 */
	public function emitirApuracaoGeralPorConselho($documento) {

		$modelCandidato = new ModelCandidato();
		$listaApuracao = $modelCandidato->pegarApuracaoGeral();
		$brancosNulos = $modelCandidato->pegarBrancoNulosTotal();

		$candidatos = array();
		$cont = 0;
		$conselho = '';

		foreach ($listaApuracao as $key) {
			if($conselho <> $key['cdt_nome_conselho']){ $cont++; $conselho = $key['cdt_nome_conselho'];}

				$candidatos[$cont][] = $key;
		}


		$bU['eleicao'] = $_SESSION['eleicao'];
		$bU['hora'] = $hora;
		$bU['listaApuracao'] = $candidatos;
		$pdf = new ControllerPdf();
		$pdf->emitirApuracaoGeralPorConselhos($bU,$documento,$brancosNulos);


	}

	/**
	 * ControllerUrna::definirPemissaoPasta()
	 * Seta as permissões necessárias na pasta de backup
   *
	 * @return void
	 */
	public function definirPemissaoPasta() {
		chmod($_SESSION['CAMINHO_MIRROR'], 0777);
		$dh = opendir($_SESSION['CAMINHO_MIRROR']);
		while (false !== ($filename = readdir($dh))) {
			if ($filename != '.' && $filename != '..') {
				chmod($_SESSION['CAMINHO_MIRROR'] . $filename, 0777);
			}
		}
		echo 'ok';
	}

	/**
	 * ControllerUrna::emitirBoletimFinal()
	 * Emite o boletim de urna
   *
	 * @return void
	 */
	public function emitirBoletimFinal() {
		$modelCandidato = new ModelCandidato();
		$listaCandidatos = $modelCandidato->listaVotosCandidatoApuracao();
		$modelVoto = new ModelVoto();
		$hora = date("Y-m-d H:i:s");
		$apuracaoFinal['eleicao'] = $_SESSION['eleicao'];
		$apuracaoFinal['hora'] = $hora;
		$apuracaoFinal['listaCandidatos'] = $listaCandidatos;
		$apuracaoFinal['listaQtdVotos'] = $modelVoto->listaQtdVotosApuracao($urna);
		$modelUrna = new ModelUrna();
		$pdf = new ControllerPdf();
		$pdf->impressaoApuracaoFinal($apuracaoFinal);
	}

	/**
	 * ControllerUrna::acessibilidade()
	 * Ativa ou desatica a assebilidade na hora da votação
   *
	 * @return void
	 */
	public function acessibilidade() {
		if ($_SESSION['acessibilidade']) {
			unset($_SESSION['acessibilidade']);
			echo trim('acessibilidadepb.jpg');
		} else {
			$_SESSION['acessibilidade'] = true;
			echo trim('acessibilidade.jpg');
		}
	}

	/**
	 * ControllerUrna::verificarSeExisteBanco()
	 * Verifica se existe base de dados na máquina
	 * @param mixed $banco
	 * @return
	 */
	public function verificarSeExisteBanco($banco) {
		$modelUrna = new ModelUrna();
		return $modelUrna->verificarSeExisteBanco($banco);
	}

	/**
	 * ControllerUrna::backupDataBase()
	 * Realiza o backup da base de dados
   *
	 * @param mixed $base
	 * @return
	 */
	public function backupDataBase($base) {
		$modelUrna = new ModelUrna();
		return $modelUrna->gerarBackUpDatabase($base);
	}

	/**
	 * ControllerUrna::dropDatabase()
	 * Deleta a base de dados
   *
	 * @param mixed $base
	 * @return
	 */
	public function dropDatabase($base) {
		$modelUrna = new ModelUrna();
		return $modelUrna->dropDatabase($base);
	}

	/**
	 * ControllerUrna::createDatabase()
	 * Cria uma nova base de dados
   *
	 * @param mixed $base
	 * @return
	 */
	public function createDatabase($base) {
		$modelUrna = new ModelUrna();
		return $modelUrna->createDatabase($base);
	}

	/**
	 * ControllerUrna::restoreDatabase()
	 * REstaura a base de dados
   *
	 * @param mixed $base
	 * @return
	 */
	public function restoreDatabase($base) {
		$modelUrna = new ModelUrna();
		return $modelUrna->restoreDatabase($base);
	}

	/**
	 * ControllerUrna::votantesUrna()
	 * Retorna o número de votantes na urna
   *
	 * @return
	 */
	public function votantesUrna() {
		$modelUrna = new ModelUrna();
		return $modelUrna->consultarVontantesUrna();
	}

	/**
	 * ControllerUrna::statusUrna()
	 *
	 * @return void
	 */
	public function statusUrna() {
		if (BD_TYPE == 'sqlsrv') {
			$modelUrna = new ModelUrna();
			$gravaMirror = $modelUrna->gravarParaDetectarBanco(BD_BASE_MIRROR);
			$statusMirror = $modelUrna->verificarSeExisteBanco(BD_BASE_MIRROR);
			$return = "true";
			$file = $_SESSION['eleicao']['id'] . '_' . $_SESSION['eleicao']['zona'] . '-' .
				$_SESSION['eleicao']['secao'] . '.txt';
			$handle = fopen(ORIGEM_STATUS . $file, "w+");
			fwrite($handle, $return);
			fclose($handle);
			$controllerPdf = new ControllerPdf();
			$controllerPdf->transmissaoDados(ORIGEM_STATUS, DESTINO_STATUS, $file);
		} elseif (BD_TYPE == 'sqlite') {
			$return = is_dir($caminho_mirror);
			if ($return != 1) {
				$_SESSION['erroDispositivoExterno'] = true;
			} else {
				if ($_SESSION['erroDispositivoExterno']) {
					$controllerEleicao = new ControllerEleicao();
					if ($controllerEleicao->copydataBases()) {
						$_SESSION['erroDispositivoExterno'] = '';
						unset($_SESSION['erroDispositivoExterno']);
					}
				}
			}
		}
		echo $return;
	}

  public function buscarUrna() {

    $modelUrna = new ModelUrna();
		$urna = $modelUrna->buscarUrna($_POST['urna']);

    exit(json_encode($urna));

  }

	/**
	 * ControllerUrna::restartServer()
	 * Reinicia o servidor
   *
	 * @return void
	 */
	public function restartServer() {
		var_dump(exec(CAMINHO_PORTAL . 'batch/restartServer.bat'));
	}
	public function releaseMesarioAjax(){
		
		$func = new Functions();
		$func->releaseScreen();
	
	}
}
