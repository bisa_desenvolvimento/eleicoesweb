<?php

include_once ("lib/MVC/Controller.php");
include_once ("lib/MVC/View.php");
include_once ("model/eleitor/ModelEleitor_" . BD_TYPE . ".php");
include_once ("model/urna/ModelUrna_" . BD_TYPE . ".php");
include_once ("controller/eleicao/ControllerEleicao.php");

/**
 * ControllerEleitor
 * 
 */
class ControllerEleitor extends Controller {

	/**
	 * Método padrão que será chamado caso nenhum método seja especificado na classe.
	 * Enviará o usuário para o menu da urna.
	 * 
	 * @return void
	 */
	public function acaoPadrao($parametros) {
		$this->procurarEleitor;
	}

	/**
	 * Busca pelo eleitor para confirmação da liberação do mesmo. E informa caos ele 
	 * já tenha voltado ou não tenha sido encontrado.
	 * 
	 * @return string eleitor
	 */
	public function procurarEleitor() {
		$controlleEleicao = new ControllerEleicao();
		$filePrincipal = CAMINHO_PORTAL . 'banco/' . BD_BASE . '.db';
		if (!$controlleEleicao->existDatabase($filePrincipal)) {
			$controlleEleicao->copydataBases();
		}
		$oab = str_pad($_POST['oabEleitor'], CARACTERES_DOCUMENTO, 0, STR_PAD_LEFT);
		$modelEleitor = new ModelEleitor();
		$eleitor = $modelEleitor->procurarEleitor($oab);
		if (!$eleitor) {
			echo 'NE';
		} else {
			if ($eleitor['elt_votou']) {
				echo 'JV';
			} else {
				echo utf8_encode(utf8_decode($eleitor['elt_nome']));
				$_SESSION['eleitor'] = $oab;
			}
		}
		die;
	}

	/**
	 * Exibe a lista de eleitora por urna
	 * 
	 * @return void
	 */
	public function listaEleitores() {
		$modelUrna = new ModelUrna();
		$listaUrna = $modelUrna->listaUrna();
		$modelEleitor = new ModelEleitor();
		include_once ("conf/functions.inc.php");
		$func = new Functions();
		foreach ($listaUrna as $listaUrna) {
			$objEleitor = $modelEleitor->listaEleitores($listaUrna->zona, $listaUrna->secao);
			$objUrna[] = array(
				"zona" => $listaUrna->zona,
				"secao" => $listaUrna->secao,
				"cancelamento" => $func->removeNumeroLetra(sha1($listaUrna->zona . $listaUrna->
					secao), 1),
				"eleitores" => $objEleitor);
		}
		$data['listaEleitores'] = $objUrna;
		$view = new View();
		$view->tipo = 'IMPRESSAO';
		$view->data = $data;
		$view->carregar("eleicao/listaEleitores.html");
		$view->mostrar();
	}

	/**
	 * Exibe a lista de eleitora por urna
	 * 
	 * @return void
	 */
	public function addEleitores() {
		$view = new View();
		$view->tipo = 'PADRAO';
		$view->data = $data;
		$view->carregar("eleicao/addEleitores.html");
		$view->mostrar();
	}

	/**
	 * ControllerEleitor::processaAddEleitores()
	 * 
	 * @return void
	 */
	public function processaAddEleitores() {
		$eleitores = $_POST['eleitores'];
		$eleitores = explode("\n", $eleitores);
		$modelUrna = new ModelUrna();
		$listaUrna = $modelUrna->listaUrna();
		$modelEleitores = new ModelEleitor();
		$modelEleitores->zerarEleitores();
		for ($x = 0; $x < count($eleitores); $x++) {
			if (trim($eleitores[$x]) != '') {
				$objEleitor = (object)array();
				$objEleitor->num = 1000 + $x;
				$objEleitor->nome = trim($eleitores[$x]);
				$objEleitor->zona = $listaUrna[0]->zona;
				$objEleitor->secao = $listaUrna[0]->secao;
				$modelEleitores->AddEleitor($objEleitor);
			}
		}
		$this->listaEleitores();
	}

	/**
	 * Exibe a lista de eleitora por urna
	 * 
	 * @return void
	 */
	public function listaComprovantes() {
		$modelUrna = new ModelUrna();
		$listaUrna = $modelUrna->listaUrna();
		$modelEleitor = new ModelEleitor();
		include ("conf/functions.inc.php");
		$func = new Functions();
		foreach ($listaUrna as $listaUrna) {
			$objEleitor = $modelEleitor->listaEleitores($listaUrna->zona, $listaUrna->secao);
			$objUrna[] = array(
				"zona" => $listaUrna->zona,
				"secao" => $listaUrna->secao,
				"localvotacao" => $listaUrna->cidade,
				"eleitores" => $objEleitor);
		}
		$data['listaEleitores'] = $objUrna;
		$view = new View();
		$view->tipo = 'IMPRESSAO';
		$view->data = $data;
		$view->carregar("eleicao/listaEleitoresComprovante.html");
		$view->mostrar();
	}

}
