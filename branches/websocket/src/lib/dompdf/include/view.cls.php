<?
/**
* [Descri��o do arquivo].
*
* [mais informa��es precisa ter 1 [ENTER] para definir novo par�grafo]
*
* [pode usar quantas linhas forem necess�rias]
* [linhas logo abaixo como esta, s�o consideradas mesmo par�grafo]
*
* @package [Nome do pacote de Classes, ou do sistema]
* @category [Categoria a que o arquivo pertence]
* @name [Apelido para o arquivo]
* @author BisaWeb <bisa@bisa.com.br>
* @copyright [Informa��es de Direitos de C�pia]
* @license [link da licen�a] [Nome da licen�a]
* @link [link de onde pode ser encontrado esse arquivo]
* @version [Vers�o atual do arquivo]
* @since [Arquivo existe desde: Data ou Versao]
*/

class View
{
	private $html;
	private $data;
	private $tipo;
	
	public function __construct() {
		$this->tipo = 'INT';
		$this->html = '';
		$this->data = null;
	}
	
	public function carregar($template) {
		$this->html = CAMINHO_PORTAL.PASTA_VIEW.$template;
		if(!is_file($this->html)){
			throw new Exception( 'View n�o encontrada!' );
		}
	}
	
	// o mtodo __set  responsvel por setar o valor de uma propriedade.
	public function __set( $chave, $valor )
	{
		$this->{$chave} = $valor;
	}
	
	//o mtodo __get  responsvel por interceptar o retorno de uma propriedade. 
	public function __get( $chave )
	{
		return $this->{$chave};
	}
	
	//Os mtodos __isset e __unset interferem nas chamadas s funes isset() e unset() respectivamente.
	public function __isset( $chave )
	{
		return isset( $this->{$chave} );
	}

	public function __unset( $chave )
	{
		unset( $this->{$chave} );
	}
	        
	//O m�todo __call  chamado toda vez que um m�todo  chamado n�o � encontrado.
	public function __call( $nomeDoMetodo, $argumentos )
	{
		throw new Exception( 'View n�o encontrado!' );
	}
	
	public function mostrarCabecalho()
	{
		switch (strtoupper($this->tipo)) {
		    case 'CANDIDATO':
		        include(CAMINHO_PORTAL."templates/" . TEMPLATE . "/header-vazio.inc.php");
		        break;
		    case 'BRANCO':
		        include(CAMINHO_PORTAL."templates/" . TEMPLATE . "/header-branco.inc.php");
		        break;
		    case 'CINZA':
		        include(CAMINHO_PORTAL."templates/" . TEMPLATE . "/header-cinza.inc.php");
		        break;
		    case 'IMPRESSAO':
		        include(CAMINHO_PORTAL."templates/" . TEMPLATE . "/header-impressao.inc.php");
		        break;
		   default:
		        include(CAMINHO_PORTAL."templates/" . TEMPLATE . "/header-vazio.inc.php");
		        break;
		}
	}
	
	public function mostrarRodape()
	{
		switch (strtoupper($this->tipo)) {
		    case 'CANDIDATO':
                break;
		    case 'IMPRESSAO':
                break;
		    default:
		        include(CAMINHO_PORTAL."templates/" . TEMPLATE . "/footer.inc.php");
		        break;
		}
	}
	
	public function mostrar() {
	
		if($this->data)
			extract($this->data);
		
		extract($_SESSION);
		
		$this->mostrarCabecalho();
		include($this->html);
        $this->mostrarRodape();
        
	}
    
    public function redirecionar($url) {
        header('Location: '+$url);
	}
		
}

?>