<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Untitled Document</title>
	
    <script type="text/javascript" src="<?=URL_PORTAL."templates/" . TEMPLATE . "/"?>js/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="<?=URL_PORTAL."templates/" . TEMPLATE . "/"?>js/sawpf.1.0.js"></script>
    <script type="text/javascript" src="<?=URL_PORTAL."templates/" . TEMPLATE . "/"?>js/actions.js"></script>
	
    <link href="<?=URL_PORTAL."templates/" . TEMPLATE . "/"?>css/reset.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?=URL_PORTAL."templates/" . TEMPLATE . "/"?>css/screen.css" rel="stylesheet" type="text/css" media="all" />
    
</head>

<body>
    <div id="modal" style="display: none;">
    	<div class="sombra"></div>
        <div id="alert">
        	<div id="confirmar" >
            	<p class="numero" id="numeroCandidato"><strong></strong></p>
                <p class="nome" id="modMensagem"><strong></strong></p>
                <a href="javascript:;" id="modOk" class="modOk modAlert">ok</a>
                <a href="javascript:;" id="modConfirma" class="modConfirma modConfirm">confirma</a>
            	<a href="javascript:;" id="modCancela" class="modCancela modConfirm">cancela</a>
            </div><!-- /confirmar -->
        </div><!--/alert-->
    </div><!-- /modal -->


