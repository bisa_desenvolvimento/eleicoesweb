<?php

/**
 * [Classe básica de LocalVotacao].
 * 
 */

class LocalVotacao {

	/**
	 * identificador de localVotacao
	 * @access private
	 * @var integer
	 */
	private $id;

	/**
	 * Descrição do local de votação
	 * @access private
	 * @var string
	 */
	private $descricao;

	/**
	 * Endereço do local de votação 
	 * @access private
	 * @var string
	 */
	private $endereco;

	/**
	 * Cep do local de votação 
	 * 
	 * @access private
	 * @var string
	 */
	private $cep;

	/**
	 * Cidade do local de votação
	 * 
	 * @access private
	 * @var string
	 */
	private $cidade = null;

	/**
	 * Estado do local de votação
	 * 
	 * @access private
	 * @var string
	 */
	private $estado = null;

	/**
	 * Telefone do local de votação
	 * 
	 * @access private
	 * @var string
	 */
	private $telefone = null;

	/**
	 * Responsável pelo local de votação
	 * 
	 * @access private
	 * @var string
	 */
	private $responsavel = null;

	/**
	 * LocalVotacao::__construct()
	 * 
	 * @param mixed $id
	 * @param mixed $descricao
	 * @param mixed $endereco
	 * @param mixed $cep
	 * @param mixed $cidade
	 * @param mixed $estado
	 * @param mixed $telefone
	 * @param mixed $responsavel
	 * @return void
	 */
	public function __construct($id = null, $descricao = null, $endereco = null, $cep = null,
		$cidade = null, $estado = null, $telefone = null, $responsavel = null) {
		$this->id = $id;
		$this->descricao = $descricao;
		$this->endereco = $endereco;
		$this->cep = $cep;
		$this->cidade = $cidade;
		$this->estado = $estado;
		$this->telefone = $telefone;
		$this->responsavel = $responsavel;
	}

	/**
	 * View::__set()
	 * Responsável por setar o valor de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @param mixed $valor
	 * @return void
	 */
	public function __set($chave, $valor) {
		$this->{$chave} = $valor;
	}

	/**
	 * View::__get()
	 * Responsável por interceptar o retorno de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __get($chave) {
		return $this->{$chave};
	}

	/**
	 * View::__isset()
	 * Interfere nas chamadas à função isset()
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __isset($chave) {
		return isset($this->{$chave});
	}

	/**
	 * View::__unset()
	 * Interfere nas chamadas às função unset()
	 * 
	 * @param mixed $chave
	 * @return void
	 */
	public function __unset($chave) {
		unset($this->{$chave});
	}

	/**
	 * View::__call()
	 * É chamado toda vez que um método chamado não é encontrado.
	 * 
	 * @param mixed $nomeDoMetodo
	 * @param mixed $argumentos
	 * @return void
	 */
	public function __call($nomeDoMetodo, $argumentos) {
	}

}
