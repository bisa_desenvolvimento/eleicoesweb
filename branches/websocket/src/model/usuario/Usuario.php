<?php

/**
 * Classe básica de Usuário
 * 
 */

class Usuario {

	/**
	 * Identificador único do usuario
	 * 
	 * @access private
	 * @var integer
	 */
	private $id = null;

	/**
	 * Login do usuário 
	 *
	 * @access private
	 * @var string
	 */
	private $login = null;

	/**
	 * Perfil do usuário
	 * 
	 * @access private
	 * @var string
	 */
	private $perfil = null;
	/**
	 *  Senha do usuário
	 * 
	 * @access private
	 * @var string
	 */
	private $senha;

	/**
	 * Usuario::__construct()
	 * 
	 * @param mixed $id
	 * @param mixed $login
	 * @param mixed $senha
	 * @param mixed $perfil
	 * @return void
	 */
	public function __construct($id = null, $login = null, $senha = null, $perfil = null) {
		$this->id = $id;
		$this->login = $login;
		$this->senha = $senha;
		$this->perfil = $perfil;
	}

	/**
	 * View::__set()
	 * Responsável por setar o valor de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @param mixed $valor
	 * @return void
	 */
	public function __set($chave, $valor) {
		$this->{$chave} = $valor;
	}

	/**
	 * View::__get()
	 * Responsável por interceptar o retorno de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __get($chave) {
		return $this->{$chave};
	}

	/**
	 * View::__isset()
	 * Interfere nas chamadas à função isset()
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __isset($chave) {
		return isset($this->{$chave});
	}

	/**
	 * View::__unset()
	 * Interfere nas chamadas às função unset()
	 * 
	 * @param mixed $chave
	 * @return void
	 */
	public function __unset($chave) {
		unset($this->{$chave});
	}

	/**
	 * View::__call()
	 * É chamado toda vez que um método chamado não é encontrado.
	 * 
	 * @param mixed $nomeDoMetodo
	 * @param mixed $argumentos
	 * @return void
	 */
	public function __call($nomeDoMetodo, $argumentos) {
	}

}
