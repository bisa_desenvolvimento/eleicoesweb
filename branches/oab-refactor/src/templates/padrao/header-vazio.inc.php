<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Eleitor</title>
    
    <script type="text/javascript" src="<?=URL_PORTAL."templates/" . TEMPLATE . "/"?>js/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="<?=URL_PORTAL."templates/" . TEMPLATE . "/"?>js/sawpf.1.0.js"></script>
    <script type="text/javascript" src="<?=URL_PORTAL."templates/" . TEMPLATE . "/"?>js/actions.js"></script>
    
    <link href="<?=URL_PORTAL."templates/" . TEMPLATE . "/"?>css/reset.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?=URL_PORTAL."templates/" . TEMPLATE . "/"?>css/screen.css" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript">
        function UR_Start() 
        {
        	UR_Nu = new Date;
        	UR_Indhold = showFilled(UR_Nu.getHours()) + ":" + showFilled(UR_Nu.getMinutes()) + ":" + showFilled(UR_Nu.getSeconds());
        	document.getElementById("ur").innerHTML = UR_Indhold;
        	setTimeout("UR_Start()",1000);
        }
                
        function showFilled(Value) 
        {
        	return (Value > 9) ? "" + Value : "0" + Value;
        }
    </script>

</head>

<body onload="UR_Start()">


    <span id="ur"></span>