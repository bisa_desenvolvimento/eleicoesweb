	$(document).ready(function(){
        $("#modCancela, #modOk").click(
            function()
            {
                closeModal();
            }
        )
    });
    
    function modal(tipo,mensagem,classConfirma) {
        
        if(tipo == 'alert') {
            $('.modAlert').show();
            $('.modConfirm').hide();
        } else {
            $('.modAlert').hide();
            $('.modConfirm').show();
            $('#modConfirma').addClass(classConfirma)
        }
        
        $("#modMensagem").empty().html(mensagem)
        
        $('#modal').show();
        
    }
    
    
    function showModal(obj, data) {
		$("#modal").fadeIn(0);
        $("#all").fadeOut(0);
	}

    function showModal2(obj, data) {
        $("#modal2").fadeIn(0);
        $("#all").fadeOut(0);
    }

	function closeModal() {
		$("#modal").fadeOut(0);
		$("#all").fadeIn(0);
	}
    
    function closeModal2() {
        $("#modal2").fadeOut(0);
        $("#all").fadeIn(0);
    }

    function statusUrna()
    {
         $.ajax({
            url:'/urna/statusUrna',
            success: function(data) {
                if (data.trim() == 'false'){
                    $(".errorMirror").show();
                    $("#procurar").hide();
                    $("#encerrarVotacao").hide();
                } else {
                    $("#procurar").show();
                    $(".errorMirror").hide();
                    pegarHora();
                }
            }
        });    

    }

    function sleep(milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
          if ((new Date().getTime() - start) > milliseconds){
            break;
          }
        }
      }
