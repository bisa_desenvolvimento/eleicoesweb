/**
 * Created by eduardo on 23/02/16.
 */
function lockEleitor() {
    $.ajax({
        url: 'http://localhost:10000/screen',
        type: "POST",
        data: JSON.stringify({
            operation: 'focus',
            name: 'eleitor'
        })
    });
}

function lockMesario() {
    $.ajax({
        url: 'http://localhost:10000/screen',
        type: "POST",
        data: JSON.stringify({
            operation: 'focus',
            name: 'mesario'
        })
    });
}

function releaseScreen() {
    $.ajax({
        url: 'http://localhost:10000/screen',
        type: "POST",
        data: JSON.stringify({
            operation: 'release',
            name: 'mesario'
        })
    });
}
