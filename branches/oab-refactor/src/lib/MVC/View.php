<?php

/**
 * View
 */
class View {
	private $html;
	private $data;
	private $tipo;

	/**
	 * View::__construct()
	 * 
	 * @return void
	 */
	public function __construct() {
		$this->tipo = 'INT';
		$this->html = '';
		$this->data = null;
	}

	/**
	 * View::carregar()
	 * 
	 * @param mixed $template
	 * @return void
	 */
	public function carregar($template) {
		$this->html = CAMINHO_PORTAL . PASTA_VIEW . $template;
		if (!is_file($this->html)) {
			throw new Exception('View não encontrada: ' . $this->html);
		}
	}

	/**
	 * View::__set()
	 * Responsável por setar o valor de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @param mixed $valor
	 * @return void
	 */
	public function __set($chave, $valor) {
		$this->{$chave} = $valor;
	}

	/**
	 * View::__get()
	 * Responsável por interceptar o retorno de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __get($chave) {
		return $this->{$chave};
	}

	/**
	 * View::__isset()
	 * Interfere nas chamadas à função isset()
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __isset($chave) {
		return isset($this->{$chave});
	}

	/**
	 * View::__unset()
	 * Interfere nas chamadas às função unset()
	 * 
	 * @param mixed $chave
	 * @return void
	 */
	public function __unset($chave) {
		unset($this->{$chave});
	}

	/**
	 * View::__call()
	 * É chamado toda vez que um método chamado não é encontrado.
	 * 
	 * @param mixed $nomeDoMetodo
	 * @param mixed $argumentos
	 * @return void
	 */
	public function __call($nomeDoMetodo, $argumentos) {
		throw new Exception('View não encontrado!');
	}

	/**
	 * View::mostrarCabecalho()
	 * Define o cabeçalho respectivo ao tipo esoclhido.
	 * 
	 * @return void
	 */
	public function mostrarCabecalho() {
		switch (strtoupper($this->tipo)) {
			case 'CANDIDATO':
				include (CAMINHO_PORTAL . "templates/" . TEMPLATE . "/header-vazio.inc.php");
				break;
			case 'BRANCO':
				include (CAMINHO_PORTAL . "templates/" . TEMPLATE . "/header-branco.inc.php");
				break;
			case 'CINZA':
				include (CAMINHO_PORTAL . "templates/" . TEMPLATE . "/header-cinza.inc.php");
				break;
			case 'IMPRESSAO':
				include (CAMINHO_PORTAL . "templates/" . TEMPLATE . "/header-impressao.inc.php");
				break;
			case 'PADRAO':
				include (CAMINHO_PORTAL . "templates/" . TEMPLATE . "/header.inc.php");
				break;
			case 'ACESSIVEL':
				include (CAMINHO_PORTAL . "templates/" . TEMPLATE . "/header-vazio.inc.php");
				break;
			default:
				include (CAMINHO_PORTAL . "templates/" . TEMPLATE . "/header-vazio.inc.php");
				break;
		}
	}

	/**
	 * View::mostrarRodape()
	 * Define o rodapé respectivo ao tipo esoclhido.
	 * 
	 * @return void
	 */
	public function mostrarRodape() {
		switch (strtoupper($this->tipo)) {
			case 'CANDIDATO':
			case 'ACESSIVEL':
			case 'IMPRESSAO':
				break;
			default:
				include (CAMINHO_PORTAL . "templates/" . TEMPLATE . "/footer.inc.php");
				break;
		}
	}

	/**
	 * View::mostrar()
	 * Renderiza a página
	 * 
	 * @return void
	 */
	public function mostrar() {
		if ($this->data) extract($this->data);
		extract($_SESSION);
		$this->mostrarCabecalho();
		include ($this->html);
		$this->mostrarRodape();
	}

	/**
	 * View::redirecionar()
	 * Redireciona par aa url especificada
	 * 
	 * @param mixed $url
	 * @return void
	 */
	public function redirecionar($url) {
		header('Location: ' + $url);
	}

}
