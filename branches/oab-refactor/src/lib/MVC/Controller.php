<?php

/**
 * Controller
 */
class Controller {

	/**
	 * Controller::__construct()
	 * 
	 * @return void
	 */
	public function __construct() {

	}

	/**
	 * Controller::__call()
	 * O método __call  chamado toda vez que um método  chamado não é encontrado.
	 * 
	 * @param mixed $nomeDoMetodo
	 * @param mixed $argumentos
	 * @return void
	 */
	public function __call($nomeDoMetodo, $argumentos) {
		throw new Exception('Controller não encontrado! ' . $nomeDoMetodo);
	}
  
}
