<?php

require_once 'lib/dompdf/lib/class.pdf.php';
require_once ("lib/MVC/View.php");
require_once ("lib/MVC/Controller.php");
require_once 'lib/dompdf/dompdf_config.inc.php';
require_once ("conf/functions.inc.php");

/**
 * ControllerPDF
 * 
 */
class ControllerPdf {

	private $objPdf;
	private $html;

	/**
	 * Construtor
	 * 
	 * @param mixed $html
	 * @return void
	 */
	public function __construct($html = null) {
		$this->objPdf = new DOMPDF();
		$this->html = $html;
	}

	/**
	 * Imprime o pdf do beletim de urna
	 * 
	 * @param array dados da urna
	 * @return void
	 */
	public function gerarPDFBoletimUrna($bU) {
		ControllerUsuario::seLogado();
		try {
			for ($x = 1; $x < $_SESSION['eleicao']['qtdBu'] + 1; $x++) {
				$bU['eleicao']['qtd'] = $x;
				$impressao .= $this->getHtmlBu($bU);
			}
			$_SESSION['eleicao']['status'] = '5';
			$this->objPdf->load_html($this->getHtmlBu($bU));
			$this->objPdf->set_paper('letter', 'portrait');
			$this->objPdf->render();
			$this->objPdf->stream("Boletim_de_urna_" . $bu["eleicao"]["titulo"] . ".pdf");
		}
		catch (exception $e) {
			throw $e;
		}
	}

	/**
	 * Imprime o pdf do apuracao geral por conselho
	 * 
	 * @param array dados da urna
	 * @return void
	 */
	public function emitirApuracaoGeralPorConselhos($bU,$documento,$brancosNulos) {


		ControllerUsuario::seLogado();
		try {
			
			$impressao = $this->getHtmlApuracaoGeralPorConselho($bU,$documento,$brancosNulos);
			die($impressao);
			
			$this->objPdf->load_html($impressao);
			$this->objPdf->set_paper('letter', 'portrait');
			$this->objPdf->render();
			$this->objPdf->stream("apuracao_geral_por_conselho.pdf");
		}
		catch (exception $e) {
			throw $e;
		}
	}


	/**
	 * Gera o pdf do beletim de urna
	 * 
	 * @param array dados da urna
	 * @return void
	 */
	public function impressaoPdfBu($bU,$tipo) {

		$quantidade = $tipo == 'bu' ? $_SESSION['eleicao']['qtdBu'] : $_SESSION['eleicao']['qtdZeresima'];

		$view = new View();
		for ($x = 1; $x < $quantidade + 1; $x++) {
			$bU['eleicao']['qtd'] = $x;
			$impressao .= $this->getHtmlBu($bU,$tipo);
		}
		$_SESSION['eleicao']['pdfBu'] = true;
		include ("lib/encDec/classEncDec.php");
		$encDec = new classEncDec(KEY_ENCRYPT);
		$txtBu = $encDec->encrypt('comp') . '|' . $encDec->encrypt($bU["eleitores"]["qtdAptos"]) .
			'|' . $encDec->encrypt($bU["eleitores"]["qtdComparecimento"]) . "\r\n";
		for ($x = 0; $x < count($bU['listaCandidatos']); $x++) {
			$txtBu .= $encDec->encrypt($bU['listaCandidatos'][$x]['cdt_oab']) . '|' . $encDec->
				encrypt($bU['listaCandidatos'][$x]['totalVotos']) . '|' . $encDec->encrypt($bU['listaCandidatos'][$x]['car_id']) .
				"\r\n";
		}
		for ($x = 0; $x < count($bU['listaBrancoNulo']); $x++) {
			$txtBu .= $encDec->encrypt($bU['listaBrancoNulo'][$x]['vot_oabCandidato']) . '|' .
				$encDec->encrypt($bU['listaBrancoNulo'][$x]['totalVotos']) . '|' . $encDec->
				encrypt($bU['listaBrancoNulo'][$x]['vot_car_id']) . "\r\n";
		}
		$secaoZona = $encDec->encrypt($bU['eleicao']['zona']) . '.' . $encDec->encrypt($bU['eleicao']['secao']) .
			"\r\n";
		$file = $bU['eleicao']['zona'] . '-' . $bU['eleicao']['secao'] . ".txt";
		if ($bU['enviar'] == "true") {
			$handle = fopen(ORIGEM_APURACAO . $file, "w+");
			fwrite($handle, $secaoZona . $txtBu);
			fclose($handle);
			if (file_exists($_SESSION['CAMINHO_MIRROR'])) {
				$handle = fopen($_SESSION['CAMINHO_MIRROR'] . $file, "w+");
			} else {
				$handle = fopen(DESTINO_BACKUP . $file, "w+");
			}
			fwrite($handle, $secaoZona . $txtBu);
			fclose($handle);
			var_dump($this->transmissaoDados(ORIGEM_APURACAO, DESTINO_APURACAO, $file));
			exit();
		} else {
			$_SESSION['fileBu'] = $file;
			$_SESSION['eleicao']['status'] = '5';
			$this->objPdf->load_html($impressao);
			$data['impressao'] = $impressao;
			$view->data = $data;
			$view->tipo = 'IMPRESSAO';
			$view->carregar("impressao/impressao.phtml");
			$view->mostrar();
		}
	}

	/**
	 * Gera o pdf da Apuração Final
	 * 
	 * @param array dados da urna
	 * @return void
	 */
	public function impressaoApuracaoFinal($apuracaoFinal) {
		$view = new View();
		$x = 0;
		$apuracaoFinal['eleicao']['qtd'] = $x;
		$impressao .= $this->getHtmlApuracaoFinal($apuracaoFinal);
		$this->objPdf->load_html($impressao);
		$data['impressao'] = $impressao;
		$view->data = $data;
		$view->tipo = 'IMPRESSAO';
		$view->carregar("impressao/impressao.phtml");
		$view->mostrar();
	}

	/**
	 * Gera o html do boletim de urna a ser utilizado no PDF
	 * 
	 * @param array dados da urna
	 * @return html boletimUrna
	 */
	public function getHtmlBu($arrValores,$tipo) {

		$tituloDocumento = $tipo == 'bu' ? 'Boletim de Urna' : 'Zerésima';

		$func = new Functions();
		$carga = $arrValores['eleicao']['zona'] . $arrValores['eleicao']["secao"];
		$carga = $func->pontinho(md5($carga));
		$this->html = '
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>.: Boletim de Urna :.</title>
            </head>
            <body>
                <table width="200" border="0" style="font-family: sans-serif; font-size:12px" align="center">
                <tr>
                    <td colspan="3" align="center">' . $arrValores['eleicao']['qtd'] .
			'ª via</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">' . $tituloDocumento . '</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>			
                    <td colspan="3" align="center">' . $arrValores["eleicao"]["titulo"] .
			'</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Zona Eleitoral</td>
					
                    <td align="right">' . $arrValores["eleicao"]["zona"] .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Seção</td>
                    <td align="right">' . $arrValores["eleicao"]["secao"] .
			'</td>			
               </tr>
               <!--tr>
                    <td colspan="2">Urna</td>
                    <td align="right">' . $arrValores["eleicao"]["urn_numero"] .
			'</td>			
               </tr-->
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Eleitores Aptos</td>					
                    <td align="right">' . $func->montaZeros($arrValores["eleitores"]["qtdAptos"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Comparecimento</td>
                    <td align="right">' . $func->montaZeros($arrValores["eleitores"]["qtdComparecimento"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Faltosos</td>
                    <td align="right">' . $func->montaZeros($arrValores["eleitores"]["qtdFaltosos"]) .
			'</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Data </td>					
                    <td align="right">' . $func->convertDate($arrValores["eleicao"]["data"]) .
			'</td> 
                </tr>
                <tr>
                    <td colspan="2">Hora </td>
                    <td align="right">' . $func->removeData($arrValores["hora"]) .
			'</td> 			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">=============================</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Candidatos</td>
                </tr>';
		foreach ($arrValores["eleicao"]["cargos"] as $cargo) {
			$this->html .= '<tr>
                    <td colspan="3">&nbsp;</td>
                </tr><tr>
                        <td colspan="3" align="center">' . $cargo['car_nome'] .
				'</td>
                    </tr>';
			$this->html .= '<tr>
                        <td>Nº</td>
                        <td align="center">Nome do Candidato</td>
                        <td align="right">Votos</td>
                    </tr>';
			foreach ($arrValores['listaCandidatos'] as $candidato) {
				if ($candidato['car_id'] == $cargo['car_id']) {
					$this->html .= '<tr>';
					$this->html .= '<td>' . $candidato["cdt_numeroCandidatura"] . '</td>';
					$this->html .= '<td align="center">' . strip_tags($candidato["cdt_nome"]) .
						'</td>';
					$this->html .= '<td align="right">' . $func->montaZeros($candidato["totalVotos"]) .
						'</td>';
					$this->html .= '</tr>';
				}
			}
			/*if ($arrValores['listaBrancoNulo']) {
				foreach ($arrValores['listaBrancoNulo'] as $branconulo) {
					if (trim($branconulo['vot_car_id']) == $cargo['car_id']) {
						$this->html .= '<tr>';
						$this->html .= '<td>&nbsp;</td>';
						$this->html .= '<td align="center">' . $branconulo["cdt_nome"] . '</td>';
						$this->html .= '<td align="right">' . $func->montaZeros($branconulo["totalVotos"]) .
							'</td>';
						$this->html .= '</tr>';
					}
				}
			} else {
				$this->html .= '<tr>';
				$this->html .= '<td>&nbsp;</td>';
				$this->html .= '<td align="center">Branco</td>';
				$this->html .= '<td align="right">' . $func->montaZeros(0) . '</td>';
				$this->html .= '</tr>';
				$this->html .= '<tr>';
				$this->html .= '<td>&nbsp;</td>';
				$this->html .= '<td align="center">Nulo</td>';
				$this->html .= '<td align="right">' . $func->montaZeros(0) . '</td>';
				$this->html .= '</tr>';
			}*/
			/*echo '<pre>';
			var_dump($arrValores['listaQtdVotosCargo'][$cargo['car_id']-1]);*/


			$this->html .= '<tr>
                   
                </tr>
                <tr>
                    <td colspan="3" align="center">---------------------------------------------</td>
                </tr>
                <tr>
                    <td colspan="2">Total de votos Nominais</td>
					
                    <td align="right">' . $func->montaZeros($arrValores['listaQtdVotosCargo'][$cargo['car_id']-1]["totalVotosNominais"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Brancos</td>
                     <td align="right">' . $func->montaZeros($arrValores['listaQtdVotosCargo'][$cargo['car_id']-1]["totalVotosBrancos"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Nulos</td>
                    <td align="right">' . $func->montaZeros($arrValores['listaQtdVotosCargo'][$cargo['car_id']-1]["totalVotosNulos"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Total Apurados</td>
                     <td align="right">' . $func->montaZeros($arrValores['listaQtdVotosCargo'][$cargo['car_id']-1]["totalApurados"]) .
			'</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">=============================</td>
                </tr>
                ';




		}
		$this->html .= '<tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
               
                <tr>
                    <td colspan="2">Total de votos Nominais</td>
					
                    <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalVotosNominais"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Brancos</td>
                     <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalVotosBrancos"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Nulos</td>
                    <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalVotosNulos"]) .
			'</td>
                </tr>
				 <tr>
                    <td colspan="2">Cancelados</td>
                    <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalCancelamentos"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Total Apurados</td>
                     <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalApurado"]) .
			'</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">=============================</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Código de Identificação da Urna</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">';
		$this->html .= $carga;
		$this->html .= '<br /><br /></td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">Assinaturas: </td>
                </tr>
                </table>
           </body>
           </html>';
		if ($arrValores['eleicao']['qtd'] < $_SESSION['eleicao']['qtdBu']) {
			$this->html .= '<br style="page-break-before: always">';
		}
		return $this->html;
	}


	/**
	 * Gera o html do boletim de urna a ser utilizado no PDF
	 * 
	 * @param array dados da urna
	 * @return html boletimUrna
	 */
	public function getHtmlBuApuracao($arrValores) {
		//var_dump($arrValores['urna']->zona);die();

		$func = new Functions();
		$carga = $arrValores['urna']->zona . $arrValores['urna']->secao;
		$carga = $func->pontinho(md5($carga));
		$this->html = '
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>.: Espelho de Apuracao :.</title>
            </head>
            <body>
                <table width="200" border="0" style="font-family: sans-serif; font-size:12px" align="center">
                <tr>
                    <td colspan="3" align="center">1 via</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Espelho de Apuracao</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>			
                    <td colspan="3" align="center">' . $arrValores["eleicao"]["titulo"] .
			'</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Zona Eleitoral</td>
					
                    <td align="right">' . $arrValores['urna']->zona  .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Secao</td>
                    <td align="right">' .  $arrValores['urna']->secao .
			'</td>			
               </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Eleitores Aptos</td>					
                    <td align="right">' . $func->montaZeros($arrValores["eleitores"]["qtdAptos"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Comparecimento</td>
                    <td align="right">' . $func->montaZeros($arrValores["eleitores"]["qtdComparecimento"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Faltosos</td>
                    <td align="right">' . $func->montaZeros(($arrValores["eleitores"]["qtdAptos"] - $arrValores["eleitores"]["qtdComparecimento"])) .
			'</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Data </td>					
                    <td align="right">' . $func->convertDate($arrValores["eleicao"]["data"]) .
			'</td> 
                </tr>
                <tr>
                    <td colspan="2">Hora </td>
                    <td align="right">' . $func->removeData($arrValores["hora"]) .
			'</td> 			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">================================================</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Candidatos</td>
                </tr>';


          
		foreach ($arrValores["eleicao"]["cargos"] as $cargo) {
			$this->html .= '<tr>
                    <td colspan="3">&nbsp;</td>
                </tr><tr>
                        <td colspan="3" align="center">' . $cargo['car_nome'] .
				'</td>
                    </tr>';

			$this->html .= '<tr>
                        <td>N</td>
                        <td align="center">Nome do Candidato</td>
                        <td align="right">Votos</td>
                    </tr>';

			foreach ($arrValores['listaCandidatos'] as $candidato) {
				if ($candidato['car_id'] == $cargo['car_id']) {
					$this->html .= '<tr>';
					$this->html .= '<td>' . $candidato["cdt_numeroCandidatura"] . '</td>';
					$this->html .= '<td align="center">' . strip_tags($candidato["cdt_nome"]) .
						'</td>';
					$this->html .= '<td align="right">' . $func->montaZeros($candidato["totalVotos"]) .
						'</td>';
					$this->html .= '</tr>';
				}
			}


			if ($arrValores['listaBrancoNulo']) {
				foreach ($arrValores['listaBrancoNulo'] as $branconulo) {
					if (trim($branconulo['vot_car_id']) == $cargo['car_id']) {
						$this->html .= '<tr>';
						$this->html .= '<td>&nbsp;</td>';
						$this->html .= '<td align="center">' . $branconulo["cdt_nome"] . '</td>';
						$this->html .= '<td align="right">' . $func->montaZeros($branconulo["totalVotos"]) .
							'</td>';
						$this->html .= '</tr>';
					}
				}
			} else {
				$this->html .= '<tr>';
				$this->html .= '<td>&nbsp;</td>';
				$this->html .= '<td align="center">Branco</td>';
				$this->html .= '<td align="right">' . $func->montaZeros(0) . '</td>';
				$this->html .= '</tr>';
				$this->html .= '<tr>';
				$this->html .= '<td>&nbsp;</td>';
				$this->html .= '<td align="center">Nulo</td>';
				$this->html .= '<td align="right">' . $func->montaZeros(0) . '</td>';
				$this->html .= '</tr>';
			}
		}


		$this->html .= '<tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">=============================</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Total Apurados</td>
                     <td align="right">' . $func->montaZeros($arrValores["eleitores"]["qtdComparecimento"]) .
			'</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">=============================</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Codigo de Identificacao da Urna</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">';
		$this->html .= $carga;
		$this->html .= '<br /><br /></td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">Assinaturas: </td>
                </tr>
                </table>
           </body>
           </html>';


		if ($arrValores['eleicao']['qtd'] < $_SESSION['eleicao']['qtdBu']) {
			$this->html .= '<br style="page-break-before: always">';
		}
		return $this->html;
	}

		/**
	 * Gera o html do boletim de urna a ser utilizado no PDF
	 * 
	 * @param array dados da urna
	 * @return html boletimUrna
	 */
	/*public function getHtmlBuApuracaoGerino($arrValores) {

		$func = new Functions();
		$carga = $arrValores['eleicao']['zona'] . $arrValores['eleicao']["secao"];
		$carga = $func->pontinho(md5($carga));
		$this->html = '
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>.: Boletim de Urna :.</title>
            </head>
            <body>
                <table width="200" border="0" style="font-family: sans-serif; font-size:12px" align="center">
                <tr>
                    <td colspan="3" align="center">1ª via</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Apuração Parcial</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>			
                    <td colspan="3" align="center">Resultado Parcial dos Candidatos ao MOMO 2015</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Zona Eleitoral</td>
					
                    <td align="right">' . $arrValores["eleicao"]["zona"] .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Seção</td>
                    <td align="right">' . $arrValores["eleicao"]["secao"] .
			'</td>			
               </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Eleitores Aptos</td>					
                    <td align="right">16</td>
                </tr>
                <tr>
                    <td colspan="2">Comparecimento</td>
                    <td align="right">16</td>
                </tr>
                <tr>
                    <td colspan="2">Faltosos</td>
                    <td align="right">0</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Data </td>					
                    <td align="right">' . $func->convertDate($arrValores["eleicao"]["data"]) .
			'</td> 
                </tr>
                <tr>
                    <td colspan="2">Hora </td>
                    <td align="right">' . $func->removeData($arrValores["hora"]) .
			'</td> 			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">================================================</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Candidatos</td>
                </tr>';


          
		
			$this->html .= '<tr>
                    <td colspan="3">&nbsp;</td>
                </tr><tr>
                        <td colspan="3" align="center">' . $cargo['car_nome'] .
				'</td>
                    </tr>';

			$this->html .= '<tr>
                        <td>Nº</td>
                        <td align="center">Nome do Candidato</td>
                        <td align="right">Kg</td>
                    </tr>';


					$this->html .= '<tr>';
					$this->html .= '<td>119111</td>';
					$this->html .= '<td align="center">Ronaldo</td>';
					$this->html .= '<td align="right">2 kg</td>';
					$this->html .= '</tr>';

					$this->html .= '<tr>';
					$this->html .= '<td>242469</td>';
					$this->html .= '<td align="center">Andre</td>';
					$this->html .= '<td align="right">1,8 kg</td>';
					$this->html .= '</tr>';


					$this->html .= '<tr>';
					$this->html .= '<td>119111</td>';
					$this->html .= '<td align="center">Gustavo</td>';
					$this->html .= '<td align="right">1,6 kg</td>';
					$this->html .= '</tr>';

					$this->html .= '<tr>';
					$this->html .= '<td>119111</td>';
					$this->html .= '<td align="center">Jorge</td>';
					$this->html .= '<td align="right">1,5 kg</td>';
					$this->html .= '</tr>';

					$this->html .= '<tr>';
					$this->html .= '<td>545458</td>';
					$this->html .= '<td align="center">Ruth Freitas</td>';
					$this->html .= '<td align="right">1,5 kg</td>';
					$this->html .= '</tr>';

					$this->html .= '<tr>';
					$this->html .= '<td>111111</td>';
					$this->html .= '<td align="center">Edvaldo</td>';
					$this->html .= '<td align="right">1,3 kg</td>';
					$this->html .= '</tr>';

					$this->html .= '<tr>';
					$this->html .= '<td>787878</td>';
					$this->html .= '<td align="center">Izabelle</td>';
					$this->html .= '<td align="right">1,2 kg</td>';
					$this->html .= '</tr>';

					$this->html .= '<tr>';
					$this->html .= '<td>696969</td>';
					$this->html .= '<td align="center">Jorginho</td>';
					$this->html .= '<td align="right">1,0 kg</td>';
					$this->html .= '</tr>';

					$this->html .= '<tr>';
					$this->html .= '<td>101010</td>';
					$this->html .= '<td align="center">Gerino</td>';
					$this->html .= '<td align="right">1,0 kg</td>';
					$this->html .= '</tr>';

					$this->html .= '<tr>';
					$this->html .= '<td>444444</td>';
					$this->html .= '<td align="center">Anderson</td>';
					$this->html .= '<td align="right">0,9 kg</td>';
					$this->html .= '</tr>';

					$this->html .= '<tr>';
					$this->html .= '<td>555555</td>';
					$this->html .= '<td align="center">Claudilene</td>';
					$this->html .= '<td align="right">0,7 kg</td>';
					$this->html .= '</tr>';

					$this->html .= '<tr>';
					$this->html .= '<td>797979</td>';
					$this->html .= '<td align="center">Lais</td>';
					$this->html .= '<td align="right">0,6 kg</td>';
					$this->html .= '</tr>';

					$this->html .= '<tr>';
					$this->html .= '<td>858585</td>';
					$this->html .= '<td align="center">Thiago</td>';
					$this->html .= '<td align="right">0,5 kg</td>';
					$this->html .= '</tr>';

					$this->html .= '<tr>';
					$this->html .= '<td>656565</td>';
					$this->html .= '<td align="center">Eduardo Bisa</td>';
					$this->html .= '<td align="right">0,2 kg</td>';
					$this->html .= '</tr>';

					$this->html .= '<tr>';
					$this->html .= '<td>363636</td>';
					$this->html .= '<td align="center">Eduardo Brasilia</td>';
					$this->html .= '<td align="right">0,2 kg</td>';
					$this->html .= '</tr>';

					$this->html .= '<tr>';
					$this->html .= '<td>656565</td>';
					$this->html .= '<td align="center">Carol</td>';
					$this->html .= '<td align="right">0,1 kg</td>';
					$this->html .= '</tr>';

			





			

		$this->html .= '<tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">=============================</td>
                </tr>
                <tr>
                    <td colspan="2">Total de votos Nominais</td>
					
                    <td align="right">00016</td>
                </tr>
                <tr>
                    <td colspan="2">Brancos</td>
                     <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalVotosBrancos"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Nulos</td>
                    <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalVotosNulos"]) .
			'</td>
                </tr>
				 <tr>
                    <td colspan="2">Cancelados</td>
                    <td align="right">00001</td>
                </tr>
                <tr>
                    <td colspan="2">Total Apurados</td>
                     <td align="right">00016</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">=============================</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Código de Identificação da Urna</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">';
		$this->html .= $carga;
		$this->html .= '<br /><br /></td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">Assinaturas: </td>
                </tr>
                </table>
           </body>
           </html>';

           die($this->html);
		if ($arrValores['eleicao']['qtd'] < $_SESSION['eleicao']['qtdBu']) {
			$this->html .= '<br style="page-break-before: always">';
		}
		return $this->html;
	}*/
	/**
	 * Gera o html do boletim de urna a ser utilizado no PDF
	 * 
	 * @param array dados da urna
	 * @return html boletimUrna
	 */
	public function getHtmlApuracaoGeralPorConselho($arrValores,$documento,$brancosNulos) {
		
		$votosGerais = 0;		
		
		if($documento == 1){
			$documento = 'Zerezima';
		}else{
			$documento = 'Apuracao por conselho';
		}

		$this->html = '
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>.:Apuracao por Conselho:.</title>
            </head>
            <body>';


		
		foreach ($arrValores['listaApuracao'] as $conselho) {
		

        $this->html .= '<table width="200" border="0" style="font-family: sans-serif; font-size:12px" align="center">
        		<tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                 <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
			1 via</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <th colspan="3" align="center">'.$documento.'</th>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>			
                    <th colspan="3" align="center">' . $arrValores["eleicao"]["titulo"] .
			'</th>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>

          
                <tr>
                    <td colspan="2">Data </td>					
                    <td align="right">' . date("d/m/Y") .
			'</td> 
                </tr>
                <tr>
                    <td colspan="2">Hora </td>
                    <td align="right">' .date("H:i:s").
			'</td> 			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>';
                
        $this->html .= '<tr>
                    <td colspan="3" align="center">==============================================</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <th colspan="3" align="center">'.$conselho[0]['cdt_nome_conselho'].'</th>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <th colspan="3" align="center">Candidatos</th>
                </tr>
                 <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>

        		<tr>
                    <th>Numero</th>
                    <th align="center">Nome do Candidato</th>
                    <th align="right">Votos</th>
                </tr>';
                $totalVotos = 0;

                foreach ($conselho as $candidato) { 

		if($candidato['cdt_oab'] != '99999'){
			if($candidato['cdt_oab'] != 0){

	                $this->html .='<tr>
					                    <td>'.$candidato['cdt_oab'].'</td>
					                    <td align="center">'.$candidato['cdt_nome'].'</td>
					                    <td align="right">'.$candidato['total'].'</td>
					                </tr>';
				    $totalVotos += $candidato['total'];
			}
                }
		}
         

		$this->html .= '<tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">------------------------------------------------------</td>
                </tr>
                 <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Total de votos Nominais</td>
					
                    <td align="right">'.$totalVotos.'</td>
                </tr>
				
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">------------------------------------------------------</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                 <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
               
                <tr>
                    <td colspan="3">Assinaturas: </td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                 <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">==============================================</td>
                </tr>
                </table>
                <br style="page-break-before: always">';

		$votosGerais += $totalVotos;
		}
				
		$this->html .='<table width="200" border="0" style="font-family: sans-serif; font-size:12px" align="center">
				<tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <th colspan="3" align="center">Total Brancos e Nulos Eleicao</th>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                 <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Total de votos '.$brancosNulos[0]['cdt_nome'].'</td>
					
                    <td align="right">'.$brancosNulos[0]['votos'].'</td>
                </tr>
                 <tr>
                    <td colspan="2">Total de votos '.$brancosNulos[1]['cdt_nome'].'</td>
					
                    <td align="right">'.$brancosNulos[1]['votos'].'</td>
                </tr>
<tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                 <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
<tr>
                    <td colspan="2">Total de votos eleicao.</td>
					
                    <td align="right">'.$votosGerais.'</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">==============================================</td>
                </tr>
                </table>';


           $this->html .='</body>
           </html>';
	
		return $this->html;
	}

	/**
	 * Gera o html da apuração final a ser utilizado no PDF
	 * 
	 * @param array dados da urna
	 * @return html boletimUrna
	 */
	public function getHtmlApuracaoFinal($arrValores) {
		$func = new Functions();
		$carga = $arrValores['eleicao']['zona'] . $arrValores['eleicao']["secao"];
		$carga = $func->pontinho(md5($carga));
		if ($arrValores['listaCandidatos'] < 0) {
			die("Erro na impressão de Zeresima -- Verifique a lista de Candidatos na urna!");
		}
		if ($arrValores['eleicao']['qtd'] > 1) {
			$this->html = '<br style="page-break-before: always" />';
		}
		$this->html .= '
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>.: Boletim de Urna :.</title>
            </head>
            <body>
                <table width="200" border="0" style="font-family: sans-serif; font-size:12px" align="center">
                <tr>
                    <td colspan="3" align="center"><img src="' . URL_PORTAL .
			'templates/' . TEMPLATE . '/img/logo/' . LOGO . '" /></td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Apuração Final</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>				
                    <td colspan="3" align="center">' . $arrValores["eleicao"]["titulo"] .
			'</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Eleitores Aptos</td>					
                    <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalAptos"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Comparecimento</td>
                    <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalVotantes"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Faltosos</td>
                    <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalFaltosos"]) .
			'</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Data </td>					
                    <!--<td align="right">' . $func->convertDate($arrValores["eleicao"]["data"]) .
			'</td>--> 
                    <td align="right">' . $func->convertDate(date("Y-m-d")) .
			'</td> 
                </tr>
                <tr>
                    <td colspan="2">Hora </td>
                    <td align="right">' . $func->removeData($arrValores["hora"]) .
			'</td> 			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">=============================</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Candidatos</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>';
		foreach ($arrValores["eleicao"]["cargos"] as $cargo) {
			$this->html .= '<tr>
                        <td colspan="3" align="center">' . $cargo['car_nome'] .
				'</td>
                    </tr>';
			$this->html .= '<tr>
                        <td>Nº</td>
                        <td align="center">Nome do Candidato</td>
                        <td align="right">Votos</td>
                    </tr>';
			foreach ($arrValores['listaCandidatos'] as $candidato) {
				if (trim($candidato['vot_car_id']) == trim($cargo['car_id'])) {
					if ($candidato['cdt_oab'] == BRANCO_NUMERO || $candidato['cdt_oab'] ==
						NULO_NUMERO) {
						$numero = '';
					} else {
						$numero = $candidato['cdt_oab'];
					}
					$this->html .= '<tr>';
					$this->html .= '<td>' . $numero . '</td>';
					$this->html .= '<td align="center">' . $candidato['cdt_nome'] . '</td>';
					$this->html .= '<td align="right">' . $func->montaZeros($candidato['totalVotos']) .
						'</td>';
					$this->html .= '</tr>';
				}
			}
			$this->html .= '<tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>';
		}
		$this->html .= '
                <tr>
                    <td colspan="3" align="center">=============================</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Total de Votos Nominais</td>					
                    <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalVotosNominais"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Brancos</td>
                     <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalVotosBrancos"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Nulos</td>
                    <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalVotosNulos"]) .
			'</td>
                </tr>
				 <!--tr>
                    <td colspan="2">Cancelados</td>
                    <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalCancelamentos"]) .
			'</td>
                </tr-->
                <tr>
                    <td colspan="2">Total Apurados</td>
                     <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalApurado"]) .
			'</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">===========================================================</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center"><img src="' . URL_PORTAL .
			'templates/' . TEMPLATE . '/img/logo/logoBisa.png" width="75" /></td>
                </tr>
                </table>
           </body>
           </html>';
		return $this->html;
	}

	/**
	 * Imprime o pdf da zerésima
	 * 
	 * @param array dados da urna
	 * @return void
	 */
	public function gerarPDFZeresima($zeresima) {
		ControllerUsuario::seLogado();
		try {
			$view = new View();
			for ($x = 1; $x < $_SESSION['eleicao']['qtdZeresima'] + 1; $x++) {
				$x = 1;
				$zeresima['eleicao']['qtd'] = $x;
				$impressao .= $this->getHtmlZeresima($zeresima);
			}
			$_SESSION['eleicao']['status'] = '2';
			$this->objPdf->load_html($impressao);
			$this->objPdf->set_paper('letter', 'portrait');
			$this->objPdf->render();
			$this->objPdf->stream("Zeresima_" . $zeresima["eleicao"]["titulo"] . ".pdf");
		}
		catch (exception $e) {
			throw $e;
		}
	}

	/**
	 * Gera o pdf da zerésima
	 * 
	 * @param array dados da urna
	 * @return void
	 */
	public function impressaoPdfZeresima($zeresima) {
		$view = new View();
		for ($x = 1; $x < $_SESSION['eleicao']['qtdZeresima'] + 1; $x++) {
			$zeresima['eleicao']['qtd'] = $x;
			$impressao .= $this->getHtmlZeresima($zeresima);
		}
		$_SESSION['eleicao']['pdfZeresima'] = true;
		$this->objPdf->load_html($impressao);
		$data['impressao'] = $impressao;
		$_SESSION['eleicao']['status'] = '2';
		$view->data = $data;
		$view->tipo = 'IMPRESSAO';
		$view->carregar("impressao/impressao.phtml");
		$view->mostrar();
	}


	/**
	 * Gera o html da zerésima a ser utilizado no PDF
	 * 
	 * @param array dados da urna
	 * @return html zeresima
	 */
	public function getHtmlZeresima($arrValores) {   echo '<pre>'; var_dump($arrValores);
		$func = new Functions();
		$carga = $_SESSION['eleicao']['zona'] . $_SESSION['eleicao']["secao"];
		$carga = $func->pontinho(md5($carga));
		$this->html = '
            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">			
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>.: Zerésima :.</title>
            </head>
            <body>
                <table width="200" border="0" style="font-family: sans-serif; font-size:12px" align="center">
                <tr>
                    <td colspan="3" align="center">' . $arrValores['eleicao']['qtd'] .
			'ª via</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Zerésima</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>				
                    <td colspan="3" align="center">' . $arrValores["eleicao"]["titulo"] .
			'</td>			
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Zona Eleitoral</td>
					
                    <td align="right">' . $arrValores["eleicao"]["zona"] .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Seção</td>
                    <td align="right">' . $arrValores["eleicao"]["secao"] .
			'</td>			
                </tr>
				<!--tr>
                    <td colspan="2">Urna</td>
                    <td align="right">' . $arrValores["eleicao"]["urn_numero"] .
			'</td>			
               </tr-->
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Eleitores Aptos</td>					
                    <td align="right">00' . $arrValores["qtdEleitoresAptos"] .
			'</td>			
                </tr>
                <tr>
                    <td colspan="2">Comparecimento</td>					
                    <td align="right">00' . $arrValores["eleitores"][0]["qtdComparecimento"] .
			'</td>			
                </tr> 
                <tr>
                    <td colspan="2">Faltosos</td>					
                    <td align="right">00' . $arrValores["eleitores"][0]["qtdFaltosos"] .
			'</td>			
                </tr>               
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">Data </td>
                    <td align="right">' . $func->convertDate($arrValores["eleicao"]["data"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Hora </td>
                    <td align="right">' . $func->removeData($arrValores["hora"]) .
			'</td> 
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">=============================</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Candidatos</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>';
		$cargo = $arrValores["listaCandidatos"][0]->cargoNome;
		$xInit = 0;
		if (count($arrValores["listaCandidatos"]) > 0) {
			foreach ($arrValores["listaCandidatos"] as $candidato) {
				$atual = $cargo;
				$cargo = $candidato['car_nome'];
				if ($xInit == 0 || $cargo != $atual) {
					if ($xInit > 0) {
						$this->html .= '<tr>
                            <td colspan="3">&nbsp;</td>
                        </tr>';
					}
					$this->html .= '<tr>
                        <td colspan="3" align="center">' . $candidato['car_nome'] .
						'</td>
                    </tr>';
					$this->html .= '<tr>
                        <td>Nº</td>
                        <td align="center">Nome do Candidato</td>
                        <td align="right">Votos</td>
                    </tr>';
				}
				$this->html .= '<tr>';
				$this->html .= '<td>' . $candidato["cdt_numeroCandidatura"] . '</td>';
				$this->html .= '<td align="center">' . $candidato["cdt_nome"] . '</td>';
				$this->html .= '<td align="right">' . $func->montaZeros($candidato["totalVotos"]) .
					'</td>';
				$this->html .= '</tr>';
				$xInit++;
			}
		} else {
			die("Erro na impressão de Zeresima -- Verifique a lista de Candidatos na urna!");
		}
		$this->html .= '<tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">=============================</td>
                </tr>
                <tr>
                    <td colspan="2">Total de votos nominais</td>
                    <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalVotosNominais"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Brancos</td>
                     <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalVotosBrancos"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Nulos</td>
                    <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalVotosNulos"]) .
			'</td>
                </tr>
				 <tr>
                    <td colspan="2">Cancelados</td>
                    <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalCancelamentos"]) .
			'</td>
                </tr>
                <tr>
                    <td colspan="2">Total Apurados</td>
                     <td align="right">' . $func->montaZeros($arrValores["listaQtdVotos"]["totalApurado"]) .
			'</tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">=============================</td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Código de Identificação da Urna</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">';
		$this->html .= $carga;
		$this->html .= '<br /><br /></td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">Assinaturas: </td>
                </tr>';
		$this->html .= '     </table>';
		if ($arrValores['eleicao']['qtd'] < $_SESSION['eleicao']['qtdZeresima']) {
			$this->html .= '     <br style="page-break-before: always">';
		}
		$this->html .= '</body>
           </html>';
		return $this->html;
	}

	/**
	 * Envia o boletim de urna para o servidor por FTP
	 * 
	 * @param string $file caminho do arquivo
	 * @return boolean
	 */
	public function transmissaoDados($origem, $destino, $file) {
		$conn_id = ftp_connect(FTP_HOST, 21);
		if ($conn_id) {
			$ftpResult = ftp_login($conn_id, FTP_USER, FTP_PASS);
			ftp_pasv($conn_id, true);
			$envioArquivo = ftp_put($conn_id, $destino . $file, $origem . $file, FTP_ASCII);
			ftp_close($conn_id);
			return $envioArquivo;
		} else {
			return false;
		}
	}
}
