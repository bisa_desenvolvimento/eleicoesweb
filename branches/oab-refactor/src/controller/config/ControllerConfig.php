<?php

include_once ("lib/MVC/View.php");
include_once ("lib/MVC/Controller.php");
include_once ("model/config/ModelConfig_" . BD_TYPE . ".php");
include_once ("model/eleicao/ModelEleicao_" . BD_TYPE . ".php");
include_once ("model/eleitor/ModelEleitor_" . BD_TYPE . ".php");
include_once ("model/urna/ModelUrna_" . BD_TYPE . ".php");
include_once ("controller/usuario/ControllerUsuario.php");

/**
 * ControllerConfig
 * 
 */
class ControllerConfig extends Controller {

	/**
	 * ControllerConfig::acaoPadrao()
	 * Método padrão que será chamado caso nenhum método seja especificado na url.
	 * Chama o método menuConfig
	 * 
	 * @return void
	 */
	public function acaoPadrao() {
		$this->cargaUrna();
	}

	/**
	 * ControllerConfig::seLogado()
	 * 
	 * @return
	 */
	public static function seLogado() {
		include_once ("/conf/functions.inc.php");
		$func = new Functions();
		$data = date("Y-m-d H:i:s");
		$tempo = $func->timeDiff(isset($_SESSION['data_config']) ? $_SESSION['data_config'] :
			$data, date("Y-m-d H:i:s"));
		if (!isset($_SESSION['error_login_config'])) {
			$view = new View();
			$view->tipo = 'branco';
			$view->data = $data;
			$view->carregar("config/login.html");
			$view->mostrar();
			die();
		} elseif ($tempo < 0 || $tempo > 300) {
			$_SESSION['data_config'] = '';
			$_SESSION['error_login_config'] = '';
			unset($_SESSION['data_config']);
			unset($_SESSION['error_login_config']);
			if ($_SESSION['error_login_config'] != 'NULL') {
				$_SESSION['error_login_config'] =
					'Tempo excedido, entre em contato com a Comissão Eleitoral.';
			}
			$view = new View();
			$view->tipo = 'branco';
			$view->data = $data;
			$view->carregar("config/login.html");
			$view->mostrar();
			die();
		}
		return false;
	}

	/**
	 * ControllerConfig::cargaUrna()
	 * 
	 * @return
	 */
	public function cargaUrna() {
		ControllerConfig::seLogado();
		include_once (CAMINHO_PORTAL . 'conf/functions.inc.php');
		$func = new Functions();
		$view = new View();
		$modelEleicao = new ModelEleicao();
		$objEleicao = $modelEleicao->eleicao();
		$modelEleitor = new ModelEleitor();
		$cargaEleitor = $modelEleitor->listaEleitores($_SESSION['eleicao']['zona'], $_SESSION['eleicao']['secao']);
		if (count($cargaEleitor) == 0 || $cargaEleitor == '') {
			$data['carga'] = true;
		}
		$data['dataInicio'] = $func->convertDate($func->removeHora($objEleicao[0]['ele_horaInicio']));
		$data['horaInicio'] = $func->removeData($objEleicao[0]['ele_horaInicio']);
		$data['dataTermino'] = $func->convertDate($func->removeHora($objEleicao[0]['ele_horaTermino']));
		$data['horaTermino'] = $func->removeData($objEleicao[0]['ele_horaTermino']);
		$view->data = $data;
		$view->tipo = 'padrao';
		$view->carregar("config/config.html");
		$view->mostrar();
	}

	/**
	 * ControllerConfig::carregarDados()
	 * 
	 * @return
	 */
	public function carregarDados() {
		$modelConfig = new ModelConfig();
		$dados = $modelConfig->listaConfig();
		$dados = $dados[0];
		$conn_id = ftp_connect(FTP_HOST, 21);
		if ($conn_id) {
			$arrayArquivosApuracaoLocal = array($dados['urn_zona'] . '-' . $dados['urn_secao'] .
					'.sql', "create-" . $dados['pasta'] . ".sql");
			$conn_id = ftp_connect(FTP_HOST, 21);
			$ftpResult = ftp_login($conn_id, FTP_USER, FTP_PASS);
			if ($ftpResult) {
				$arquivos = ftp_nlist($conn_id, DESTINO_APURACAO . "../sql/" . $dados['pasta']);
				if (count($arquivos) > 0) {
					foreach ($arquivos as $list) {
						$file = end(explode("/", $list));
						if (in_array($file, $arrayArquivosApuracaoLocal)) {
							echo $list . "\n";
							$listFiles .= $file . ', ';
							$fp = fopen(ORIGEM_APURACAO . $file, 'w+');
							$ret = ftp_fget($conn_id, $fp, $list, FTP_ASCII, 0);
							fclose($fp);
							break;
						}
					}
					ftp_close($conn_id);
					echo $listFiles;
				} else {
					echo 'false';
				}
			} else {
				echo 'false';
			}
		} else {
			echo 'false';
		}
	}

	/**
	 * ControllerConfig::buscarArquivoImportacaoDados()
	 * 
	 * @param mixed $dados
	 * @return bool
	 */
	public function buscarArquivoImportacaoDados($dados) {
		$conn_id = ftp_connect(FTP_HOST, 21);
		if ($conn_id) {
			$arrayArquivosApuracaoLocal = array($dados['urn_zona'] . '-' . $dados['urn_secao'] .
					'.sql');
			$conn_id = ftp_connect(FTP_HOST, 21);
			$ftpResult = ftp_login($conn_id, FTP_USER, FTP_PASS);
			if ($ftpResult) {
				$arquivos = ftp_nlist($conn_id, DESTINO_APURACAO . "../sql/" . $dados['pasta']);
				if (count($arquivos) > 0) {
					foreach ($arquivos as $list) {
						$file = end(explode("/", $list));
						if (in_array($file, $arrayArquivosApuracaoLocal)) {
							$fp = fopen(ORIGEM_APURACAO . $file, 'w+');
							$ret = ftp_fget($conn_id, $fp, $list, FTP_ASCII, 0);
							fclose($fp);
							break;
						}
					}
				}
				ftp_close($conn_id);
				return $this->inserirDados($arrayArquivosApuracaoLocal);
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * ControllerConfig::inserirDados()
	 * 
	 * @param mixed $files
	 * @return bool
	 */
	public function inserirDados($files) {
		$modelConfig = new ModelConfig();
		for ($x = 0; $x < count($files); $x++) {
			$pointer = fopen(ORIGEM_APURACAO . $files[$x], 'r');
			while (!feof($pointer)) {
				$line = fgets($pointer, 4096);
				if (trim($line) != '') {
					$lines .= $line;
				}
			}
		}
		fclose($pointer);
		if ($modelConfig->executarLinha($lines)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ControllerConfig::gravaDadosEleicao()
	 * 
	 * @return void
	 */
	public function gravaDadosEleicao() {
		ControllerConfig::seLogado();
		include_once (CAMINHO_PORTAL . 'conf/functions.inc.php');
		$func = new Functions();
		$_POST['data_inicio'] = $func->convertDate($_POST['data_inicio']);
		$_POST['data_final'] = $func->convertDate($_POST['data_final']);
		$modelEleicao = new ModelEleicao();
		$modelEleicao->alterarDataEleicao($_POST);
		$this->cargaUrna();
	}

	/**
	 * ControllerConfig::acesso()
	 * 
	 * @return void
	 */
	public function acesso() {
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$modelUrna = new ModelUrna();
			$objUrna = $modelUrna->listaUrna();
			$_SESSION['eleicao']['zona'] = $objUrna[0]->zona;
			$_SESSION['eleicao']['secao'] = $objUrna[0]->secao;
			$codigo = $this->gerarCodigo();
			if (isset($_SESSION['error_login_config']) && $_SESSION['error_login_config'] == false) {
				$this->cargaUrna();
				die();
			} elseif ($codigo == strtoupper($_POST['senha'])) {
				include_once ("model/urna/ModelUrna_" . BD_TYPE . ".php");
				$modelUrna = new ModelUrna();
				$_SESSION['error_login_config'] = false;
				$_SESSION['data_config'] = date("Y-m-d H:i:s");
				sleep(1);
				$modelUrna->updateLibera();
				$this->cargaUrna();
				die();
			} else {
				$_SESSION['error_login_config'] = 'Dados inválidos.';
				$view = new View();
				$view->tipo = 'branco';
				$view->data = $data;
				$view->carregar("config/login.html");
				$view->mostrar();
				die();
			}
		} else {
			$this->acaoPadrao();
			die();
		}
	}

	/**
	 * ControllerConfig::gerarCodigo()
	 * 
	 * @param mixed $arr
	 * @return string $codigo
	 */
	public function gerarCodigo($arr) {
		include_once ("model/urna/ModelUrna_" . BD_TYPE . ".php");
		$modelUrna = new ModelUrna();
		$modelConfig = new ModelConfig();
		$objUrna = $modelUrna->listaUrna();
		if (is_object($arr)) {
			$zona = $arr->zona != '' ? $arr->zona : $objUrna[0]['urn_zona'];
			$secao = $arr->secao != '' ? $arr->secao : $objUrna[0]['urn_secao'];
			$id = is_numeric($arr->id) ? $arr->id : $objUrna[0]->liberaConfig;
		} else {
			$zona = $objUrna[0]->zona;
			$secao = $objUrna[0]->secao;
			$modelUrna = new ModelUrna();
			$id = $objUrna[0]->liberaConfig;
		}
		include_once (CAMINHO_PORTAL . 'conf/functions.inc.php');
		$func = new Functions();
		$objUrna = $modelUrna->listaUrna();
		$codigo = $func->gerarCodigoConfig(sha1($zona . $secao), $id);
		$tam = strlen($codigo);
		$arr = array();
		for ($x = 0; $x < $tam; $x++) {
			$arr[] = $codigo[$x];
		}
		sort($arr);
		$codigo = '';
		for ($x = 0; $x < count($arr); $x++) {
			$codigo .= strtoupper($arr[$x]);
		}
		return $codigo;
	}

	/**
	 * ControllerConfig::codigo()
	 * 
	 * @return void
	 */
	public function codigo() {
		$view = new View();
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$codidoIndetificao = sha1($_POST['zona'] . $_POST['secao']);
			$qtd = round(strlen($codidoIndetificao) / 4);
			$arr = (object)array();
			for ($x = 0; $x < $qtd; $x++) {
				$arr->zona = $_POST['zona'];
				$arr->secao = $_POST['secao'];
				$arr->id = $x;
				$codes[] = $this->gerarCodigo($arr);
			}
		}
		$data['codes'] = $codes;
		$data['zona'] = $_POST['zona'];
		$data['secao'] = $_POST['secao'];
		$view->data = $data;
		$view->tipo = 'padrao';
		$view->carregar("config/geraCodigo.html");
		$view->mostrar();
	}

}
