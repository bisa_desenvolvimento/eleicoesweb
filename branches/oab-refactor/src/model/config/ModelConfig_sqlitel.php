<?php

include_once ("lib/MVC/Model.php");

/**
 * [Classe responsável pela comunicação de Config com o Banco de Dados].
 * 
 */
class ModelConfig extends Model {

	/**
	 * ModelConfig::__construct()
	 * 
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * listaConfig 
	 * 
	 * @param void
	 * @return Array<Config> listaConfig
	 * 
	 */
	public function listaConfig() {
		$sql = "SELECT 
                    equ_nome,
                    urn_numero,
                    urn_zona,
                    urn_secao,
                    pasta
                FROM
                    equipamentos
                WHERE 
                    equ_nome = '" . BD_HOST . "'";

		$rs = $this->conexaoMySql->query($sql);
		if ($rs && $rs->rowCount()) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			$row = $this->montarListaObjetos($arr, 'Config');
			return $arr;
		} else {
			return false;
		}
	}

	/**
	 * ModelConfig::executarLinha()
	 * 
	 * @param mixed $linha
	 * @return
	 */
	public function executarLinha($linha) {
		return $this->conexao->query($linha);
	}

}
