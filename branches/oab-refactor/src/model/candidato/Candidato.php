<?php

/**
 * [Candidato.php].
 *
 * Classe básica de Candiddato do sistema
 */
class Candidato {

	/**
	 * Número de cadastro do Candidato na OAB
	 * @access private
	 * @var String
	 */
	private $oab;

	/**
	 * Nome do Candidato
	 * @access private
	 * @var string
	 */
	private $nome;

	/**
	 * Número da candidatura
	 * 
	 * @access private
	 * @var string
	 */
	private $numCandidatura;

	/**
	 * Caminho da foto do candidato.
	 * 
	 * @access private
	 * @var string
	 */
	private $foto;

	/**
	 * Cargo ao qual o candidato está concorrendo
	 * 
	 * @access private
	 * @var string
	 */
	private $cargo;

	/**
	 * Cargo ao qual o candidato está concorrendo
	 * 
	 * @access private
	 * @var string
	 */
	private $cargoNome;

	/**
	 * Candidato::__construct()
	 * 
	 * @param mixed $oab
	 * @param mixed $nome
	 * @param mixed $numCandidatura
	 * @param mixed $foto
	 * @param mixed $cargo
	 * @param mixed $cargoNome
	 * @return void
	 */
	public function __construct($oab = null, $nome = null, $numCandidatura = null, $foto = null,
		$cargo = null, $cargoNome = null) {
		$this->oab = $oab;
		$this->nome = $nome;
		$this->numCandidatura = $numCandidatura;
		$this->foto = $foto;
		$this->cargo = $cargo;
		$this->cargoNome = $cargoNome;
	}

	/**
	 * View::__set()
	 * Responsável por setar o valor de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @param mixed $valor
	 * @return void
	 */
	public function __set($chave, $valor) {
		$this->{$chave} = $valor;
	}

	/**
	 * View::__get()
	 * Responsável por interceptar o retorno de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __get($chave) {
		return $this->{$chave};
	}

	/**
	 * View::__isset()
	 * Interfere nas chamadas à função isset()
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __isset($chave) {
		return isset($this->{$chave});
	}

	/**
	 * View::__unset()
	 * Interfere nas chamadas às função unset()
	 * 
	 * @param mixed $chave
	 * @return void
	 */
	public function __unset($chave) {
		unset($this->{$chave});
	}

	/**
	 * View::__call()
	 * É chamado toda vez que um método chamado não é encontrado.
	 * 
	 * @param mixed $nomeDoMetodo
	 * @param mixed $argumentos
	 * @return void
	 */
	public function __call($nomeDoMetodo, $argumentos) {
	}

}
