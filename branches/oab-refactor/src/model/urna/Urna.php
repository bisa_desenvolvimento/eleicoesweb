<?php

/**
 * Urna
 *
 * Classe básica de urna
 *
 */

class Urna {

	/**
	 * Códgigo da Zona da Urna 
	 * 
	 * @access private
	 * @var integer
	 */
	private $zona = null;

	/**
	 * Códgigo da Secao da Urna 	
	 * 
	 * @access private
	 * @var string
	 */
	private $secao = null;

	/**
	 * Hora de emissao da zeresima 
	 * 
	 * @access private
	 * @var string
	 */
	private $horaZeresima = null;

	/**
	 * Hora de emissao da Bu
	 *
	 * @access private
	 * @var string
	 */
	private $horaBu = null;

	/**
	 * Descricao do local de votação 
	 * @access private
	 * @var string
	 */
	private $localVotacao = null;

	/**
	 * 
	 * 
	 * @access private
	 * @var string
	 */
	private $usuario = null;

	/**
	 *  Número de votos contabilizados
	 * 
	 * @access private
	 * @var int
	 */
	private $votosContabilizados = null;

	/**
	 *  Número de votos contabilizados
	 * 
	 * @access private
	 * @var int
	 */
	private $cidade = null;

	/**
	 *  Número de votos contabilizados
	 * 
	 * @access private
	 * @var int
	 */
	private $cancelados = null;

	/**
	 *  Número de votos contabilizados
	 * 
	 * @access private
	 * @var int
	 */
	private $numero = null;

	/**
	 *  Número de votos contabilizados
	 * 
	 * @access private
	 * @var int
	 */
	private $liberaConfig = null;

	/**
	 * Urna::__construct()
	 * 
	 * @param mixed $zona
	 * @param mixed $secao
	 * @param mixed $horaZeresima
	 * @param mixed $horaBu
	 * @param mixed $localVotacao
	 * @param mixed $usuario
	 * @param mixed $votosContabilizados
	 * @param mixed $cidade
	 * @param mixed $cancelados
	 * @param mixed $numero
	 * @param mixed $liberaConfig
	 * @return void
	 */
	public function __construct($zona = null, $secao = null, $horaZeresima = null, $horaBu = null,
		$localVotacao = null, $usuario = null, $votosContabilizados = null, $cidade = null,
		$cancelados = null, $numero = null, $liberaConfig = null) {
		$this->zona = $zona;
		$this->secao = $secao;
		$this->horaZeresima = $horaZeresima;
		$this->horaBu = $horaBu;
		$this->localVotacao = $localVotacao;
		$this->usuario = $usuario;
		$this->votosContabilizados = $votosContabilizados;
		$this->cidade = $cidade;
		$this->cancelados = $cancelados;
		$this->numero = $numero;
		$this->liberaConfig = $liberaConfig;
	}

	/**
	 * View::__set()
	 * Responsável por setar o valor de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @param mixed $valor
	 * @return void
	 */
	public function __set($chave, $valor) {
		$this->{$chave} = $valor;
	}

	/**
	 * View::__get()
	 * Responsável por interceptar o retorno de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __get($chave) {
		return $this->{$chave};
	}

	/**
	 * View::__isset()
	 * Interfere nas chamadas à função isset()
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __isset($chave) {
		return isset($this->{$chave});
	}

	/**
	 * View::__unset()
	 * Interfere nas chamadas às função unset()
	 * 
	 * @param mixed $chave
	 * @return void
	 */
	public function __unset($chave) {
		unset($this->{$chave});
	}

	/**
	 * View::__call()
	 * É chamado toda vez que um método chamado não é encontrado.
	 * 
	 * @param mixed $nomeDoMetodo
	 * @param mixed $argumentos
	 * @return void
	 */
	public function __call($nomeDoMetodo, $argumentos) {
	}

}
