<?php

include_once ("lib/MVC/Model.php");

/**
 * Classe responsável pela comunicação entre o controller ControllerUrna e o banco de dados.
 * 
 */
class ModelUrna extends Model {

	/**
	 * ModelUrna::__construct()
	 * 
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Lista todas as urnas
	 * 
	 * @return Array<Urna> listaUrna
	 * 
	 */
	public function listaUrna() {
		$sql = "SELECT 
                    urn_zona,
                    urn_secao,
                    urn_horaZeresima,
                    urn_horaBu,
                    l.locvot_id,
                    usuario_usu_id,
                    urn_votosContabilizados,
                    l.locvot_cidade,
                    u.urn_cancelamentos,
                    urn_numero, 
                    urn_libera
                FROM
                    urna u
                    INNER JOIN 
                        localvotacao l
                    ON 
                        u.locvot_id = l.locvot_id
                ORDER BY 
                    urn_zona, urn_secao";
		$rs = $this->conexao->query($sql);

		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			$row = $this->montarListaObjetos($arr, 'Urna');
			return $row;
		} else {
			return false;
		}
	}

	/**
	 * Lista dados da urna da seção e zona específica passada por parâmetro
	 * 
	 * @param Urna objUrna
	 * @return Array<Urna> listaUrna
	 * 
	 */
	public function listaUrnaPorSecaoZona($obj) {
		$sql = "SELECT 
                    urn_zona,
                    urn_secao,
                    urn_horaZeresima,
                    urn_horaBu,
                    locvot_id,
                    usuario_usu_id,
                    urn_votosContabilizados,
                    urn_numero
                FROM
                    urna      
                WHERE 
                    urn_zona = '" . $obj->zona . "'
                AND
                    urn_secao = '" . $obj->secao . "'";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			$row = $this->montarListaObjetos($arr, 'Urna');
			return $row;
		} else {
			return false;
		}
	}

	/**
	 * Lista os dados da urna para a apuração final
	 * 
	 * @return Array<Urna> listaUrna
	 * 
	 */
	public function listaUrnaParaApuracao() {
		$sql = "SELECT ur.urn_zona, 
                       ur.urn_secao, 
                       ur.urn_horaZeresima, 
                       lc.locvot_descricao, 
                       ur.urn_horaBu, 
                       us.usu_login, 
                       ur.urn_votosContabilizados,
                       urn_numero,
                       urn_libera
                  FROM
                       urna AS ur INNER JOIN usuario us
                       ON ur.usuario_usu_id = us.usu_id
                                                INNER JOIN localvotacao lc
                       ON ur.locvot_id = lc.locvot_id
                  ORDER BY urn_zona, urn_secao;";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}

	/**
	 * Retorna a hora de emissão da zerésima 
	 * 
	 * @param Urna urna 
	 * @return String hora
	 *  
	 */
	public function getHoraEmissaoZeresima($urna) {
		$sql = "SELECT 
                    SUBSTR(urn_horaZeresima,12, 5) AS hora
                FROM 
                    urna
                WHERE 
                    urna.urn_secao = '{$urna->secao}'
                AND 
                    urna.urn_zona = '{$urna->zona}'";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr[0]["hora"];
		} else {
			return false;
		}
	}

	/**
	 * Retorna a hora de emissão do BU
	 * 
	 * @param Urna urna 
	 * @return String hora 
	 * 
	 */
	public function getHoraEmissaoBu($urna) {
		$sql = "SELECT 
                    SUBSTR(urn_horaZeresima,12, 5) AS hora
                 FROM 
                    urna
                 WHERE 
                    urna.urn_secao = '{$urna->secao}'
                AND 
                    urna.urn_zona = '{$urna->zona}'";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr[0]["hora"];
		} else {
			return false;
		}
	}

	/**
	 * Atualiza a hora de emissão de Zeresima
	 * 
	 * @param String hora 
	 * @return boolean
	 * 
	 */
	public function updateZeresima($hora) {
		try {
			$this->conexao->beginTransaction();
			$sql = "UPDATE urna SET urn_horaZeresima = '" . $hora . "'";
			$acao = $this->conexao->prepare($sql);


			if ($acao->execute()) {
				//$acaoMirror = $this->conexaoMirror->prepare($sql);
				//$acaoMirror->execute();
			}

			$this->conexao->commit();
			return true;
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
			$retorno['result'] = false;
			$retorno['msg'] = 'Falha.';
			return false;
		}

	}

	/**
	 * Atualiza a hora de emissão do BU
	 * 
	 * @param String hora 
	 * @return boolean
	 * 
	 */
	public function updateBu($hora) {
		try {
			$this->conexao->beginTransaction();
			$sql = "UPDATE urna SET urn_horaBu = '" . $hora . "'";
			$acao = $this->conexao->prepare($sql);
			if ($acao->execute()) {
				//$acaoMirror = $this->conexaoMirror->prepare($sql);
				//$acaoMirror->execute();
			}
			$this->conexao->commit();
			return true;
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
			$retorno['result'] = false;
			$retorno['msg'] = 'Falha.';
			return false;
		}
	}

	/**
	 * Atualiza o status atual da urna
	 * 
	 * @param integer status 
	 * @return boolean
	 * 
	 */
	public function alterarStatus($status) {
		try {
			$this->conexao->beginTransaction();
			$sql = "UPDATE urna
                    SET urn_status = " . $status . "
                      WHERE urn_zona = '" . $_SESSION['eleicao']['zona'] . "'
                        AND urn_secao = '" . $_SESSION['eleicao']['secao'] . "'"; 
			$acao = $this->conexao->prepare($sql);
			if ($acao->execute()) { 
				//$acaoMirror = $this->conexaoMirror->prepare($sql);
				//$acaoMirror->execute(); 
			}
			$this->conexao->commit();
			return true;
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
			$retorno['result'] = false;
			$retorno['msg'] = 'Falha.';
			return false;
		}
	}

	/**
	 * Retorna o status atual da urna
	 * 
	 * @return integer status
	 * 
	 */
	public function obterStatus() {
		$sql = "SELECT urn_status
                  FROM urna
                  WHERE urn_zona = '" . $_SESSION['eleicao']['zona'] . "'
                    AND urn_secao = '" . $_SESSION['eleicao']['secao'] . "'";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr[0]["urn_status"];
		} else {
			return false;
		}
	}

	/**
	 * Retorna se a urna já foi apurada ou não
	 * 
	 * @param Urna urna
	 * @return bool - se a urna já foi apurada
	 * 
	 */
	public function consultarUrnaApurada($urna) {
		$sql = "SELECT 
                    urn_votosContabilizados 
                FROM
                    urna
                WHERE
                    urn_zona = '" . $urna->zona . "'
                AND
                    urn_secao = '" . $urna->secao . "'
                AND 
                    urn_votosContabilizados <> 0";
                  
		$rs = $this->conexao->query($sql);
		
		if ($rs) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Adiciona mais um cancelamento de voto à urna
	 * 
	 * @return bool - se o status foi atualizado
	 * 
	 */
	public function atualizarCancelamento() {
		$sql = "UPDATE urna
                SET urn_cancelamentos = urn_cancelamentos + 1
                  WHERE urn_zona = '" . $_SESSION['eleicao']['zona'] . "'
                    AND urn_secao = '" . $_SESSION['eleicao']['secao'] . "'";
		$acao = $this->conexao->prepare($sql);
		if ($acao->execute()):
			//$acaoMirror = $this->conexaoMirror->prepare($sql);
			//$acaoMirror->execute();
		endif;
		return true;
	}

	/**
	 * Limpa dados da urna nas duas bases, zerando as mesmas para o início de uma nova votação
	 * 
	 * @return bool - se o status foi atualizado
	 * 
	 */
	public function atualizarStatusUrna() {
		try {
			$this->conexao->beginTransaction();
			$sql = "UPDATE urna
                        SET urn_horaZeresima = NULL, 
                            urn_status = 1, 
                            urn_cancelamentos = 0";
			$acao = $this->conexao->prepare($sql);
			if (!$acao->execute()) {
				throw $e;
			} else {
				$acaoMirror = $this->conexaoMirror->prepare($sql);
				$acaoMirror->execute();
				$sql = "UPDATE urna
                            SET urn_horaZeresima = NULL;";
				$acao = $this->conexao->prepare($sql);
				if (!$acao->execute()) {
					throw $e;
				} else {
					$acaoMirror = $this->conexaoMirror->prepare($sql);
					$acaoMirror->execute();
					echo 'OK';
					$this->conexao->commit();
					return 'ok';
				}
			}
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
			header('Location: /urna/menuUrna/ERROR/');
		}
	}

	/**
	 * ModelUrna::atualizarVotosCancelados()
	 * Atuliza a quantidade de votos cancelados
	 * 
	 * @param mixed $vtCancelados
	 * @param mixed $zona
	 * @param mixed $secao
	 * @return
	 */
	public function atualizarVotosCancelados($vtCancelados, $zona, $secao) {
		$sql = "UPDATE urna SET urn_cancelamentos = " . $vtCancelados .
			" WHERE urn_zona = '" . $zona . "' AND urn_secao = '" . $secao . "'";
		$acao = $this->conexao->prepare($sql);
		try {
			$this->conexao->beginTransaction();
			if (!$acao->execute()) {
				return false;
			} else {
				$acaoMirror = $this->conexaoMirror->prepare($sql);
				$acaoMirror->execute();
				$this->conexao->commit();
				return true;
			}
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
		}
	}

	/**
	 * ModelUrna::inserirTotalEleitores()
	 * Insere o toral de eleitores na urna especificada
	 * 
	 * @param mixed $total
	 * @param mixed $votantes
	 * @param mixed $zona
	 * @param mixed $secao
	 * @return
	 */
	public function inserirTotalEleitores($total, $votantes, $zona, $secao,$urna='0') {
		$sql = "UPDATE urna
                SET urn_votosContabilizados = urn_votosContabilizados-1,
                    urn_totalEleitores = '" . $total . "', 
                    urn_totalVotantes = '" . $votantes . "'
                  WHERE urn_zona = '" . $zona . "'
                    AND urn_secao = '" . $secao . "'";
		$acao = $this->conexao->prepare($sql);
		try {
			$this->conexao->beginTransaction();
			if ($acao->execute()) {
			  $this->conexao->commit();
				return true;
			}
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
		}
	}

	/**
	 * ModelUrna::verificarSeExisteBanco()
	 * 
	 * @param mixed $banco
	 * @return
	 */
	public function verificarSeExisteBanco($banco) {
		$sql = "EXEC sp_verificar_banco ?";
		$this->conexao->setAttribute(PDO::ATTR_CASE, PDO::CASE_NATURAL);
		$this->conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$rs = $this->conexao->prepare($sql);
		$rs->setFetchMode(PDO::FETCH_OBJ);
		$param = $banco;
		$rs->bindParam(1, $param);
		if ($rs->execute()) {
			$arr = $rs->fetch();
			return $arr->situacao;
		} else {
			return false;
		}
	}

	/**
	 * ModelUrna::gerarBackUpDatabase()
	 * 
	 * @param mixed $base
	 * @return
	 */
	public function gerarBackUpDatabase($base) {
		try {
			if (file_exists(DESTINO_BACKUP . $base . ".bak")) {
				unlink(DESTINO_BACKUP . $base . ".bak");
			}
			$sql = "EXEC sp_backup_banco ?, ?";
			$this->conexao->setAttribute(PDO::ATTR_CASE, PDO::CASE_NATURAL);
			$this->conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->conexao->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
			$rs = $this->conexao->prepare($sql);
			$rs->setFetchMode(PDO::FETCH_OBJ);
			$param = $base;
			$destino = DESTINO_BACKUP;
			$rs->bindParam(1, $param);
			$rs->bindParam(2, $destino);
			if ($rs->execute()) {
				return true;
			} else {
				return false;
			}
		}
		catch (PDOException $e) {
			echo '<pre>';
			print_r($e);
			echo '</pre>';
		}
	}

	/**
	 * ModelUrna::dropDatabase()
	 * 
	 * @param mixed $base
	 * @return
	 */
	public function dropDatabase($base) {
		$sql = "IF db_id('" . $base . "') IS NOT NULL 
                    BEGIN 
                        DROP DATABASE " . $base . " 
                    END";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ModelUrna::createDatabase()
	 * 
	 * @param mixed $base
	 * @return
	 */
	public function createDatabase($base) {
		$sql = "CREATE DATABASE " . $base . "\n";
		return $this->conexao->query($sql);
	}

	/**
	 * ModelUrna::restoreDatabase()
	 * 
	 * @param mixed $base
	 * @return
	 */
	public function restoreDatabase($base) {
		if (strstr($base, 'Mirror')) {
			$useDatabase = BD_BASE;
		} else {
			$useDatabase = BD_BASE_MIRROR;
		}
		if (file_exists(DESTINO_BACKUP . $useDatabase . ".bak")) {
			$database = $useDatabase;
			$mirror = $base;
			$backup = DESTINO_BACKUP;
			$recovery = DESTINO_RECOVERY;
			$sql = "EXECUTE [" . $database . "].[dbo].[sp_restaurar_banco]
                   '" . $database . "'
                  ,'" . $mirror . "'
                  ,'" . $backup . "'
                  ,'" . $recovery . "'";
			$rs = $this->conexao->prepare($sql);
			if ($rs->execute()) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * ModelUrna::execMaster()
	 * 
	 * @return void
	 */
	public function execMaster() {
		$sql = "EXECUTE sp_proc_master 
                       " . BD_BASE . "
                      ," . BD_BASE_MIRROR . "
                      ," . DESTINO_BACKUP . "";
		$this->conexao->setAttribute(PDO::ATTR_CASE, PDO::CASE_NATURAL);
		$this->conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$rs = $this->conexao->prepare($sql);
		$rs->setFetchMode(PDO::FETCH_OBJ);
		$rs = $this->conexao->prepare($sql);
	}

	/**
	 * ModelUrna::gravarParaDetectarBanco()
	 * 
	 * @param mixed $banco
	 * @return
	 */
	public function gravarParaDetectarBanco($banco) {
		$sql = "SELECT ele_descricao FROM " . $banco . ".dbo.eleicao";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ModelUrna::consultarVontantesUrna()
	 * 
	 * @return
	 */
	public function consultarVontantesUrna() {
		$sql = "SELECT (
                	SELECT 
                		COUNT(*) AS qtd
                	FROM 
                		eleitor
                	WHERE
                		elt_votou = 1) 
                AS votantes
                UNION
                SELECT (
                	SELECT 
                		COUNT(*) AS qtd
                	FROM 
                		eleitor
                ) AS total";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}

	/**
	 * ModelUrna::updateLibera()
	 * 
	 * @return
	 */
	public function updateLibera() {
		$sql = "UPDATE urna SET urn_libera = urn_libera + 1";
		$acao = $this->conexao->prepare($sql);
		try {
			$this->conexao->beginTransaction();
			if (!$acao->execute()) {
				return false;
			} else {
				$acaoMirror = $this->conexaoMirror->prepare($sql);
				$acaoMirror->execute();
				$this->conexao->commit();
				return true;
			}
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
		}
	}
  
  public function buscarUrna($urna) {
    $sql = "SELECT 
              urn_zona,
              urn_secao,
              escola_nome,
              urn_votosContabilizados AS status
            FROM
              urna 
              INNER JOIN escolas 
                ON zona_id = urn_zona 
            WHERE urn_secao = '" . $urna . "'";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr[0];
		} else {
			return false;
		}
  }
  
  public function consultarStatusUrnaApurada($zona, $secao) {
    $sql = "SELECT 
              urn_votosContabilizados 
            FROM
              urna 
            WHERE urn_zona = '$zona' 
              AND urn_secao = '$secao'";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr[0]['urn_votosContabilizados'];
		} else {
			return false;
		}
  }

  public function verificarUrnasSemContabilizacao()
  {
  		  
  		$sql = "SELECT SUM(urna.`urn_votosContabilizados`) AS total FROM urna";

		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr[0];
		} else {
			return false;
		}
  }

}
