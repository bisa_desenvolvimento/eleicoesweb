<?php

require_once ("lib/MVC/Model.php");

/**
 * Classe responsável pela comunicação entre o ControllerEleicao e o Banco de dados.
 * 
 */
class ModelEleicao extends Model {

	/**
	 * ModelEleicao::__construct()
	 * 
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * ModelEleicao::atualizarInicioEleicao()
	 * 
	 * @return bool
	 */
	public function atualizarInicioEleicao() {
		try {
			$this->conexao->beginTransaction();
			$sql = "UPDATE eleicao SET ele_horaInicio = DATEADD(day, 1, ele_horaInicio)";
			$acao = $this->conexao->prepare($sql);
			$acao->execute();
			$this->conexao->commit();
			return true;
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
			$retorno['result'] = false;
			$retorno['msg'] = 'Falha.';
			return false;
		}
	}

	/**
	 * Retorna os dados da eleição atual
	 * 
	 * @return Array<Eleicao> listaEleicao
	 */
	public function eleicao() {
		$sql = "SELECT 
                    ele_id, 
                    ele_descricao,
                    ele_data,
                    ele_horaInicio,
                    ele_horaTermino,
                    ele_qtdVotosCandidatos,
                    ele_qtdBu,
                    ele_qtdZeresima
                FROM 
                eleicao";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			exit();
			return false;
		}
	}

	/**
	 * Retorna os cargos da eleição
	 * 
	 * @return Array<Eleicao> listaEleicao
	 */
	public function cargos() {
		$sql = "SELECT car_id, 
                       car_nome
                  FROM cargo
                  ORDER BY car_id";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}

	/**
	 * Retorna os cargos da eleição por zona
	 * 
	 * @return Array<Eleicao> listaEleicao
	 */
	public function cargosPorZona($zona) {
		$sql = "SELECT car_id, 
                       car_nome
                  FROM cargo
                  WHERE car_id > 0
                  ORDER BY car_id";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}

	/**
	 * Retorna os cargos da eleição
	 * 
	 * @return Array<Eleicao> listaEleicao
	 */
	public function zonas() {
		$sql = "SELECT DISTINCT urn_zona
                  FROM urna
                  WHERE urn_zona != '0000'";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}

	/**
	 * Retorna o total de votos dos candidatos
	 * 
	 * @return int totalVotos
	 */
	public function resultado() {
		$sql = "SELECT candidato.cdt_oab,
                       UrnaVotos.vot_car_id,
                       candidato.cdt_nome,
                       candidato.cdt_numerocandidatura,
                       COALESCE(UrnaVotos.totalvotos, 0)AS totalVotos
                FROM   candidato AS CANDIDATO
                       LEFT OUTER JOIN(SELECT vot_oabcandidato,
                                              vot_car_id,
                                              Sum(vot_sequencial)AS totalVotos
                                       FROM   voto
                                       GROUP  BY vot_oabcandidato,
                                                 vot_car_id)AS UrnaVotos
                                    ON UrnaVotos.vot_oabcandidato = candidato.cdt_oab
                ORDER  BY vot_car_id,
                          totalvotos DESC";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}
	/**
	 * Retorna o total de votos dos candidatos separados por secoes
	 * 
	 * @return Array<Eleicao> listaCandidatos
	 */
	public function resultadoPublicoGeral($conselho) {

			header('Content-Type: text/html; charset=iso-8859-1',true);	
			$sql = "SELECT 
				  candidato.cdt_nome_conselho,
				  candidato.cdt_nome,
				  candidato.cdt_oab,
				  SUM(voto.vot_sequencial) AS total 
				FROM
				  voto 
				  INNER JOIN candidato 
				    ON candidato.cdt_oab = voto.vot_oabCandidato
				 WHERE candidato.`cdt_oab` <> '0' AND candidato.`cdt_oab` <> '99999' AND candidato.`cdt_situacao` = 'D' AND candidato.`cdt_id_conselho` = ".$conselho."
				GROUP BY candidato.cdt_oab 
				ORDER BY candidato.cdt_nome_conselho,
				  total DESC,
				  candidato.cdt_nota DESC,
				  candidato.cdt_idade ";
							

			$rs = $this->conexao->query($sql);

			if ($rs) {

				$listaCandidatos = $rs->fetchAll(PDO::FETCH_ASSOC);

				
			} else {
				$listaCandidatos = false;
			}
			
			return $listaCandidatos;
		}
		
	

	/**
	 * Retorna o percentual de urnas já apuradas na eleição
	 * 
	 * @return Array<Eleicao> percentualPorBlocos
	 */
	public function urnasApuradasPorBlocos() {
		$sql_blocos = "SELECT localvotacao.locvot_id FROM localvotacao";
		$resultado = $this->conexao->query($sql_blocos);
		$arr_blocos = $resultado->fetchAll(PDO::FETCH_ASSOC);
		$percentualPorBlocos = array();
		$cont = 0;
		foreach ($arr_blocos as $bloco) {
			$sql = "SELECT CAST(( 
                    SELECT COUNT( urn_votosContabilizados
                                )
                      FROM urna
                      WHERE urn_votosContabilizados <> 0 AND locvot_id = " . $bloco['locvot_id'] .
				" 
                  ) * 100 AS decimal( 10, 2
                                    )
                 ) / ( 
                       SELECT COUNT( *
                                   )
                         FROM urna AS Urna WHERE locvot_id = " . $bloco['locvot_id'] .
				"
                     )AS pctUrnasApuradas, locvot_id FROM urna WHERE locvot_id = " .
				$bloco['locvot_id'] . " group by locvot_id;";
			$rs = $this->conexao->query($sql);
			$dados = $rs->fetchAll(PDO::FETCH_ASSOC);
			if (count($dados) > 0) {
				$percentualPorBlocos[$cont]['pctUrnasApuradas'] = $dados[0]['pctUrnasApuradas'];
				$percentualPorBlocos[$cont]['locvot_id'] = $dados[0]['locvot_id'];
			} else {
				$percentualPorBlocos[$cont]['pctUrnasApuradas'] = '0,00';
				$percentualPorBlocos[$cont]['locvot_id'] = $bloco['locvot_id'];
			}
			$cont++;
		}
		return $percentualPorBlocos;
	}

	/**
	 * Retorna o percentual de urnas já apuradas na eleição
	 * 
	 * @return float percentualUrnasApuradas
	 */
	public function urnasApuradas() {
		$sql = "SELECT CAST(( 
                  SELECT COUNT( urn_votosContabilizados
                              )
                    FROM urna
                    WHERE urn_votosContabilizados <> 0
                ) * 100 AS decimal( 10, 2
                                  )
               ) / ( 
                     SELECT COUNT( *
                                 )
                       FROM urna AS Urna
                   )AS pctUrnasApuradas;";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr[0];
		} else {
			return '0,00';
		}
	}

	/**
	 * Retorna o percentual de urnas já apuradas na eleição
	 * 
	 * @return float percentualUrnasApuradas
	 */
	public function urnasApuradasSemVotoSeparado() {
		$sql = "SELECT CAST(( 
                  SELECT COUNT( urn_votosContabilizados
                              )
                    FROM urna
                    WHERE 
                        urn_votosContabilizados <> 0
                    AND 
                        urn_zona <> '0000'
                    AND
                        urn_secao <> '0000'
                ) * 100 AS decimal( 10, 2 )
               ) / ( 
                     SELECT COUNT( *
                                 )
                       FROM urna AS Urna
                       WHERE 
                          urn_zona <> '0000'
                       AND
                          urn_secao <> '0000'
                   )AS pctUrnasApuradas;";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr[0];
		} else {
			return '0,00';
		}
	}

	/**
	 * ModelEleicao::existeVotoSeparado()
	 * 
	 * @return
	 */
	public function existeVotoSeparado() {
		$sql = "SELECT 
                    COUNT(*) AS total 
                FROM 
                    voto 
                WHERE 
                    vot_secao = '0000' 
                AND 
                    vot_zona = '0000'";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr[0];
		} else {
			return false;
		}
	}

	/**
	 * ModelEleicao::gerarSeparado()
	 * 
	 * @return
	 */
	public function gerarSeparado() {
		$sql = "SELECT l.locvot_cidade, cdt_nome, vot_zona , 
                  vot_secao ,
                  SUM( vot_sequencial
                            ) as votos
                  FROM voto v, candidato c, 
                       urna u, localvotacao l
                  WHERE v.vot_oabCandidato = c.cdt_oab
                  AND v.vot_zona = u.urn_zona
                  AND u.locvot_id = l.locvot_id
                  GROUP BY l.locvot_cidade, vot_zona, vot_secao, cdt_nome
                  ORDER BY vot_zona, vot_secao";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}

	/**
	 * ModelEleicao::alterarDataEleicao()
	 * 
	 * @param mixed $data
	 * @return
	 */
	public function alterarDataEleicao($data) {
		try {
			$this->conexao->beginTransaction();
			$sql = "UPDATE eleicao
                       SET ele_data = '" . $data['data_inicio'] . " 00:00:00'
                          ,ele_horaInicio = '" . $data['data_inicio'] . " " . $data['hora_inicio'] .
				"'
                          ,ele_horaTermino = '" . $data['data_final'] . " " . $data['hora_final'] .
				"'";
			$acao = $this->conexao->prepare($sql);
			$acao->execute();
			$this->conexao->commit();
			return true;
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
			return false;
		}
	}

	public function eleicaoMapaUrnas()
	{
		$sql = "SELECT 
				  localvotacao.locvot_descricao,
				  localvotacao.locvot_id,
				  urna.urn_secao,
				  urna.urn_votosContabilizados
				    
				FROM
				  urna 
				INNER JOIN localvotacao 
				  ON urna.locvot_id = localvotacao.locvot_id";

		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}
	public function urnasApuradasGeral()
	{
		$sql = "SELECT COUNT(urn_secao) AS total, SUM(urn_votosContabilizados) AS urnas_contabilizadas FROM urna";

		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}
}
