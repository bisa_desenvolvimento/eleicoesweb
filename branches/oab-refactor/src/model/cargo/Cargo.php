<?php

/**
 * [Cargo.php].
 *
 * Classe básica de Candiddato do sistema
 */

class Cargo {
	/**
	 * Número de cadastro do Cargo na id
	 * @access private
	 * @var String
	 */
	private $id;

	/**
	 * Nome do Cargo
	 * @access private
	 * @var string
	 */
	private $nome;

	/**
	 * Número da candidatura
	 * 
	 * @access private
	 * @var string
	 */
	private $ordem;


	/**
	 * Caminho da votos do Cargo.
	 * 
	 * @access private
	 * @var string
	 */
	private $votos;

	/**
	 * Cargo::__construct()
	 * 
	 * @param mixed $id
	 * @param mixed $nome
	 * @param mixed $ordem
	 * @param mixed $votos
	 * @return void
	 */
	public function __construct($id = null, $nome = null, $ordem = null, $votos = null) {
		$this->id = $id;
		$this->nome = $nome;
		$this->ordem = $ordem;
		$this->votos = $votos;
	}

	/**
	 * View::__set()
	 * Responsável por setar o valor de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @param mixed $valor
	 * @return void
	 */
	public function __set($chave, $valor) {
		$this->{$chave} = $valor;
	}

	/**
	 * View::__get()
	 * Responsável por interceptar o retorno de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __get($chave) {
		return $this->{$chave};
	}

	/**
	 * View::__isset()
	 * Interfere nas chamadas à função isset()
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __isset($chave) {
		return isset($this->{$chave});
	}

	/**
	 * View::__unset()
	 * Interfere nas chamadas às função unset()
	 * 
	 * @param mixed $chave
	 * @return void
	 */
	public function __unset($chave) {
		unset($this->{$chave});
	}

	/**
	 * View::__call()
	 * É chamado toda vez que um método chamado não é encontrado.
	 * 
	 * @param mixed $nomeDoMetodo
	 * @param mixed $argumentos
	 * @return void
	 */
	public function __call($nomeDoMetodo, $argumentos) {
	}

}
