<?php

require_once ("lib/MVC/Model.php");

/**
 * Classe responsável pela comunicação entre o ControllerLocalVotacao e o Banco de dados.
 * 
 */
class ModelLocalVotacao extends Model {

	/**
	 * ModelLocalVotacao::__construct()
	 * 
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Lista de todos os locais de votação
	 * 
	 * @return Array<LocalVotacao> listaLocalVotacao
	 * 
	 */
	public function listaLocalVotacao() {
		$sql = "SELECT 
                    locvot_id,
                    locvot_descricao,
                    locvot_endereco,
                    locvot_cep,
                    locvot_cidade,
                    locvot_estado,
                    locvot_telefone,
                    locvot_reponsavel
                FROM
                    " . BD_BASE . ".dbo.localVotacao
                ORDER BY 
                    locvot_descricao";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			$row = $this->montarListaObjetos($arr, 'LocalVotacao');
			return $row;
		} else {
			return false;
		}
	}

	/**
	 * Lista os dados do local de votação informado
	 * 
	 * @param int id - id do local de votação 
	 * @return Array<LocalVotacao> listaLocalVotacao
	 * 
	 */
	public function listaLocalVotacaoPorId($id) {
		$sql = "SELECT 
                    locvot_id,
                    locvot_descricao,
                    locvot_endereco,
                    locvot_cep,
                    locvot_cidade,
                    locvot_estado,
                    locvot_telefone,
                    locvot_reponsavel
                FROM
                    " . BD_BASE . ".dbo.localVotacao            
                WHERE 
                    locvot_id = " . $id;
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			$row = $this->montarListaObjetos($arr, 'LocalVotacao');
			return $row;
		} else {
			return false;
		}
	}

}
