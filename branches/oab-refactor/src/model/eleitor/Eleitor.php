<?

/**
 * [Classe b�sica de Elei��o].
 */

class Eleitor {

	/**
	 * @access private
	 * @var integer
	 */
	private $oab = null;

	/**
	 * @access private
	 * @var string
	 */
	private $nome = null;

	/**
	 * @access private
	 * @var string
	 */
	private $votou = null;


	/**
	 * @access private
	 * @var string
	 */
	private $zona = null;

	/**
	 * @access private
	 * @var string
	 */
	private $secao = null;

	/**
	 * Eleitor::__construct()
	 * 
	 * @param mixed $oab
	 * @param mixed $nome
	 * @param mixed $votou
	 * @param mixed $zona
	 * @param mixed $secao
	 * @return void
	 */
	public function __construct($oab = null, $nome = null, $votou = null, $zona = null,
		$secao = null) {
		$this->oab = $oab;
		$this->nome = $nome;
		$this->votou = $votou;
		$this->zona = $zona;
		$this->secao = $secao;
	}

	/**
	 * View::__set()
	 * Respons�vel por setar o valor de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @param mixed $valor
	 * @return void
	 */
	public function __set($chave, $valor) {
		$this->{$chave} = $valor;
	}

	/**
	 * View::__get()
	 * Respons�vel por interceptar o retorno de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __get($chave) {
		return $this->{$chave};
	}

	/**
	 * View::__isset()
	 * Interfere nas chamadas � fun��o isset()
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __isset($chave) {
		return isset($this->{$chave});
	}

	/**
	 * View::__unset()
	 * Interfere nas chamadas �s fun��o unset()
	 * 
	 * @param mixed $chave
	 * @return void
	 */
	public function __unset($chave) {
		unset($this->{$chave});
	}

	/**
	 * View::__call()
	 * � chamado toda vez que um m�todo chamado n�o � encontrado.
	 * 
	 * @param mixed $nomeDoMetodo
	 * @param mixed $argumentos
	 * @return void
	 */
	public function __call($nomeDoMetodo, $argumentos) {
	}

}
