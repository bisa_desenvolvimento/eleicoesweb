# Máquina no Docker
 
1. Instale o docker: https://docs.docker.com/mac/

2. Instale o docker compose: https://docs.docker.com/compose/install/

2. Crie o container:


```
sudo docker-composer build
```

4. Rode o comando para subir o container

```
sudo docker-compose up -d
```

4. Veja se o serviço subiu

```
sudo docker ps
```
```
CONTAINER ID        IMAGE               COMMAND                CREATED             STATUS              PORTS                  NAMES
c5b91673d3de        eleicoesweb         "apache2-foreground"   9 seconds ago       Up 8 seconds        0.0.0.0:8080->80/tcp   oabrefactor_web_1

```

# Ações

* Remover a imagem e os containers

```
sudo docker rmi --force <id>
```

Trocando <id> pelo ID da imagem

* Sincronizar máquina com o diretório do container (para desenvolvimento)

```
sudo docker run -d -p 8080:80 --name eleicowesweb-run eleicoesweb
```

* Visualizar logs

```
sudo docker logs c5b91673d3de
```

* Abrir terminal na máquina

```
sudo docker-compose run web bash
```