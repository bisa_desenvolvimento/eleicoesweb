<?php

include_once ("lib/MVC/Model.php");
/**
 * Classe responsável pela comunicação entre a classe básica de voto e o banco de dados.
 * 
 */
class ModelVoto extends Model {
	/**
	 * ModelVoto::__construct()
	 * 
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}
	/**
	 * Insere os votos nas bases de dados e atualiza o eleitor par ainformar que o mesmo
	 * já votou
	 *  
	 * @param Array<Votos> votos - votos do eleitor
	 * @return boolean  
	 */
	public function inserirVoto($votos, $cargo) {
		$qtdCargo = 0;
		try {
			$this->conexao->beginTransaction();
			$i = 0;
			foreach ($votos as $voto) {
				$sequencial = $this->obterSequencialMaximo($voto, $cargo[$i++]);
				$sequencial++;
				$arrSequencial[] = $sequencial;
				$sql = "INSERT INTO voto(
                                vot_zona,
                                vot_secao,
                                vot_oabCandidato,
                                vot_sequencial,
                                vot_valido, 
                                vot_car_id
                            ) VALUES (
                            '" . addslashes($_SESSION['eleicao']['zona']) . "',
                            '" . addslashes($_SESSION['eleicao']['secao']) . "',
                            '" . addslashes($voto) . "',
                            '" . $sequencial . "',
                            '1',
                            " . $cargo[$qtdCargo] . "
                            );";
				$acao = $this->conexao->prepare($sql);
				if (!$acao->execute()) {
					$this->conexao->rollBack();
					$this->erroVoto();
				} else {
				}
				$qtdCargo++;
			}
			$sqlEleitor = "UPDATE eleitor
                               SET elt_votou = 1,
                               elt_datahora = TRIM(elt_datahora) || '_' || '" .
				date("Y-m-d H:i:s") . "' || '|' || '" . $_SESSION['eleicao']['zona'] . "-" . $_SESSION['eleicao']['secao'] .
				"' 
                             WHERE elt_oab= '" . $_SESSION['eleitor'] . "'";
			$acaoEleitor = $this->conexao->prepare($sqlEleitor);
			if (!$acaoEleitor->execute()) {
				$this->conexao->rollBack();
				$this->erroVoto();
			}
			$this->conexao->commit();
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
			$this->erroVoto();
		}
		unset($_SESSION['eleitor']);
		return true;
	}
	/**
	 * Insere um voto no banco de dados de apuração e sinaliza a urna como já apurada
	 * 
	 * @param Voto voto 
	 * @return boolean  
	 * 
	 */
	public function inserirVotoSincronizado($obj) {
		try {
			$this->conexao->beginTransaction();
			$rollback = false;
			foreach ($obj->votos as $oab => $sequencial) {
				if (!$oab) {
					$cargo = 'NULL';
				}
				$sql = "INSERT INTO voto(
                                vot_zona,
                                vot_secao,
                                vot_oabCandidato,
                                vot_sequencial,
                                vot_valido,
                                vot_car_id,
                                vot_origem
                            ) VALUES (
                            '" . addslashes($obj->zona) . "',
                            '" . addslashes($obj->secao) . "',
                            '" . addslashes($oab) . "',
                            '" . $sequencial . "',
                            '1',
                            '1',
                            'E'
                            );";
				$acao = $this->conexao->prepare($sql);
				if (!$acao->execute()) {
					$rollback = true;
				}
			}
			if (!$rollback) {
				//echo 'OK Commit';
				$this->conexao->commit();
				return true;
			} else {
				//echo 'erro rollback';
				$this->conexao->rollBack();
				return false;
			}
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
			return false;
		}
	}
	/**
	 * Insere os votos manuais no banco de dados de apuração e sinaliza a urna como já apurada
	 * 
	 * @param Voto voto 
	 * @return boolean  
	 * 
	 */
	public function inserirVotoManual($obj) {
		try {
			$this->conexao->beginTransaction();
			$rollback = false;
      
      $sqlDel = "DELETE FROM
                voto 
              WHERE vot_zona = '" . addslashes($obj->zona) . "'
                AND vot_secao = '" . addslashes($obj->secao) . "'
                AND vot_origem = 'M'";
      
      $acaoUrna = $this->conexao->prepare($sqlDel);
		  if ($acaoUrna->execute()) {
		    
  			foreach ($obj->votos as $cargo => $voto) {
  				foreach ($voto as $oab => $sequencial) {
  					$sql = "INSERT INTO voto(
                                  vot_zona,
                                  vot_secao,
                                  vot_oabCandidato,
                                  vot_sequencial,
                                  vot_valido,
                                  vot_car_id,
  								                vot_origem
                              ) VALUES (
                              '" . addslashes($obj->zona) . "',
                              '" . addslashes($obj->secao) . "',
                              '" . addslashes($oab) . "',
                              '" . $sequencial . "',
                              '1',
                              '" . $cargo . "',
                              'M' 
                              )";
  					$acao = $this->conexao->prepare($sql);
  					if (!$acao->execute()) {
  						$rollback = true;
  					}
  				}
  			}
  			$this->contabilizaVoto($obj);
  			if (!$rollback) {
  				$this->conexao->commit();
  				return true;
  			} else {
  				$this->conexao->rollBack();
  				return false;
  			}
		  }
    }
		catch (PDOException $e) {
			$this->conexao->rollBack();
			return false;
		}
	}
	/**
	 * ModelVoto::contabilizaVoto()
	 * 
	 * @param mixed $obj
	 * @return void
	 */
	public function contabilizaVoto($obj) {
		$sqlUrna = "UPDATE urna
                        SET urn_votosContabilizados = urn_votosContabilizados+2,
                        urn_totalEleitores = " . $obj->eleitores . "
                          WHERE urn_zona
                                = 
                                '" . $obj->zona . "'
                            AND urn_secao
                                = 
                                '" . $obj->secao . "'";
		$acaoUrna = $this->conexao->prepare($sqlUrna);
		if (!$acaoUrna->execute()) {
			$rollback = true;
		}
	}
	/**
	 * Retorna mensagem de erro 
	 * 
	 * @param void 
	 * @return false;
	 * 
	 */
	private function erroVoto() {
		$retorno['result'] = false;
		$retorno['msg'] = 'Falha no cadastro do Voto.';
		return false;
	}
	/**
	 * Retorna o maior voto de um candidato
	 * 
	 * @param codigoOab
	 * @return int voto 
	 */
	public function obterSequencialMaximo($oab, $cargo) {
		$sql = "SELECT 
                  COUNT(vot_sequencial) AS voto 
                FROM
                  voto 
                WHERE vot_oabCandidato = '" . $oab . "' AND vot_car_id = '" . $cargo .
			"'";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr[0]['voto'];
		} else {
			return 0;
		}
	}
	/**
	 * Retorna se a urna já possui votos computados
	 * 
	 * @param Urna urna 
	 * @return Array<Voto> votos
	 */
	public function consultarVotosLocal($arr) {
		$zona = $arr->zona;
		$secao = $arr->secao;
		$urna = $arr->urna;
		$banco = $arr->dataBase;
		$sql = "SELECT vot_zona , 
                  vot_secao ,
                  COUNT( vot_sequencial)
                  FROM voto 
                  WHERE vot_zona = '".$arr->zona."'
                  AND vot_secao = '".$arr->secao."'
                  GROUP BY vot_zona, vot_secao, vot_oabCandidato"; 
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			if (count($arr) > 0) return true;
			else  return false;
		} else {
			return false;
		}
	}
	/**
	 * Insere votos no banco espelhado
	 * 
	 * @param Urna urna 
	 * @return boolean 
	 */
	public function addVotosSecaoSync($obj) {
		try {
			if (count($obj) > 0) {
				$this->conexao->beginTransaction();
				$contador = 0;
				foreach ($obj as $obj) {
					$sql = "INSERT INTO voto
                                   (vot_zona
                                   ,vot_secao
                                   ,vot_oabCandidato
                                   ,vot_sequencial
                                   ,vot_valido)
                             VALUES
                                   ('" . $obj->zona . "', 
                                   '" . $obj->secao . "', 
                                   '" . $obj->oabCandidato . "',
                                   " . $obj->sequencial . ",
                                   1);";
					$acao = $this->conexao->prepare($sql);
					if (!$acao->execute()) {
						$this->conexao->rollBack();
						header('Location: /urna/menuUrna/ERROR/');
					}
					$contador++;
					$zonaFinal = $obj->zona;
					$secaoFinal = $obj->secao;
				}
				$sql2 = "UPDATE urna
                        SET urn_votosContabilizados <> 0
                          WHERE urn_zona
                                = 
                                '" . $zonaFinal . "'
                            AND urn_secao
                                = 
                                '" . $secaoFinal . "'";
				$acao2 = $this->conexao->prepare($sql2);
				if (!$acao2->execute()) {
					$this->conexao->rollBack();
					header('Location: /urna/menuUrna/ERROR/');
				} else {
					$this->conexao->commit();
					return 'ok';
				}
			}
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
			header('Location: /urna/menuUrna/ERROR/');
		}
	}
	/**
	 * Lista a quantidade de votos por candidato em cada urna
	 * 
	 * @param Urna urna 
	 * @return Array valoresQtdEleitoresVotosBu
	 * 
	 */
	public function listaQtdVotosBU($urna) {
		$sql = "SELECT 
                   (SELECT COUNT(vot_sequencial) FROM
                    voto,
                    candidato ,
                    urna
                WHERE
                    voto.vot_oabCandidato = candidato.cdt_oab
                AND 
                	candidato.cdt_numeroCandidatura NOT IN (" . BRANCO_NUMERO . "," .
			NULO_NUMERO . ")
                AND
                    urna.urn_secao = voto.vot_secao
                AND
                    urna.urn_zona  = voto.vot_zona
               ) AS totalVotosNominais,                  
                  (SELECT 
                	COUNT(vot_sequencial)
                   FROM
                    voto,
                    candidato ,
                    urna
                WHERE
                    voto.vot_oabCandidato = candidato.cdt_oab
                AND
                    urna.urn_secao = voto.vot_secao
                AND
                    urna.urn_zona  = voto.vot_zona
                AND
                	voto.vot_oabCandidato = candidato.cdt_oab 
                AND 
                	candidato.cdt_numeroCandidatura = '" . BRANCO_NUMERO . "'   
                 ) AS totalVotosBrancos , 
                  (SELECT 
                	COUNT(vot_sequencial)
                 FROM
                    voto,
                    candidato ,
                    urna
                WHERE
                    voto.vot_oabCandidato = candidato.cdt_oab
                AND
                    urna.urn_secao = voto.vot_secao
                AND
                    urna.urn_zona  = voto.vot_zona
                AND
                	voto.vot_oabCandidato = candidato.cdt_oab 
                AND 
                	candidato.cdt_numeroCandidatura = '" . NULO_NUMERO .
			"'             
                 ) AS totalVotosNulos , 
                 (SELECT COUNT(vot_sequencial) 
                 FROM
                    voto,
                    candidato ,
                    urna
                WHERE
                    voto.vot_oabCandidato = candidato.cdt_oab
                AND
                    urna.urn_secao = voto.vot_secao
                AND
                    urna.urn_zona  = voto.vot_zona      
                 ) AS totalApurado, 
			 (SELECT count(eleitor.elt_oab)
                FROM eleitor)
                AS totalAptos, 
             (SELECT count(voto.vot_oabCandidato)
                FROM voto) 
                AS totalVotantes,
             (SELECT ABS(
                 (SELECT COUNT(elt_oab) FROM eleitor WHERE elt_votou = 1) - 
                 (SELECT COUNT(elt_oab) FROM eleitor WHERE elt_votou = 0)
                 )) 
                AS totalFaltosos,
				(SELECT urn_cancelamentos 
         FROM urna)
            AS totalCancelamentos";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr[0];
		} else {
			return false;
		}
	}


	public function listaQtdVotosBUPorCargo() {
		$sql = "SELECT 
				  COUNT(vot_sequencial) AS totalApurados,
				  
				  (SELECT 
				    COUNT(vot_sequencial) 
				  FROM
				    voto 
				  WHERE vot_oabCandidato NOT IN (" . BRANCO_NUMERO . "," . NULO_NUMERO . ") 
				    AND voto.vot_car_id = v.vot_car_id 
				  GROUP BY vot_car_id) AS totalVotosNominais,
				  
				  (SELECT 
				    COUNT(vot_sequencial) 
				  FROM
				    voto 
				  WHERE vot_oabCandidato = '" . BRANCO_NUMERO . "' 
				    AND voto.vot_car_id = v.vot_car_id) AS totalVotosBrancos,
				  (SELECT 
				    COUNT(vot_sequencial) 
				  FROM
				    voto 
				  WHERE vot_oabCandidato = '" . NULO_NUMERO . "' 
				    AND voto.vot_car_id = v.vot_car_id) AS totalVotosNulos,
				  vot_car_id AS cargo 
				FROM
				  voto v 
				GROUP BY vot_car_id";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}

	/**
	 * Lista a quantidade de votos por candidato para apuração
	 * 
	 * @param Urna urna 
	 * @return Array valoresQtdEleitoresVotosBu
	 * 
	 */
	public function listaQtdVotosApuracao() {
		$sql = "SELECT 
            (SELECT SUM(vot_sequencial) FROM voto WHERE vot_oabCandidato NOT IN (" .
			BRANCO_NUMERO . "," . NULO_NUMERO . ")) AS totalVotosNominais, 
            (SELECT SUM(vot_sequencial) FROM voto WHERE vot_oabCandidato = '" .
			BRANCO_NUMERO . "') AS totalVotosBrancos , 
            (SELECT SUM(vot_sequencial) FROM voto WHERE vot_oabCandidato = '" .
			NULO_NUMERO . "') AS totalVotosNulos , 
            (SELECT SUM(vot_sequencial) FROM voto) AS totalApurado, 
            (SELECT SUM(urn_totalEleitores) FROM urna) AS totalAptos, 
            (SELECT SUM(vot_sequencial) FROM voto) AS totalVotantes, 
            (SELECT ( SELECT SUM(urn_totalEleitores) FROM urna) - 
                    ( SELECT SUM(vot_sequencial) FROM voto)) AS totalFaltosos";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr[0];
		} else {
			return false;
		}
	}
	/**
	 * Apaga os votos da urna
	 * 
	 * @return void
	 */
	public function apagarVotosUrna() {
		try {
			$this->conexao->beginTransaction();
			$sql = "DELETE FROM voto; UPDATE eleitor SET elt_votou = 0; UPDATE urna set urn_horaZeresima = null, urn_status = 1, urn_cancelamentos = 0, urn_votosContabilizados = 0, urn_libera = 0;";
			$acao = $this->conexao->prepare($sql);
			if (!$acao->execute()) {
				throw $e;
			}
			$this->conexao->commit();
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
			header('Location: /urna/menuUrna/ERROR/');
		}
	}
}
