<?php

/**
 * Classe básica de Voto
 */
class Voto {
  
	/**
	 * Zona a que o voto pertence. 
	 * 
	 * @access private
	 * @var integer
	 */
	private $zona = null;

	/**
	 * Secao que o voto pertence
	 * @access private
	 * @var string
	 */
	private $secao = null;

	/**
	 *
	 * Código da oab do candidato 
	 * 
	 * @access private
	 * @var string
	 */
	private $oabCandidato = null;

	/**
	 * valor sequencial de voto. 
	 * 
	 * @access private
	 * @var string
	 */
	private $sequencial = null;

	/**
	 * Voto::__construct()
	 * 
	 * @param mixed $zona
	 * @param mixed $secao
	 * @param mixed $oabCandidato
	 * @param mixed $sequencial
	 * @return void
	 */
	public function __construct($zona = null, $secao = null, $oabCandidato = null, $sequencial = null) {
		$this->zona = $zona;
		$this->secao = $secao;
		$this->oabCandidato = $oabCandidato;
		$this->sequencial = $sequencial;
	}

	/**
	 * View::__set()
	 * Responsável por setar o valor de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @param mixed $valor
	 * @return void
	 */
	public function __set($chave, $valor) {
		$this->{$chave} = $valor;
	}

	/**
	 * View::__get()
	 * Responsável por interceptar o retorno de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __get($chave) {
		return $this->{$chave};
	}

	/**
	 * View::__isset()
	 * Interfere nas chamadas à função isset()
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __isset($chave) {
		return isset($this->{$chave});
	}

	/**
	 * View::__unset()
	 * Interfere nas chamadas às função unset()
	 * 
	 * @param mixed $chave
	 * @return void
	 */
	public function __unset($chave) {
		unset($this->{$chave});
	}

	/**
	 * View::__call()
	 * É chamado toda vez que um método chamado não é encontrado.
	 * 
	 * @param mixed $nomeDoMetodo
	 * @param mixed $argumentos
	 * @return void
	 */
	public function __call($nomeDoMetodo, $argumentos) {
	}

}
