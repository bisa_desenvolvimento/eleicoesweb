<?php

include_once ("lib/MVC/Model.php");

/**
 * Classe responsável pela comunicação de Candidato com o Banco de Dados.
 * 
 */
class ModelCandidato extends Model {

	/**
	 * ModelCandidato::__construct()
	 * 
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Retorna a lista dos candidatos excluindo o Branco e o Nulo
	 * 
	 * @param void
	 * @return Array<Candidato> listaCandidato
	 * 
	 */
	public function listaCandidato($zona) {
		$sql = "SELECT 
              cdt_oab,
              cdt_nome,
              cdt_numeroCandidatura,
              c.car_id,
              c.car_nome,
              cdt_fotoCandidato
            FROM
              candidato can 
              INNER JOIN cargo c 
                ON can.car_id = c.car_id 
            WHERE cdt_numeroCandidatura NOT IN ('".BRANCO_NUMERO."', '".NULO_NUMERO."') 
              
            ORDER BY car_ordem,
              cdt_nome";
              
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			//$row = $this->montarListaObjetos($arr, 'Candidato');
			return $arr;
		} else {
			return false;
		}
	}

  public function listaCandidatoVotosAputacaoManual($zona,$secao) {
    $sql = "SELECT 
              cdt_oab,
              cdt_nome,
              cdt_numeroCandidatura,
              c.car_id,
              c.car_nome,
              v.vot_sequencial 
            FROM
              candidato can 
              INNER JOIN cargo c 
                ON can.car_id = c.car_id 
              INNER JOIN voto v 
                ON can.cdt_oab = v.vot_oabCandidato 
            WHERE cdt_numeroCandidatura NOT IN ('".BRANCO_NUMERO."', '".NULO_NUMERO."') 
              AND v.vot_zona = '".$zona."' 
              AND v.vot_secao = '".$secao."' 
              AND v.vot_origem = 'M' 
            ORDER BY car_ordem,
              cdt_nome";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			//$row = $this->montarListaObjetos($arr, 'Candidato');
			return $arr;
		} else {
			return false;
		}
  }
  
  public function listaBrancoNuloVotosAputacaoManual($zona,$secao) {
    $sql = "SELECT 
              cdt_oab,
              cdt_nome,
              cdt_numeroCandidatura,
              cdt_fotoCandidato,
              v.vot_sequencial 
            FROM
              voto v 
              INNER JOIN candidato can 
                ON can.cdt_oab = v.vot_oabCandidato 
            WHERE cdt_numeroCandidatura IN ('".BRANCO_NUMERO."', '".NULO_NUMERO."') 
              AND v.vot_zona = '".$zona."' 
              AND v.vot_secao = '".$secao."' 
              AND v.vot_origem = 'M' 
            ORDER BY cdt_nome";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			//$row = $this->montarListaObjetos($arr, 'Candidato');
			return $arr;
		} else {
			return false;
		}
  }

	/**
	 * Retorna os dados do candidato com o número da OAB informado
	 * 
	 * @param codigoOab
	 * @return Array<Candidato> listaCandidato
	 * 
	 */
	public function listaCandidatoPorOab($oab,$cargo) {
		$sql = "SELECT 
                cdt_oab,
                cdt_nome,
                cdt_numeroCandidatura,
                cdt_fotoCandidato
              FROM
                candidato              
              WHERE 
                cdt_oab = '" . $oab . "'
                AND car_id IN ('". $cargo . "','0')";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			$row = $this->montarListaObjetos($arr, 'Candidato');
			return $row[0];
		} else {
			return false;
		}
	}
	

	/**
	 * Retorna a lista de votos de cada candidato na urna sem os brancos e nulos
	 * 
	 * @param Urna urna
	 * @return Array<float> votosCandidatoUrna
	 * 
	 */
	public function pegarApuracaoGeral() {

		header('Content-Type: text/html; charset=iso-8859-1');
		
		$sql = "SELECT 	
			candidato.cdt_nome,
			candidato.cdt_oab,
			SUM(voto.vot_sequencial) AS total
			FROM 
			voto
			INNER JOIN candidato
			ON candidato.cdt_oab = voto.vot_oabCandidato
			WHERE 
			candidato.cdt_situacao = 'D'
			GROUP BY candidato.cdt_oab
			ORDER BY			
			total DESC, 
			candidato.cdt_nota DESC, 
			candidato.cdt_idade DESC";

			$rs = $this->conexao->query($sql);

		//die($sql);
		
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}


	/**
	 * Retorna a lista de votos de cada candidato na urna sem os brancos e nulos
	 * 
	 * @param Urna urna
	 * @return Array<float> votosCandidatoUrna
	 * 
	 */
	public function listaVotosCandidatoUrna($urna) {

		header('Content-Type: text/html; charset=utf-8');

		$sql = "SELECT cdt_oab, 
                       cdt_nome, 
                       cdt_numeroCandidatura, 
                       COALESCE( totalVotos, 0
                               )AS totalVotos,
                       car_nome,
                       candidato.car_id,
                       cdt_nota,
                       cdt_idade
                  FROM
                       candidato AS Candidato LEFT OUTER JOIN( 
                                                                SELECT vot_oabCandidato, 
                                                                       vot_sequencial
                                                                            AS totalVotos
                                                                  FROM voto
                                                                  WHERE vot_secao = '{$urna->secao}'
                                                                    AND vot_zona = '{$urna->zona}'
                                                                  GROUP BY vot_oabCandidato
                                                              )AS UrnaVotos
                       ON UrnaVotos.vot_oabCandidato = candidato.cdt_oab
                       INNER JOIN cargo AS cargo
                       ON Candidato.car_id = cargo.car_id
                  WHERE candidato.cdt_numeroCandidatura NOT IN( '" .
			BRANCO_NUMERO . "','" . NULO_NUMERO . "')
                  ORDER BY car_nome, totalVotos DESC , cdt_nota DESC, cdt_idade DESC, cdt_nome ASC;";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}
	
		/**
	 * Retorna a lista de votos de cada candidato na urna sem os brancos e nulos
	 * 
	 * @param Urna urna
	 * @return Array<float> votosCandidatoUrna
	 * 
	 */
	public function listaVotosCandidatoUrnaApuracao($urna) {
		
		header('Content-Type: text/html; charset=iso-8859-1');
	

		$sql = "SELECT cdt_oab, 
                       cdt_nome, 
                       cdt_numeroCandidatura, 
                       COALESCE( totalVotos, 0
                               )AS totalVotos,
                       car_nome,
                       candidato.car_id,
                       cdt_nota,
                       cdt_idade
                  FROM
                       candidato AS Candidato INNER JOIN( 
                                                                SELECT vot_oabCandidato, 
                                                                       vot_sequencial
                                                                            AS totalVotos
                                                                  FROM voto
                                                                  WHERE vot_secao = '{$urna->secao}'
                                                                    AND vot_zona = '{$urna->zona}'
                                                                  GROUP BY vot_oabCandidato
                                                              )AS UrnaVotos
                       ON UrnaVotos.vot_oabCandidato = candidato.cdt_oab
                       INNER JOIN cargo AS cargo
                       ON Candidato.car_id = cargo.car_id
                  WHERE candidato.cdt_numeroCandidatura NOT IN( '" .
			BRANCO_NUMERO . "','" . NULO_NUMERO . "')
                  ORDER BY totalVotos DESC , cdt_nota DESC, cdt_idade DESC, cdt_nome ASC;";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}
	/**
	 * Retorna a lista de votos brancos e nulos por cargo
	 * 
	 * @param Urna urna
	 * @return Array<float> votosCandidatoUrna 
	 */
	public function listaVotosBrancoNuloCargo($urna) {
		$sql = "SELECT voto.vot_oabCandidato, 
                       candidato.cdt_nome, 
                       COUNT( voto.vot_sequencial
                            )AS totalVotos, 
                       vot_car_id
                  FROM
                       voto INNER JOIN candidato
                       ON voto.vot_oabCandidato = candidato.cdt_oab
                  WHERE vot_oabCandidato IN( '" . BRANCO_OAB . "', '" . NULO_OAB .
			"'
                                           )
                      AND voto.vot_zona = '" . $urna->zona . "'
                      AND voto.vot_secao = '" . $urna->secao .
			"'                
                  GROUP BY voto.vot_oabCandidato, 
                           candidato.cdt_nome, 
                           vot_car_id";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}

	/**
	 * Retorna a lista de votos de cada candidato na apuralção sem os brancos e nulos
	 * 
	 * @param Urna urna
	 * @return Array<float> votosCandidatoUrna
	 */
	public function listaVotosCandidatoApuracao() {
		$sql = "SELECT candidato.cdt_oab,
                       UrnaVotos.vot_car_id,
                       candidato.cdt_nome,
                       candidato.cdt_numerocandidatura,
                       COALESCE(UrnaVotos.totalvotos, 0)AS totalVotos
                FROM   candidato AS CANDIDATO
                       LEFT OUTER JOIN(SELECT vot_oabcandidato,
                                              vot_car_id,
                                              Sum(vot_sequencial)AS totalVotos
                                       FROM   voto
                                       GROUP  BY vot_oabcandidato,
                                                 vot_car_id)AS UrnaVotos
                                    ON UrnaVotos.vot_oabcandidato = candidato.cdt_oab
                ORDER  BY vot_car_id,
                          totalvotos DESC";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}

	/**
	 * Retorna lista de votos de cada candidato na urna incluindo branco e nulo
	 * 
	 * @param Urna urna
	 * @return Array<float> votosCandidatoUrna
	 * 
	 */
	public function listaVotosCandidatoUrnaFull($urna) {
		$sql = "SELECT candidato.cdt_oab, 
                       candidato.cdt_nome, 
                       candidato.cdt_numeroCandidatura, 
                       COALESCE( UrnaVotos.totalVotos, 0
                               )AS totalVotos, 
                       LTRIM( RTRIM( UrnaVotos.car_id
                              )
                       )AS car_id, 
                       car_nome
                  FROM
                       -- candidato AS candidato RIGHT OUTER JOIN( 
                       candidato AS candidato LEFT OUTER JOIN(
                                                                              SELECT vot_oabCandidato, 
                                                                                     vot_sequencial AS totalVotos, 
                                                                                     vot_car_id AS car_id
                                                                                FROM voto
                                                                                WHERE vot_zona = '" .
			$urna->zona . "'
                                                                                  AND vot_secao = '" .
			$urna->secao . "'
																				  AND vot_urna = '" . $urna->urna . "'
                                                                            )AS UrnaVotos
                       ON UrnaVotos.vot_oabCandidato = candidato.cdt_oab
                                                            LEFT OUTER JOIN cargo car
                       ON candidato.car_id = car.car_id
                  ORDER BY totalVotos DESC, cdt_nota DESC, cdt_idade DESC ";

                  //var_dump($sql);die();
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}

		/**
	 * Retorna candidato com status de indeferido
	 * 
	 * @param Candidato numero
	 * @return Array<Candidato> Candidato
	 * 
	 */
	public function buscaCandidatoDeferido($candidato)
	{

		header('Content-Type: text/html; charset=iso-8859-1',true);

		$sql = "SELECT 
				  candidato.cdt_nome,
				  candidato.cdt_oab,
				  secoes.secao_nome,
				  candidato.cdt_situacao AS deferido 
				FROM
				  candidato 
				  INNER JOIN secoes 
				    ON candidato.cdt_id_conselho = secoes.secao_id
				WHERE candidato.cdt_oab = '".$candidato."'";

			   // die($sql);
                 
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}
			/**
	 * Atualiza candidato para deferido
	 * 
	 * @param Candidato status
	 * @return Array<Candidato> Candidato
	 * 
	 */
	public function deferirCandidato($candidato,$status,$protocolo,$imagem)
	{
		

		try {
			$this->conexao->beginTransaction();
			$sql = "UPDATE candidato SET cdt_situacao = '".$status."' WHERE cdt_oab = '".$candidato."'";
			$acao = $this->conexao->prepare($sql);
			$acao->execute();
			$this->conexao->commit();

			$this->conexao->beginTransaction();
			$sql = "INSERT INTO deferimento (
					  cdt_oab,
					  cdt_protocolo,
					  cdt_imagem,
					  cdt_data_alteracao,
					  cdt_status
					) 
					VALUES
					  (
					    '".$candidato."',
					    '".$protocolo."',
					    '".$imagem."',
					     NOW(),
					    '".$status."'
					  )";
			$acao = $this->conexao->prepare($sql);

			$acao->execute();
			$this->conexao->commit();

		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
			return false;
		}
		return true;
	}

	public function uploadImagemRequerimento($file)
	{
		$caminho = CAMINHO_PORTAL."/templates/padrao/imagemProtocolo/";
		date_default_timezone_set("Brazil/East");
		$tipo = strtolower(substr($_FILES['imagem']['name'], -4));
		$nome = date("Y-m-d-H-i-s").$tipo;
		
		if (move_uploaded_file($_FILES['imagem']['tmp_name'], $caminho.$nome)){
			return $nome;
		}else { 
			return false;
		}

	}
  
  public function listaVotosCandidatoUrnaShow($urna) {
    $sql = "SELECT 
              cdt_oab,
              cdt_nome,
              cdt_numeroCandidatura,
              cdt_fotoCandidato,
              SUM(vot_sequencial) AS vot_sequencial
            FROM
              voto 
              INNER JOIN candidato
                ON vot_oabCandidato = cdt_oab
            WHERE vot_zona = '".$urna->zona."' 
              AND vot_secao = '".$urna->secao."' 
              AND vot_oabCandidato NOT IN ('".BRANCO_OAB."','".NULO_OAB."') 
              AND vot_sequencial > 0 
              AND cdt_situacao = 'D'"; 
    if($urna->origem == 'E' || $urna->origem == 'M') {
    $sql .= " AND vot_origem = '".$urna->origem."'";
    }    
    $sql .= " GROUP BY cdt_oab 
              ORDER BY vot_sequencial DESC, cdt_nota DESC, cdt_idade DESC, cdt_nome ASC";
    $rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
  }
  
  public function listaVotosCandidatoUrnaZeroShow($urna) {
    $sql = "SELECT 
              cdt_oab,
              cdt_nome,
              cdt_numeroCandidatura,
              cdt_fotoCandidato,
              SUM(vot_sequencial) AS vot_sequencial
            FROM
              voto 
              INNER JOIN candidato
                ON vot_oabCandidato = cdt_oab
            WHERE vot_zona = '".$urna->zona."' 
              AND vot_secao = '".$urna->secao."' 
              AND vot_oabCandidato NOT IN ('".BRANCO_OAB."','".NULO_OAB."') 
              AND vot_sequencial = 0 
              AND cdt_situacao = 'D'"; ; 
    if($urna->origem == 'E' || $urna->origem == 'M') {
    $sql .= " AND vot_origem = '".$urna->origem."'";
    }    
    $sql .= " GROUP BY cdt_oab 
              ORDER BY cdt_nome ASC";
              
    $rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
  }
  
  public function listaVotosBrancoNuloUrnaShow($urna) {
   

    $sql = "(SELECT 
              cdt_oab,
              cdt_nome,
              cdt_numeroCandidatura,
              cdt_fotoCandidato,
              SUM(vot_sequencial) as votos 
            FROM
              voto 
              INNER JOIN candidato 
                ON vot_oabCandidato = cdt_oab 
            WHERE vot_zona = '".$urna->zona."' 
              AND vot_secao = '".$urna->secao."'"; 
              if($urna->origem == 'E' || $urna->origem == 'M') {
                $sql .= " AND vot_origem = '".$urna->origem."'";
              }    
              $sql .= " AND (
                vot_oabCandidato = '".NULO_OAB."' 
                OR cdt_situacao = 'I'
              )) 
            UNION
            (SELECT 
              cdt_oab,
              cdt_nome,
              cdt_numeroCandidatura,
              cdt_fotoCandidato,
              SUM(vot_sequencial) AS vot_sequencial 
            FROM
              voto 
              INNER JOIN candidato 
                ON vot_oabCandidato = cdt_oab 
            WHERE vot_zona = '".$urna->zona."' 
              AND vot_secao = '".$urna->secao."'"; 
              if($urna->origem == 'E' || $urna->origem == 'M') {
                $sql .= " AND vot_origem = '".$urna->origem."'";
              }    
              $sql .= " AND vot_oabCandidato = '".BRANCO_OAB."'
              GROUP BY cdt_oab)";
    $rs = $this->conexao->query($sql); 
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
  }

	public function relatorioIndeferidos()
	{
		header('Content-Type: text/html; charset=iso-8859-1',true);
		$sql = "SELECT 
				  candidato.`cdt_oab`,
				  candidato.`cdt_nome`,
				  candidato.`cdt_nome_conselho`,
				  IF(candidato.cdt_situacao='I','INDEFERIDO','DEFERIDO') AS cdt_situacao
				FROM
				  candidato
				ORDER BY cdt_situacao DESC, cdt_nome ASC";
		$rs = $this->conexao->query($sql);

		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}

	public function pegarBrancoNulosTotal()
	{
		$sql = "(SELECT 
				  'NULO' AS cdt_nome,
				  '99999' AS cdt_numeroCandidatura,
				  SUM(vot_sequencial) AS votos 
				FROM
				  voto 
				  INNER JOIN candidato 
				    ON vot_oabCandidato = cdt_oab 
				WHERE vot_oabCandidato = '99999' 
				  OR cdt_situacao = 'I') 
				UNION
				(SELECT 
				  cdt_nome,
				  cdt_numeroCandidatura,
				  SUM(vot_sequencial) AS votos 
				FROM
				  voto 
				  INNER JOIN candidato 
				    ON vot_oabCandidato = cdt_oab 
				WHERE vot_oabCandidato = '0')";

		$rs = $this->conexao->query($sql);
		
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
			
	}

}
