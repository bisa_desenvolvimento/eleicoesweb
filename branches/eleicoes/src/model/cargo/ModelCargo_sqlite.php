<?php

include_once ("lib/MVC/Model.php");

/**
 * [Classe responsável pela comunicação de Cargo com o Banco de Dados].
 * 
 */
class ModelCargo extends Model {

	/**
	 * ModelCargo::__construct()
	 * 
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * listaCargo 
	 * 
	 * @param void
	 * @return Array<Cargo> listaCargo
	 * 
	 */
	public function listaCargo() {
		$sql = "SELECT 
                    car_id,
                    car_nome,
                    car_ordem,
                    car_votos
                FROM
                    cargo
                WHERE
                    car_id > 0
                ORDER BY 
                    car_ordem";

		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			return false;
		}
	}

}
