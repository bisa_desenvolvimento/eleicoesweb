<?php

include_once ("lib/MVC/Model.php");

/**
 * Classe responsável pela comunicação entre a classe usuário e o banco de dados. 
 *
 */
class ModelUsuario extends Model {

	/**
	 * ModelUsuario::__construct()
	 * 
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Pesquisa o usuário no banco de dados
	 * 
	 * @param String login - Login do usuário 
	 * @param String senha - Senha do usuário 
	 * @return Usuario objUsuario
	 */
	public function pesquisarAcesso($login, $senha) {
		$sql = "SELECT usu_id, usu_login, usu_senha, usu_perfilUsuario from usuario where usu_login = '" .
			$login . "' AND usu_senha = '" . md5($senha) . "';";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			$row = $this->montarObjeto($arr[0], 'Usuario');
			return $row;
		} else {
			return false;
		}
	}

}
