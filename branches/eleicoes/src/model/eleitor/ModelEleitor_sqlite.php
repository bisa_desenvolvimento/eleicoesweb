<?php

require_once ("lib/MVC/Model.php");

/**
 * Classe esponsável pela comunicação entre a classe Eleitor e o Banco de dados.
 */
class ModelEleitor extends Model {

	/**
	 * ModelEleitor::__construct()
	 * 
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Retorna o nome do eleitor e se o mesmo já votou
	 * 
	 * @param string eleitor - Código da oab do eleitor
	 * @return Eleitor objEleitor
	 * 
	 */
	public function procurarEleitor($eleitor) {
		$sql = "SELECT 
                  elt_nome,
                  elt_votou 
                FROM
                  eleitor 
                WHERE elt_oab = '" . $eleitor . "'";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr[0];
		} else {
			return false;
		}
	}

	/**
	 * Retorna a quantidade de eleitores da urna, quantos compareceram à eleição e 
	 * quantos faltaram
	 * 
	 * @param Urna urna - Código da oab do eleitor
	 * @return Array<float> valoresQuantidadeEleitores
	 * 
	 */
	public function getQuantidadeEleitores($urna) {


		$sql = "SELECT( 
                    SELECT COUNT( elt_oab
                                )
                      FROM eleitor
                      WHERE urna_urn_zona = '{$urna->zona}'
                        AND urna_urn_secao = '{$urna->secao}'
                  )AS qtdAptos, 
                  ( 
                    SELECT COUNT( elt_oab
                                )
                      FROM eleitor
                      WHERE urna_urn_zona = '{$urna->zona}'
                        AND urna_urn_secao = '{$urna->secao}'
                        AND elt_votou = 1
                  )AS qtdComparecimento, 
                  ( 
                    SELECT COUNT( elt_oab
                                )
                      FROM eleitor
                      WHERE urna_urn_zona = '{$urna->zona}'
                        AND urna_urn_secao = '{$urna->secao}'
                        AND elt_votou = 0
                  )AS qtdFaltosos;";

		$rs = $this->conexao->query($sql);
		if ($rs) {
			
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			die('ali');
			return false;
		}
	}

	/**
	 * Retorna a quantidade de eleitores da urna, quantos compareceram à eleição e 
	 * quantos faltaram para apuração
	 * 
	 * @param Urna urna - Código da oab do eleitor
	 * @return Array<float> valoresQuantidadeEleitores
	 * 
	 */
	public function getQuantidadeEleitoresApuracao($urna) {


		$sql = "SELECT 
				  (SELECT 
				    COUNT(elt_oab) 
				  FROM
				    eleitor 
				  WHERE urna_urn_zona = '{$urna->zona}'
                    AND urna_urn_secao = '{$urna->secao}')
					AS qtdAptos,
				  (SELECT 
				    SUM(vot_sequencial) 
				  FROM
				    voto 
				  WHERE vot_zona = '{$urna->zona}'
				    AND vot_secao = '{$urna->secao}')
					AS qtdComparecimento,
				  (SELECT 
				    COUNT(elt_oab) 
				  FROM
				    eleitor 
				  WHERE urna_urn_zona = '{$urna->zona}'
				    AND urna_urn_secao = '{$urna->secao}' 
				    AND elt_votou = 0) AS qtdFaltosos;";

		$rs = $this->conexao->query($sql);

		//die($sql);
		if ($rs) {
			
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $arr;
		} else {
			die('ali');
			return false;
		}
	}

	/**
	 * Retorna a listagem dos eleitores cadastrados na urna
	 * 
	 * @param String zona  - Código da zona
	 * @param String secao - Código da secao
	 * @return Array<Eleitor> objEleitor
	 * 
	 */
	public function listaEleitores($zona, $secao) {
		$sql = " SELECT elt_oab
                      ,elt_nome
                      ,elt_votou
                      ,urna_urn_zona
                      ,urna_urn_secao
                  FROM 
                    eleitor
                  WHERE 
                    urna_urn_zona = '" . $zona . "'
                  AND
                    urna_urn_secao = '" . $secao . "'
                  ORDER 
                    BY elt_oab ";
		$rs = $this->conexao->query($sql);
		if ($rs) {
			$arr = $rs->fetchAll(PDO::FETCH_ASSOC);
			return $this->montarListaObjetos($arr, 'Eleitor');
		} else {
			return false;
		}
	}

	/**
	 * Atualiza o status do eleitor para 0, atualizando o status para 'não votaram'
	 * 
	 * @return void
	 */
	public function zerarStatusEleitor() {
		try {
			$this->conexao->beginTransaction();
			$sql = "UPDATE eleitor SET elt_votou = 0";
			$acao = $this->conexao->prepare($sql);
			if (!$acao->execute()) {
				throw $e;
			} else {
				$sql = "UPDATE eleitor SET elt_votou = 0";
				$acao = $this->conexao->prepare($sql);
				if (!$acao->execute()) {
					throw $e;
				} else {
					echo 'OK';
					$this->conexao->commit();
					return 'OK';
				}
			}
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
			header('Location: /urna/menuUrna/ERROR/');
		}
	}

	/**
	 * ModelEleitor::AddEleitor()
	 * 
	 * @param mixed $objEleitor
	 * @return void
	 */
	public function AddEleitor($objEleitor) {
		try {
			$this->conexao->beginTransaction();
			$sql = "INSERT INTO eleitor (elt_oab ,elt_nome ,elt_votou ,urna_urn_zona ,urna_urn_secao) VALUES ('" .
				$objEleitor->num . "' ,'" . $objEleitor->nome . "' ,0 ,'" . $objEleitor->zona .
				"' ,'" . $objEleitor->secao . "')";
			$acao = $this->conexao->prepare($sql);
			if (!$acao->execute()) {
				$this->conexao->rollBack();
			}
			$this->conexao->commit();
		}
		catch (PDOException $e) {
			$this->conexao->rollBack();
			header('Location: /eleitor/addEleitores');
		}
	}

	/**
	 * ModelEleitor::zerarEleitores()
	 * 
	 * @return void
	 */
	public function zerarEleitores() {
		$sql = "DELETE FROM voto;
                DELETE FROM eleitor;";
		$acao = $this->conexao->prepare($sql);
		$acao->execute();
	}

}
