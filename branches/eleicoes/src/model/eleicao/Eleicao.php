<?php

/**
 * [Classe básica de Eleição].
 * 
 */

class Eleicao {

	/**
	 * id da eleição
	 * @access private
	 * @var integer
	 */
	private $id;

	/**
	 * Descrição da eleição(título)
	 * @access private
	 * @var string
	 */
	private $descricao;

	/**
	 * Data da eleição.
	 * @access private
	 * @var string
	 */
	private $data;


	/**
	 * hora Início da eleição
	 * @access private
	 * @var string
	 */
	private $horaInicio;

	/**
	 * hora término da eleição
	 * @access private
	 * @var string
	 */
	private $horaTermino;

	/**
	 * Quantidade máxima de votos por candidato na eleição
	 * @access private
	 * @var string
	 */
	private $qtdVotosCandidatos;

	/**
	 * Eleicao::__construct()
	 * 
	 * @param mixed $id
	 * @param mixed $descricao
	 * @param mixed $data
	 * @param mixed $horaInicio
	 * @param mixed $horaTermino
	 * @param mixed $qtdVotosCandidatos
	 * @return void
	 */
	public function __construct($id = null, $descricao = null, $data = null, $horaInicio = null,
		$horaTermino = null, $qtdVotosCandidatos = null) {
		$this->id = $id;
		$this->descricao = $descricao;
		$this->data = $data;
		$this->horaInicio = $horaInicio;
		$this->horaTermino = $horaTermino;
		$this->qtdVotosCandidatos = $qtdVotosCandidatos;
	}

	/**
	 * View::__set()
	 * Responsável por setar o valor de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @param mixed $valor
	 * @return void
	 */
	public function __set($chave, $valor) {
		$this->{$chave} = $valor;
	}

	/**
	 * View::__get()
	 * Responsável por interceptar o retorno de uma propriedade.
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __get($chave) {
		return $this->{$chave};
	}

	/**
	 * View::__isset()
	 * Interfere nas chamadas à função isset()
	 * 
	 * @param mixed $chave
	 * @return
	 */
	public function __isset($chave) {
		return isset($this->{$chave});
	}

	/**
	 * View::__unset()
	 * Interfere nas chamadas às função unset()
	 * 
	 * @param mixed $chave
	 * @return void
	 */
	public function __unset($chave) {
		unset($this->{$chave});
	}

	/**
	 * View::__call()
	 * É chamado toda vez que um método chamado não é encontrado.
	 * 
	 * @param mixed $nomeDoMetodo
	 * @param mixed $argumentos
	 * @return void
	 */
	public function __call($nomeDoMetodo, $argumentos) {
	}

}
