<?
/**
 * Classe de conexão com o banco local.
 * 
 */
class ConexaoMySql
{
    
	private static $con = NULL;
    
    /**
    * Cria a conexão com a base de dados
    * 
    * @static
    * @return resource conexão ocm o banco
    */
    public static function getConexao() 
	{
        if (ConexaoMySql::$con == NULL) { 	

			include "config.inc.php";		

			try {
                ConexaoMySql::$con = new PDO('mysql:host=mysql.bisaweb.com.br;
				                         port=3306;
				                         dbname=elecoesweb_config',
				                         'eleicoesweb', 
				                         'abc1@#');
            }catch (PDOException $e) {
			    //levanta erro
		    
                ConexaoMySql::$con = new PDO(BD_TYPE.':Server='.BD_HOST.';
				                         Database='.BD_BASE_MIRROR,
				                         BD_USER, 
				                         BD_PASS);
			}
	   	}
		return ConexaoMySql::$con;
	}
	
    /**
     * Verifica o último id inserido no banco e retorna o próximo id 
     * 
     * @param int sequência
     * @return int próximo id
     */ 
	public static function nextId($sequence)
	{
die('aqui');
	  	$sql = "SELECT nextval('$sequence') AS seq";
		
		$con = Conexao::getConexao();
		try {
			$arr = $con->query($sql);
			if($arr == ""){
				throw new Exception("Erro");
			}
			$row = $arr->fetch(PDO::FETCH_ASSOC);
			return $row['seq'];
		}catch (Exception $e){
			return 0;
		}
		
	}
	  
    /**
     * fecha a conexão com o banco
     * 
     * @return void
     */
	public static function close()
	{
	  	ConexaoMySql::$con = NULL;
	 }
		
	}
