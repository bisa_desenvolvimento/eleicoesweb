<?php
/**
 * Functions
 * 
 */
class Functions
{

	/**
     * Converte data entre padrões brasileiro e americano
     * 
     * @param string data
     * @return string data
     */
	function convertDate($date) 
	{
		if (trim($date) != ''):
			if (strstr($date, '-')): // Data no formato 12-10-2012
				$date = explode('-', $date);
				$date = $date[2].'/'.$date[1].'/'.$date[0];
			else:
				$date = explode('/', $date); // Data no formato 12/10/2012
				$date = $date[2].'-'.$date[1].'-'.$date[0];
			endif;
			
		endif;
		
		return $date;
			
	}
	
    /**
     * Converte data e horário entre padrões brasileiro e americano
     * 
     * @param string data
     * @return string data
     */
	function convertDateTime($date) 
	{
		if (trim($date) != ''):
			if (strstr($date, '-')): // Formato x-x-x
                $date = explode(" ", $date);
                $hora = $date[1];
				$date = explode('-', $date[0]);
				$date = $date[2].'/'.$date[1].'/'.$date[0].' '.$hora;
			else:
				$date = explode(" ", $date);
                $hora = $date[1];
                $date = explode('/', $date[1]);
				$date = $date[2].'-'.$date[1].'-'.$date[0].' '.$hora;
			endif;
			
		endif;
		
		return $date;
			
	}

    /**
     * Remove a hora do dateTime
     * 
     * @param string dateTime
     * @return string data
     */
    function removeHora($dataHora)
    {
        $dataHora = explode(' ', $dataHora);
        if (count($dataHora) > 0):
            return $dataHora[0];
        else:
            return false;
        endif;
    }
    
    /**
     * Remove a data do dateTime
     * 
     * @param string dateTime
     * @return string hora
     */
    function removeData($dataHora)
    {
        $dataHora = explode(' ', $dataHora);
        if (count($dataHora) > 0):
            $hora = $dataHora[1];
            $hora = explode('.', $hora);
            if (count($hora) > 0):
                return $hora[0];
            else:
                return false;
            endif;
        else:
            return false;
        endif;
    }
    
    /**
     * Adiciona pontos separatórios no valor passado
     * 
     * @param string valor
     * @return string valor pontuado
     */
	function pontinho($valor)
	{
	   
       $valor = sha1($valor);
		
		$string = '';
		
		$cont = 1;
		
		for($x = 0; $x < strlen($valor); $x++):
			
			$string .= $valor[$x];
			
			if ($cont == 2):
				
				$cont = 1;
				if ($x < strlen($valor) - 1):
					$string .= '.';
				endif;
				
			else:
				$cont++;
			endif;
			
				
			//endif;
			
		endfor;
		
		return $string;
		
	}
	
    /**
     * Retorna apenas números ou letras do valor passado
     * 
     * @param string valor
     * @param int tipo (1 somente número, 2 somente letras) 
     * @return
     */
	function removeNumeroLetra($valor, $tipo)
	{
		
		$string = '';
		
		if ($tipo == 1): // Somente número
		
			$string = preg_replace("/[a-zA-Z]/", "", $valor);  
		
		elseif ($tipo == 2): // Somente letras
			
			$string = preg_replace('/[0-9]/', '', $valor);
			
		endif;
		
		$string = str_replace(".", "", $string);
		
		return $string;
		
	}
    
    function montaZeros($value)
    {
        $zero = '';
        
        for($x = 0; $x < 5 - strlen($value); $x++):
        
            $zero .= '0';
                        
        endfor;
        ;
        return $zero.$value;
        
    }
    
    function existeArquivo($arquivo)
    {
        return file_exists($arquivo);
    }
    
    function diffDate($d1, $d2, $type='', $sep='-')
    {
        $d1 = explode($sep, $d1);
        $d2 = explode($sep, $d2);
        switch ($type)
        {
            case 'A':
                $X = 31536000;
                break;
            case 'M':
                $X = 2592000;
                break;
            case 'D':
                $X = 86400;
                break;
            case 'H':
                $X = 3600;
                break;
            case 'MI':
                $X = 60;
                break;
            default:
                $X = 1;
        }
        
        $data1 = mktime(0, 0, 0, $d1[1], $d1[2], $d1[0] );
        $data2 = mktime(0, 0, 0, $d2[1], $d2[2], $d2[0]);
        
        $retorno = floor( ($data1 - $data2) / $X); 
        
        return $retorno;
        
    }
 
    function remover_espacos($texto) {
        return preg_replace('/\s/', '', $texto);
    }
    
    function remover_acentos($IO) {
        $IM=ereg_replace(" ","_",$IO);
        $IM=strtolower($IM);
        $IM = str_replace ("á","a", $IM);
        $IM = str_replace ("é","e", $IM);
        $IM = str_replace ("í","i", $IM);
        $IM = str_replace ("ó","o", $IM);
        $IM = str_replace ("ú","u", $IM);
        $IM = str_replace ("ã","a", $IM);
        $IM = str_replace ("õ","o", $IM);
        $IM = str_replace ("â","a", $IM);
        $IM = str_replace ("ê","e", $IM);
        $IM = str_replace ("ô","o", $IM);
        $IM = str_replace ("ü","u", $IM);
        $IM = str_replace ("ç","c", $IM);
        $IM = str_replace ("Á","A", $IM);
        $IM = str_replace ("É","E", $IM);
        $IM = str_replace ("Í","I", $IM);
        $IM = str_replace ("Ó","O", $IM);
        $IM = str_replace ("Ú","U", $IM);
        $IM = str_replace ("Ç","C", $IM);
        return $IM;
    }
    
    function replaceString($string)
    {
        return strtolower(str_replace('oab_', '', $string));
    }
    
    public static function dbug($par) {
        echo '<pre>';
        print_r($par);
        exit;
    }
    
   function timeDiff($firstTime,$lastTime)
    {
        $firstTime=strtotime($firstTime);
        $lastTime=strtotime($lastTime);
        $timeDiff=$lastTime-$firstTime;
        return $timeDiff;
    }
    
    function gerarCodigoConfig($valor, $range)
    {
        if ($range > 0):
            $range += 3;
        endif;
        return substr(strtoupper($valor), $range, 4);
    }

    /**
     * Envia uma requisição post HTTP
     *
     * @param $data array Dados para enviar no corpo da requisição
     */
    function sendRequest($data, $url) {
        $data_string = json_encode($data);

        $ch = curl_init(WINDOW_MANAGER . "/$url");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        return $result;
    }

    /**
     * Função que trava o foco da tela no eleitor
     */
    function lockEleitor() {

        $data = array(
            "operation" => "focus",
            "name" => "eleitor"
        );

        $url = "screen";

        $result = $this->sendRequest($data, $url);

        return $result;

    }

    /**
     * Função que trava o foco da tela no mesário
     */
    function lockMesario() {
        $data = array(
            "operation" => "focus",
            "name" => "mesario"
        );

        $url = "screen";

        $result = $this->sendRequest($data, $url);

        return $result;

    }

    /**
     * Libera foco para outras telas que não mesário e eleitor
     */
    function releaseScreen() {
        $data = array(
            "operation" => "release",
            "name" => "mesario"
        );

        $url = "screen";

        $result = $this->sendRequest($data, $url);

        return $result;
    }

    /**
     * Função que ativa o bloqueio da tecla numlock no teclado normal
     * quando um teclado USB estiver bloqueado
     */
    function lockTeclado() {
        $data = array(
            "operation" => "lock",
            "name" => "Genius" // FIXME: colocar aqui o nome do teclado
        );

        $url = "keyboard";

        $result = $this->sendRequest($data, $url);

        return $result;

    }

    function copyMirror() {
        // Primeiro pega o diretório
        $origin = CAMINHO_PORTAL . "banco/" . BD_BASE_LOCAL . ".db";
        $mirror = BD_BASE_MIRROR . ".db";

        $data = array(
            "operation" => "cp",
            "origin" => $origin,
            "mirror_name" => $mirror
        );

        $url = "mirror";

        $result = $this->sendRequest($data, $url);

        return $result;
    }

    function restoreMirror() {
        // Primeiro pega o diretório
        $origin = CAMINHO_PORTAL . "banco/" . BD_BASE_LOCAL . ".db";
        $mirror = BD_BASE_MIRROR . ".db";

        $data = array(
            "operation" => "restore",
            "origin" => $origin,
            "mirror_name" => $mirror
        );

        $url = "mirror";

        $result = $this->sendRequest($data, $url);

        return $result;
    }
    
 }

?>
