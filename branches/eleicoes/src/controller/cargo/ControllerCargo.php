<?php

include_once ("lib/MVC/View.php");
include_once ("lib/MVC/Controller.php");
include_once ("model/Cargo/Cargo_" . BD_TYPE . ".php");

/**
 * ControllerCargo
 * 
 */
class ControllerCargo extends Controller {

	/**
	 * Método padrão que será chamado caso nenhum método seja especificado na url.
	 * Chama o método menuCargo
	 * 
	 * @return void
	 */
	public function acaoPadrao() {}

}
