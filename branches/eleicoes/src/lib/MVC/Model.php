<?php

/**
 * Model
 */
class Model {

	protected $conexao = null;
	protected $conexaoMirror = null;
	protected $conexaoMySql = null;

	/**
	 * Model::__construct()
	 * 
	 * @return void
	 */
	public function __construct() {
		require_once (CAMINHO_PORTAL . "conf/Conexao.php");
		//require_once (CAMINHO_PORTAL . "conf/ConexaoMySql.php");
		$this->conexao = Conexao::getConexao();
		//$this->conexaoMirror = Conexao::getConexaoMirror();
	}

	/**
	 * Model::__call()
	 * O método __call  chamado toda vez que um método  chamado não é encontrado.
	 * 
	 * @param mixed $nomeDoMetodo
	 * @param mixed $argumentos
	 * @return void
	 */
	public function __call($nomeDoMetodo, $argumentos) {
		throw new Exception('Model nao encontrado!' . $nomeDoMetodo);
	}

	/**
	 * Model::montarListaObjetos()
	 * 
	 * @param mixed $array
	 * @param mixed $tipoObjeto
	 * @return
	 */
	public function montarListaObjetos($array, $tipoObjeto) {
		$resultado = array();
		for ($i = 0; $i < count($array); $i++) {
			$arr = $this->montarObjeto($array[$i], $tipoObjeto);
			$resultado[$i] = $arr;
		}
		return $resultado;
	}

	/**
	 * Model::montarObjeto()
	 * 
	 * @param mixed $arrayCampos
	 * @param mixed $tipoObjeto
	 * @return
	 */
	public function montarObjeto($arrayCampos, $tipoObjeto) {
		$modulo = strtolower($tipoObjeto);
		$caminho = "model/" . $modulo . "/";
		eval('require_once("' . CAMINHO_PORTAL . $caminho . $tipoObjeto . '.php");');
		if (sizeof($arrayCampos) > 0) {
			$stringCampos = '';
			foreach ($arrayCampos as $valor) {
				$stringCampos .= " '" . $valor . "',";
			}
			eval('$temp = new ' . $tipoObjeto . '(' . substr($stringCampos, 0, -1) . ');');
			return $temp;
		} else {
			return null;
		}
	}

}
