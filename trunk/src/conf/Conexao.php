<?php
//exit($_SESSION['CAMINHO_MIRROR'].BD_BASE_MIRROR.".db");
/**
 * Classe de conexão com o banco local.
 * 
 */
class Conexao
{
	private static $con = NULL;
	private static $conMirror = NULL;
    
    /**
    * Cria a conexão com a base de dados
    * 
    * @static
    * @return resource conexão ocm o banco
    */
    public static function getConexao()
	{
        if (Conexao::$con == NULL) { 	

			include "config.inc.php";		

			try {
			     if (BD_TYPE == 'mysql'):
				
                    Conexao::$con = new PDO('mysql:host='.BD_HOST.';
    				                         port='.BD_PORT.';
    				                         dbname='.BD_BASE,
    				                         BD_USER, 
    				                         BD_PASS);
    				
                elseif (BD_TYPE == 'sqlsrv'):
                    Conexao::$con = new PDO(BD_TYPE.':Server='.BD_HOST.';
    				                         Database='.BD_BASE,
    				                         BD_USER, 
    				                         BD_PASS);
    				
                elseif (BD_TYPE == 'sqlite'):
                    
		    Conexao::$con = new PDO("sqlite:".CAMINHO_PORTAL."banco/".BD_BASE.".db");
                    //exit("sqlite:".CAMINHO_PORTAL."banco/".BD_BASE.".db");
                    //Conexao::$conMirror = new PDO("sqlite:".$_SESSION['CAMINHO_MIRROR'].BD_BASE_MIRROR.".db");
                    
			    endif;
            }catch (PDOException $e) {
			    //levanta erro
			    echo 'erro:'.$e;
                   // Conexao::$con = new PDO(BD_TYPE.':Server='.BD_HOST.';
//    				                         Database='.BD_BASE_MIRROR,
//    				                         BD_USER, 
//    				                         BD_PASS);
			}
	   	}
		return Conexao::$con;
	}
	
    public static function getConexaoMirror()
	{
        if (Conexao::$conMirror == NULL) { 	

			include "config.inc.php";		

			try {
			     if (BD_TYPE == 'sqlite'):
                    Conexao::$conMirror = new PDO("sqlite:".$_SESSION['CAMINHO_MIRROR'].BD_BASE_MIRROR.".db");
			    endif;
            }catch (PDOException $e) {
			    //levanta erro
			    
                    Conexao::$conMirror = new PDO(BD_TYPE.':Server='.BD_HOST.';
    				                         Database='.BD_BASE_MIRROR,
    				                         BD_USER, 
    				                         BD_PASS);
			}
	   	}
		return Conexao::$conMirror;
	}
	
    /**
     * Verifica o último id inserido no banco e retorna o próximo id 
     * 
     * @param int sequência
     * @return int próximo id
     */ 
	public static function nextId($sequence)
	{
	  	$sql = "SELECT nextval('$sequence') AS seq";
		
		$con = Conexao::getConexao();
		try {
			$arr = $con->query($sql);
			if($arr == ""){
				throw new Exception("Erro");
			}
			$row = $arr->fetch(PDO::FETCH_ASSOC);
			return $row['seq'];
		}catch (Exception $e){
			return 0;
		}
		
	}
	  
    /**
     * fecha a conexão com o banco
     * 
     * @return void
     */
	public static function close()
	{
	  	Conexao::$con = NULL;
	 }
		
	}
