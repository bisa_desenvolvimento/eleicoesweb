<?php

include_once ("lib/MVC/Controller.php");
include_once ("lib/MVC/View.php");
include_once ("model/usuario/ModelUsuario_" . BD_TYPE . ".php");
include_once ("controller/eleicao/ControllerEleicao.php");

/**
 * ControllerUsuario
 * 
 */
class ControllerUsuario extends Controller {

	/**
	 * Verifica se o usuário está logado, caso contrário redireciona para a tela de login.
	 * 
	 * @return void
	 */
	public static function seLogado() {
		if (!$_SESSION['usuarioLogado']) {
			$controllerUsuario = new controllerUsuario();
			$controllerUsuario->login();
			die();
		} else {
			return true;
		}
	}

	/**
	 * Verifica se mesário está logado, caso contrário redireciona para a tela de espera de votação no terminal do eleitor.
	 * 
	 * @return void
	 */
	public static function seMesarioLogado() {
		if (!$_SESSION['usuarioLogado']) {
			$controllerUrna = new ControllerUrna();
			$controllerUrna->standByEleitorInicio();
			die();
		}
	}

	/**
	 * Verifica se administrador está logado, caso contrário redireciona para a tela de menu urna.
	 * 
	 * @return void
	 */
	public static function seAdministradorLogado() {
		if (!$_SESSION['usuarioLogado'] || $_SESSION['usuario']['perfil'] != 1) {
			$controllerUrna = new ControllerUrna();
			$controllerUrna->menuUrna();
			die();
		}
	}

	/**
	 * Método padrão que será chamado caso nenhum método seja especificado na classe.
	 * Enviará o usuário para a tela de login.
	 * 
	 * @return void
	 */
	public function acaoPadrao($parametros) {
		$this->usuarioHome($parametros);
	}

	/**
	 * Verifica se usuário está logado, caso naõ estaja envia envia para a tela de login, caso esteja verifica se é 
	 * administrador ou mesário e envia para a respectiva tela.
	 * 
	 * @return void
	 */
	public function login($parametros = null) {
		if (!$_SESSION['usuarioLogado']) {
			$view = new View();
			$view->tipo = 'branco';
			if (!file_exists(CAMINHO_PORTAL . 'batch/')) {
				mkdir(CAMINHO_PORTAL . 'batch/', 0777);
				chmod(CAMINHO_PORTAL . 'batch/', 0777);
			}
			if (BD_TYPE == 'sqlsrv') {
				$arrFiles = array(
					'backupDbEleicao',
					'backupDbEleicaoMirror',
					'renameInstanceDbEleicao',
					'renameInstanceDbEleicaoMirror',
					'restoreDbEleicao',
					'restoreDbEleicaoMirror');
				$arrCommands = array(
					"USE " . BD_BASE . " EXEC dbo.sp_backup_banco " . BD_BASE . ", 'C:\SQLData\'",
					"USE " . BD_BASE_MIRROR . " EXEC dbo.sp_backup_banco " . BD_BASE_MIRROR . ", 'C:\SQLData\'",
					"ALTER DATABASE " . BD_BASE . " MODIFY FILE (name = " . BD_BASE_MIRROR .
						", newname = " . BD_BASE . "); ALTER DATABASE " . BD_BASE .
						" MODIFY FILE (name = " . BD_BASE_MIRROR . "_log, newname = " . BD_BASE .
						"_log);",
					"ALTER DATABASE " . BD_BASE_MIRROR . " MODIFY FILE (name = " . BD_BASE .
						", newname = " . BD_BASE_MIRROR . "); ALTER DATABASE " . BD_BASE_MIRROR .
						" MODIFY FILE (name = " . BD_BASE . "_log, newname = " . BD_BASE_MIRROR .
						"_log);",
					"USE " . BD_BASE . " EXEC dbo.sp_restore_database " . BD_BASE . ", " .
						BD_BASE_MIRROR . ", 'C:\SQLData\'",
					"USE " . BD_BASE_MIRROR . " EXEC dbo.sp_restore_database " . BD_BASE_MIRROR .
						", " . BD_BASE . ", 'C:\SQLData\'",
					);
				for ($x = 0; $x < count($arrFiles); $x++) {
					$handle = fopen(CAMINHO_PORTAL . 'batch/' . $arrFiles[$x] . '.bat', "w+");
					$contentConfig = 'sqlcmd -E -S "' . BD_HOST . '" -Q "' . $arrCommands[$x] . '"';
					fwrite($handle, $contentConfig);
					fclose($handle);
				}
				$handle = fopen(CAMINHO_PORTAL . 'batch/restartServer.bat', 'w+');
				$contentConfig = 'runas /profile /user:' . BD_HOST . ' "cmd" /K
                                  net stop mssqlserver
                                  net start mssqlserver';
				fwrite($handle, $contentConfig);
				fclose($handle);
			} elseif (BD_TYPE == 'sqlite') {
				$controllerEleicao = new ControllerEleicao();
				$data['msg'] = $controllerEleicao->copydataBases();
			}
			$view->data = $data;
			$view->carregar("usuario/login.html");
			$view->mostrar();
		} else {
			if ($_SESSION['usuario']['perfil'] == 1) {
				header("location: /eleicao/menuGeral/");
			} elseif ($_SESSION['usuario']['perfil'] == 2) {
				header("location: /urna/menuUrna/");
			}
		}
	}

	/**
	 * Verifica se usuário existe, caso sim realiza o login e seta as configuraçõe da eleição na sessão.
	 * 
	 * @return void
	 */
	public function processaLogin($parametros) {
		include_once ("model/usuario/ModelUsuario_" . BD_TYPE . ".php");
		include_once ("model/eleicao/ModelEleicao_" . BD_TYPE . ".php");
		include_once ("model/urna/ModelUrna_" . BD_TYPE . ".php");
		include_once ("model/cargo/ModelCargo_" . BD_TYPE . ".php");
		include_once ("controller/urna/ControllerUrna.php");
		include_once ("conf/functions.inc.php");
		$controllerEleicao = new ControllerEleicao();
		$data['msg'] = $controllerEleicao->copydataBases();
        $func = new Functions();
		$modelEleicao = new ModelEleicao();
		$objEleicao = $modelEleicao->eleicao();
		$restoreAction = false;
		$model = new ModelUsuario();
		$func = new Functions;
		$modelUrna = new ModelUrna();
		$dataHojeServidor = date("Y-m-d");
		$dataHojeEleicao = $func->removeHora($objEleicao[0]['ele_horaInicio']);
		$diferencaEntreDatas = $func->diffDate($dataHojeEleicao, $dataHojeServidor, 'D');

        if (ENVIRONMENT == 'production') {
            if ($diferencaEntreDatas != 0 && $objEleicao[0]['ele_horaInicio'] != '') {
                $data['result'] = false;
                $data['msg'] = "Votações encerradas para o dia de hoje.";
                $view = new View();
                $view->data = $data;
                $view->tipo = 'padrao';
                $view->carregar("usuario/xmlLogin.html");
                $view->mostrar();
                die();
            }
            if ($objEleicao[0]['ele_horaInicio'] == '') {
                $data['result'] = false;
                $data['msg'] = "Falha no Login, tente novamente.";
                $view = new View();
                $view->data = $data;
                $view->tipo = 'padrao';
                $view->carregar("usuario/xmlLogin.html");
                $view->mostrar();
                die();
            }
        }

		$execute = $model->pesquisarAcesso($_POST['login'], $_POST['senha']);
		$data['result'] = 'ok';
		if ($execute) {
			sleep(5);
			$_SESSION['usuario']['id'] = $execute->id;
			$_SESSION['usuario']['nome'] = $execute->login;
			$_SESSION['usuario']['perfil'] = $execute->perfil;
			$_SESSION['usuarioLogado'] = true;
			$_SESSION['eleicao']['id'] = $objEleicao[0]['ele_id'];
			$_SESSION['eleicao']['titulo'] = $objEleicao[0]['ele_descricao'];
			$_SESSION['eleicao']['data'] = $func->removeHora($objEleicao[0]['ele_horaInicio']);
			$_SESSION['eleicao']['dataFim'] = $func->removeHora($objEleicao[0]['ele_horaTermino']);
			$_SESSION['eleicao']['horaInicio'] = $func->removeData($objEleicao[0]['ele_horaInicio']);
			$_SESSION['eleicao']['horaTermino'] = $func->removeData($objEleicao[0]['ele_horaTermino']);
			$_SESSION['eleicao']['qtdVotos'] = $objEleicao[0]['ele_qtdVotosCandidatos'];
			$_SESSION['eleicao']['qtdBu'] = $objEleicao[0]['ele_qtdBu'];
			$_SESSION['eleicao']['qtdZeresima'] = $objEleicao[0]['ele_qtdZeresima'];
			$modelCargo = new ModelCargo();
			$objCargo = $modelCargo->listaCargo();
			$_SESSION['eleicao']['cargos'] = $objCargo;
			$modelUrna = new ModelUrna();
			$objUrna = $modelUrna->listaUrna();
			$_SESSION['eleicao']['zona'] = $objUrna[0]->zona;
			$_SESSION['eleicao']['secao'] = $objUrna[0]->secao;
			$_SESSION['eleicao']['urn_numero'] = $objUrna[0]->numero;
            if (!file_exists(CAMINHO_MIRROR."config.ini")) {
                $handle = fopen($_SESSION['CAMINHO_MIRROR'] . 'config.ini', "w+");
                $contentConfig = md5($_SESSION['eleicao']['zona']) . ';' . md5($_SESSION['eleicao']['secao']). ';' . md5($_SESSION['eleicao']['Urn_numero']);
                fwrite($handle, $contentConfig);
                fclose($handle);
            }
			if ($_SESSION['usuario']['perfil'] == 1) {
				header("location: /eleicao/menuGeral/");
			} elseif ($_SESSION['usuario']['perfil'] == 2) {
				//var_dump(exec("START " . CAMINHO_PORTAL . 'winF12.exe'));
				sleep(1);
				header("location: /urna/menuUrna/");
			}
		} else {
			$data['result'] = false;
			$data['msg'] = 'Login e/ou Senha inválidos';
			$view = new View();
			$view->data = $data;
			$view->tipo = 'padrao';
			$view->carregar("usuario/xmlLogin.html");
			$view->mostrar();
		}
	}

}
